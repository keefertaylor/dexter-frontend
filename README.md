# dexter-frontend

Please read the [Dexter frontend documentation](./docs/dexter-frontend.md) 
located in the docs directory to learn how to use `dexter-frontend`.

## Third party dependencies

The Dexter frontend depends on the following third party services.

### Tezos Node

In order to get data about the Dexter and FA1.2 contracts (like current amount 
of XTZ and token held by dexter and the user), Dexter queries data from a Tezos 
node. The node has two default values.

- [https://mainnet.tezos.org.ua](https://mainnet.tezos.org.ua) for production
- [https://tezos-dev.cryptonomic-infra.tech](https://tezos-dev.cryptonomic-infra.tech) for testing and development

In the settings tab, the user can choose a preferred node.

### Beacon

Beacon is an implementation of the 
[tzip-10 Wallet Interaction Standard](https://gitlab.com/tzip/tzip/tree/master/proposals/tzip-10).
It is a Chrome extension that allows users to sign Tezos transactions from a web
application. It interacts with a locally connected [Ledger](https://www.ledger.com/) 
that has a Tezos address or a locally stored Tezos secret key (only recommended for non-mainnet 
purposes). The dexter app is not responsible for signing and sending data to a 
Tezos node, it passes this reponsibility to the user's chosen wallet (which will use 
the node they have chosen in settings).

In order to communicate with the beacon extension, a web application must use the
[beacon-sdk](https://github.com/airgap-it/beacon-sdk/). This library is written
in TypeScript. Since Dexter is written in ReasonML, we have written FFI types and
functions around the values we need.

Dexter calls Beacon in four cases:

- To get user permissions and sign in to the user's wallet
- To perform an exchange (XTZ to Token or Token to XTZ)
- To add liquidity
- To remove liquidity

When the user performs an action with Beacon, they can review the details before
confirming. The user cannot change the details of the transaction in Beacon. If
they want to make a change, the must go through the Dexter UI.

#### Links

- [Beacon Playground](https://www.walletbeacon.io/)
- [Beacon Extension & SDK Release](https://medium.com/airgap-it/beacon-extension-sdk-release-de152dd94ad6)
- [beacon-sdk](https://github.com/airgap-it/beacon-sdk/)
- [beacon-extension](https://github.com/airgap-it/beacon-extension/)
- [tzip-10: Wallet Interaction Standard](https://gitlab.com/tzip/tzip/tree/master/proposals/tzip-10)

### TezBridge

TezBridge is a web based wallet. It allows users to sign Tezos transactions 
from web applications. It is a pure web application. The user does not need
to install other software. Communication with TezBridge will open a second
tab in their browser.

Users can store their secret keys in local storage (only recommended for non-mainnet 
purposes) via TezBridge or a Ledger for signing transactions.

We have some FFI types and functions around the TezBridge JavaScript code.

Dexter calls TezBridge in five cases:

- To initiate a session with the user's ledger or stored secret key
- To change the Tezos node that TezBridge uses (broken)
- To perform an exchange (XTZ to Token or Token to XTZ)
- To add liquidity
- To remove liquidity

#### Links

- [TezBridge](https://www.tezbridge.com/)
- [TezBridge source linked in Dexter](https://www.tezbridge.com/plugin.js)
- [TezBridge documentation](https://docs.tezbridge.com/)

### Messari

We display the USD value of XTZ based on the market rate provided by Messari.
We also display the USD value of FA1.2 tokens based on the market rate of
XTZ to USD and the dexter exchange rate of FA1.2 tokens to XTZ 
(token's xtz value * xtz's usd value).

#### Links

- [Messari market data API](https://data.messari.io/api/v1/assets/xtz/metrics/market-data)

### TezBlock.io

TezBlock.io is a tezos block expxlorer. We provided a link to the current block 
and the user's transactions on TezBlock.

#### Links

- [mainnet block](https://tezblock.io/block/)
- [mainnet transaction](https://tezblock.io/transaction/)
- [carthagenet block](https://carthagenet.tezblock.io/block/)
- [carthagenet transaction](https://carthagenet.tezblock.io/transaction/)

### Sentry

Sentry collects JavaScript errors from the user's web browser. It does not
collect their IP address, public key or private key.

#### Links

- [Sentry](https://sentry.io)

### Google Fonts

We use the Source Sans Pro font from Google.

#### Links

- [Font link](https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600&display=swap)

### Caldera

Caldera is an internal development tool for testnet only. It allows users
to withdraw 500 XTZ once to their public address on carthagenet for testing
purposes. It will not be used in production

#### Links

- [Caldera](https://caldera.camlcase.io/caldera/transfer/)
- [Caldera project](https://gitlab.com/camlcase-dev/caldera/)
