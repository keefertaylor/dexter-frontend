# Tokens 

Dexter creates a market between the Tezos native XTZ tokens and the Tezos smart 
contract based tokens FA1.2. FA1.2 tokens are fungible and transferable tokens.
There is a minimum specification of what they must do like being transferable and 
fungible and provide data on the total supply and how much an account owns. 
However, beyond that the creators are allowed to include whatever features they 
like.

Because the possibility of FA1.2 token contracts is so large, we have developed 
a list of expected behaviors that make an FA1.2 contract compatible with Dexter.
The list is filled out for every token that Dexter supports and users can review
and decide if they are willing to purchase certain tokens.

## Technical Details

https://gitlab.com/tzip/tzip/-/blob/master/proposals/tzip-7/tzip-7.md

https://gitlab.com/tzip/tzip/-/blob/master/proposals/tzip-7/ManagedLedger.tz

## Checklists

We maintain a checklist of desirable and undesirable features for an FA1.2 to
be considered a good candidate for pairing with a Dexter contract.
https://gitlab.com/camlcase-dev/dexter/-/blob/develop/docs/token-integration.md

### usdtz

### tezos btc
