# Liquidity Pools

Each Dexter contract holds two liquidity (asset) pools. One for XTZ, the xtz 
pool, and one for FA1.2, the token pool. Ownership of these liquidity pools 
are represented by pool tokens. When you add liquidity to a Dexter contract, 
it gives puts pool tokens in your ownership. Pool tokens are non-transferable, 
but can be redeemed for an equivalent value of XTZ and FA1.2. 

The value of the pool tokens are determined by the amount in the liquidity pools.
Whenever you redeem `X` pool tokens, you will receive 
`X * xtz pool / total pool tokens` of XTZ and 
`X * token pool / total pool tokens` of FA1.2 tokens.

The total pool tokens and number of pool tokens you own can only be incremented 
through add liquidity and decremented with redeem pool tokens. When other users 
trade  one token for another, it does not affect the total amount of pool tokens 
you own. However, it changes the amounts of the liquidity pools.

When a trader performs XTZ to FA1.2 tokens, it increments the amount of XTZ liquidity
pool and decreases the amount of the token pool. This causes redeeming pool 
tokens to return more XTZ and less FA1.2 tokens. It also causes the XTZ to FA1.2
token exchange rate to decrease.

When a trader performs FA1.2 tokens to XTZ, it increments the amount of token liquidity
pool and decreases the amount of the XTZ pool. This causes redeeming pool 
tokens to return more FA1.2 tokens and less XTZ. It also causes the XTZ to FA1.2
token exchange rate to increase.

## Technical

Each Dexter has an abstract number LQT, which are referred to in the frontend as
token pools. When an account adds liquidity to Dexter, Dexter keeps track of how
much the user has contributed by minting LQT tokens and placing them in the 
ownership of the account. This tokens are non-transferable. The account can keep
them and at some point in the future redeem them for an equal amount of XTZ and
FA1.2 token held by Dexter. 

When there is no liquidity in Dexter, the next provider is free to decide how
much XTZ and FA1.2 to include and set the initial exchange rate. The initial 
amount of LQT that it creates is wholly a function of the amount of XTZ added,
but you must add at least one XTZ.

`initialLiquidity = XTZ in`

When there is liquidity in Dexter, the amount of LQT created and given to the 
liquidity is a function of:

`XTZ in * total LQT in dexter / XTZ pool`

Also, when you add liquidity in a Dexter contract, you decide the amount of 
XTZ added and a maximum amount of tokens added. The amount of tokens deposited
is determined by the following formula:

`XTZ in * token pool / xtz pool` then round up.

If this is greater than the max you provided, the operation will fail, thus 
preventing a trade that would take more tokens than you are willing to add.
