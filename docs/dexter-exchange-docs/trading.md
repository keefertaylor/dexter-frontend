# Trading

Once a Dexter contract has liquidity, anyone can perform trades if they have 
enough XTZ or FA1.2 token to purchase a minimal amount according to the 
exchange rate.

If you hold XTZ and you want to purchase FA1.2, you can sell XTZ to Dexter and
it will give you the equivalent amount of FA1.2 minus 0.3% of your XTZ which
is given to the liquidity providers as a fee.

The opposite relation holds true you hold FA1.2 tokens and want to sell them 
to Dexter. It will give you an equivalent amount of XTZ minus 0.3% of your 
FA1.2 tokens which is give to the liquidity providers as a fee.

Finally, you can trade a FA1.2 token A for another FA1.2 token B. This works 
just like performing A to XTZ then XTZ to B, but it ensures that you get your
preferred exchange rate and does it faster than performing it in two operations.
There is a 0.3% fee paid in A on A to XTZ then another 0.3% fee paid in XTZ on
XTZ to B.

## Technical

- token pool is how many FA1.2 tokens Dexter holds.
- XTZ pool is how much XTZ Dexter holds.
- XTZ in is how much the trader is selling to Dexter to receive FA1.2 token.
- token in is how much the trader is selling to Dexter to receive XTZ.

The XTZ to Token exchange rate formula:

(token pool * (XTZ in - 0.3%)) / (XTZ pool + (XTZ in - 0.3%))

The Token to XTZ exchange rate formula:

(XTZ pool * (token in - 0.3%)) / (token pool + (token in - 0.3%))
