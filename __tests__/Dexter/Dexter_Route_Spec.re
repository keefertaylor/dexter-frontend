open Jest;
open Expect;
open Dexter_Route;

describe("toUrl", () => {
  test("Exchange", () =>
    expect(toUrl(Exchange)) |> toEqual("exchange")
  );
  test("AddLiquidity", () =>
    expect(toUrl(LiquidityPool(AddLiquidity))) |> toEqual("liquidity/add")
  );
  test("RemoveLiquidity", () =>
    expect(toUrl(LiquidityPool(RemoveLiquidity)))
    |> toEqual("liquidity/remove")
  );
  test("PoolData", () =>
    expect(toUrl(LiquidityPool(PoolData))) |> toEqual("liquidity/pool")
  );
  test("NotFound", () =>
    expect(toUrl(NotFound)) |> toEqual("404")
  );
});

describe("fromPath", () => {
  test("[]", () =>
    expect(fromPath([])) |> toEqual(Exchange)
  );
  test("['exchange']", () =>
    expect(fromPath(["exchange"])) |> toEqual(Exchange)
  );
  test("['liquidity', 'add']", () =>
    expect(fromPath(["liquidity", "add"]))
    |> toEqual(LiquidityPool(AddLiquidity))
  );
  test("['liquidity', 'remove']", () =>
    expect(fromPath(["liquidity", "remove"]))
    |> toEqual(LiquidityPool(RemoveLiquidity))
  );
  test("['liquidity', 'pool']", () =>
    expect(fromPath(["liquidity", "pool"]))
    |> toEqual(LiquidityPool(PoolData))
  );
  test("['liquidity', '404']", () =>
    expect(fromPath(["liquidity", "404"])) |> toEqual(NotFound)
  );
  test("['icorrect']", () =>
    expect(fromPath(["incorrect"])) |> toEqual(NotFound)
  );
});

describe("getTokensHash", () => {
  test("MTZ_XTZ", () =>
    expect(
      getTokensHash(
        Fixtures_Balances.mtzBalance,
        Fixtures_Balances.xtzOneTezBalance,
      ),
    )
    |> toEqual("MTZ_XTZ")
  );
  test("XTZ_MTZ", () =>
    expect(
      getTokensHash(
        Fixtures_Balances.xtzOneTezBalance,
        Fixtures_Balances.mtzBalance,
      ),
    )
    |> toEqual("XTZ_MTZ")
  );
  test("TZG_TZT", () =>
    expect(
      getTokensHash(
        Fixtures_Balances.tzgBalance,
        Fixtures_Balances.tztBalance,
      ),
    )
    |> toEqual("TZG_TZT")
  );
});
