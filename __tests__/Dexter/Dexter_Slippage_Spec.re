open Jest;
open Expect;
open Dexter_Slippage;

describe("ofFloat", () => {
  test("0.1", () =>
    expect(ofFloat(0.1)) |> toEqual(PointOne)
  );
  test("0.5", () =>
    expect(ofFloat(0.5)) |> toEqual(Half)
  );
  test("1.", () =>
    expect(ofFloat(1.)) |> toEqual(One)
  );
  test("10.", () =>
    expect(ofFloat(10.)) |> toEqual(Custom(Valid(10., "10")))
  );
});

describe("toFloat", () => {
  test("PointOne", () =>
    expect(toFloat(PointOne)) |> toEqual(0.1)
  );
  test("Half", () =>
    expect(toFloat(Half)) |> toEqual(0.5)
  );
  test("One", () =>
    expect(toFloat(One)) |> toEqual(1.)
  );
  test("Custom(Invalid('0.23.'))", () =>
    expect(toFloat(Custom(Invalid("0.23.")))) |> toEqual(0.)
  );
});

describe("toString", () => {
  test("PointOne", () =>
    expect(toString(PointOne)) |> toEqual("0.1%")
  );
  test("Half", () =>
    expect(toString(Half)) |> toEqual("0.5%")
  );
  test("One", () =>
    expect(toString(One)) |> toEqual("1.0%")
  );
  test("Custom(Valid(10., '10'))", () =>
    expect(toString(Custom(Valid(10., "10")))) |> toEqual("10.00%")
  );
  test("Custom(Invalid('0.23.'))", () =>
    expect(toString(Custom(Invalid("0.23.")))) |> toEqual("")
  );
});
