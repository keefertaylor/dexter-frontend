open Jest;
open Expect;
open InputType;
open Valid;
open Dexter_Value;

describe("getMutezValue", () => {
  test("Valid", () =>
    expect(getMutezValue(Tezos.Mutez.oneTez))
    |> toEqual(Valid(Tezos.Mutez.oneTez, Tezos.Mutez.(toTezString(oneTez))))
  );
  test("Invalid", () =>
    expect(getMutezValue(~invalid=true, Tezos.Mutez.oneTez))
    |> toEqual(Invalid(Tezos.Mutez.(toTezString(oneTez))))
  );
  test("mutezZeroValue", () =>
    expect(mutezZeroValue)
    |> toEqual(Valid(Tezos.Mutez.zero, Tezos.Mutez.(toTezString(zero))))
  );
});

describe("getTokenValue", () => {
  test("Valid", () =>
    expect(getTokenValue(Tezos.Token.one))
    |> toEqual(
         Valid(Tezos.Token.one, Tezos.Token.toString(Tezos.Token.one)),
       )
  );
  test("Invalid", () =>
    expect(getTokenValue(~invalid=true, Tezos.Token.one))
    |> toEqual(Invalid(Tezos.Token.toString(Tezos.Token.one)))
  );
  test("tokenZeroValue", () =>
    expect(tokenZeroValue)
    |> toEqual(
         Valid(Tezos.Token.zero, Tezos.Token.toString(Tezos.Token.zero)),
       )
  );
});

describe("getMutezInputValue", () => {
  test("Valid", () =>
    expect(getMutezInputValue(Tezos.Mutez.oneTez))
    |> toEqual(
         Valid(
           Mutez(Tezos.Mutez.oneTez),
           Tezos.Mutez.(toTezString(oneTez)),
         ),
       )
  );
  test("Invalid", () =>
    expect(getMutezInputValue(~invalid=true, Tezos.Mutez.oneTez))
    |> toEqual(Invalid(Tezos.Mutez.(toTezString(oneTez))))
  );
  test("mutezZeroInputValue", () =>
    expect(mutezZeroInputValue)
    |> toEqual(
         Valid(Mutez(Tezos.Mutez.zero), Tezos.Mutez.(toTezString(zero))),
       )
  );
});

describe("getTokenInputValue", () => {
  test("Valid", () =>
    expect(getTokenInputValue(Tezos.Token.one))
    |> toEqual(
         Valid(
           Token(Tezos.Token.one),
           Tezos.Token.toString(Tezos.Token.one),
         ),
       )
  );
  test("Invalid", () =>
    expect(getTokenInputValue(~invalid=true, Tezos.Token.one))
    |> toEqual(Invalid(Tezos.Token.toString(Tezos.Token.one)))
  );
  test("tokenZeroInputValue", () =>
    expect(tokenZeroInputValue)
    |> toEqual(
         Valid(
           Token(Tezos.Token.zero),
           Tezos.Token.toString(Tezos.Token.zero),
         ),
       )
  );
});

describe("getZeroInputValue", () => {
  test("XTZ", () =>
    expect(getZeroInputValue(Fixtures_Balances.xtzOneTezBalance))
    |> toEqual(
         Valid(
           Mutez(Tezos.Mutez.zero),
           Tezos.Mutez.toTezString(Tezos.Mutez.zero),
         ),
       )
  );
  test("Token", () =>
    expect(getZeroInputValue(Fixtures_Balances.mtzBalance))
    |> toEqual(
         Valid(
           Token(Tezos.Token.zero),
           Tezos.Token.toString(Tezos.Token.zero),
         ),
       )
  );
});

describe("inputValueToStringWithCommas", () => {
  test("Token", () =>
    expect(
      inputValueToStringWithCommas(
        Valid(
          Token(Tezos.Token.one),
          Tezos.Token.toString(Tezos.Token.one),
        ),
      ),
    )
    |> toEqual(Tezos.Token.one |> Tezos.Token.toStringWithCommas)
  );
  test("XTZ", () =>
    expect(
      inputValueToStringWithCommas(
        Valid(
          Mutez(Tezos.Mutez.one),
          Tezos.Mutez.toString(Tezos.Mutez.one),
        ),
      ),
    )
    |> toEqual(Tezos.Mutez.one |> Tezos.Mutez.toTezStringWithCommas)
  );
});
