open Jest;
open Expect;

describe("toStringWithCommas", () => {
  test("USD, 1, 1.92", () =>
    expect(
      Currency.toStringWithCommas(Fixtures_CurrenciesState.currencyUSD, 1.),
    )
    |> toEqual({js|$1.92|js})
  );
  test("BTC, 10, 0.00142140", () =>
    expect(
      Currency.toStringWithCommas(Fixtures_CurrenciesState.currencyBTC, 10.),
    )
    |> toEqual({js|₿0.00142140|js})
  );
  test("NOK, 100, 1,777.00", () =>
    expect(
      Currency.toStringWithCommas(Fixtures_CurrenciesState.currencyNOK, 100.),
    )
    |> toEqual({js|1,777.00 kr|js})
  );
  test("XDR, 5, 6.70", () =>
    expect(
      Currency.toStringWithCommas(Fixtures_CurrenciesState.currencyXDR, 5.),
    )
    |> toEqual({js|6.70 XDR|js})
  );
  test("BHD, 100, 71.192", () =>
    expect(
      Currency.toStringWithCommas(Fixtures_CurrenciesState.currencyBHD, 100.),
    )
    |> toEqual({js|BD 71.192|js})
  );
});

describe("findBySymbolOrDefault", () => {
  test("BTC", () =>
    expect(
      Currency.findBySymbolOrDefault(
        "BTC",
        Fixtures_CurrenciesState.currencies,
      ),
    )
    |> toEqual(Some(Fixtures_CurrenciesState.currencyBTC))
  );
  test("ABC", () =>
    expect(
      Currency.findBySymbolOrDefault(
        "ABC",
        Fixtures_CurrenciesState.currencies,
      ),
    )
    |> toEqual(Some(Fixtures_CurrenciesState.currencyUSD))
  );
  test("USD; empty", () =>
    expect(Currency.findBySymbolOrDefault("USD", [])) |> toEqual(None)
  );
});
