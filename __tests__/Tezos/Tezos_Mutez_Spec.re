open Jest;
open Expect;

describe("ofTezString", () => {
  test("", _ =>
    expect(Tezos.Mutez.ofTezString("0.000001"))
    |> toEqual(Tezos.Mutez.ofInt64(1L))
  );

  test("", _ =>
    expect(Tezos.Mutez.ofTezString("0"))
    |> toEqual(Tezos.Mutez.ofInt64(0L))
  );

  test("", _ =>
    expect(Tezos.Mutez.ofTezString("0."))
    |> toEqual(Tezos.Mutez.ofInt64(0L))
  );

  test("", _ =>
    expect(Tezos.Mutez.ofTezString("0.0"))
    |> toEqual(Tezos.Mutez.ofInt64(0L))
  );

  test("", _ =>
    expect(Tezos.Mutez.ofTezString("1.1"))
    |> toEqual(Tezos.Mutez.ofInt64(1100000L))
  );

  test("", _ =>
    expect(Tezos.Mutez.ofTezString("1.12"))
    |> toEqual(Tezos.Mutez.ofInt64(1120000L))
  );

  test("", _ =>
    expect(Tezos.Mutez.ofTezString("1.123456"))
    |> toEqual(Tezos.Mutez.ofInt64(1123456L))
  );

  test("", _ =>
    expect(Tezos.Mutez.ofTezString("12"))
    |> toEqual(Tezos.Mutez.ofInt64(12000000L))
  );

  test("", _ =>
    expect(Tezos.Mutez.ofTezString("23"))
    |> toEqual(Tezos.Mutez.ofInt64(23000000L))
  );

  test("", _ =>
    expect(Tezos.Mutez.ofTezString("123.145"))
    |> toEqual(Tezos.Mutez.ofInt64(123145000L))
  );

  test("too many digits", _ =>
    expect(Belt.Result.isError(Tezos.Mutez.ofTezString("0.0000001")))
    |> toEqual(true)
  );

  test("too many digits", _ =>
    expect(Belt.Result.isError(Tezos.Mutez.ofTezString("0.0000000")))
    |> toEqual(true)
  );
});

test("compare", _ => {
  expect(Tezos.Mutez.compare(Tezos.Mutez.zero, Tezos.Mutez.zero))
  |> toEqual(0)
  |> ignore;
  expect(Tezos.Mutez.compare(Tezos.Mutez.zero, Tezos.Mutez.one))
  |> toEqual(-1)
  |> ignore;
  expect(Tezos.Mutez.compare(Tezos.Mutez.one, Tezos.Mutez.zero))
  |> toEqual(1);
});

test("tokenToXtz fee", _ => {
  expect(Tezos.Mutez.(subPointThreePercent(one)))
  |> toEqual(Tezos.Mutez.zero)
  |> ignore;
  expect(Tezos.Mutez.(subPointThreePercent(ofIntUnsafe(2))))
  |> toEqual(Tezos.Mutez.one)
  |> ignore;
  expect(Tezos.Mutez.(subPointThreePercent(ofIntUnsafe(1000))))
  |> toEqual(Tezos.Mutez.ofIntUnsafe(997))
  |> ignore;
  expect(Tezos.Mutez.(subPointThreePercent(ofIntUnsafe(2000))))
  |> toEqual(Tezos.Mutez.ofIntUnsafe(1994))
  |> ignore;
  expect(Tezos.Mutez.(subPointThreePercent(ofIntUnsafe(10000))))
  |> toEqual(Tezos.Mutez.ofIntUnsafe(9970))
  |> ignore;
  expect(Tezos.Mutez.(subPointThreePercent(ofIntUnsafe(22345))))
  |> toEqual(Tezos.Mutez.ofIntUnsafe(22277));
});

describe("Tezos_Mutez.toTezFloat", () => {
  test("1.23", _ => {
    expect(
      Belt.Result.map(
        Tezos.Mutez.ofTezString("1.23"),
        Tezos.Mutez.toTezFloat,
      ),
    )
    |> toEqual(Belt.Result.Ok(1.23))
  });

  test("0.001", _ => {
    expect(
      Belt.Result.map(
        Tezos.Mutez.ofTezString("0.001"),
        Tezos.Mutez.toTezFloat,
      ),
    )
    |> toEqual(Belt.Result.Ok(0.001))
  });

  test("1.0", _ => {
    expect(
      Belt.Result.map(
        Tezos.Mutez.ofTezString("1.0"),
        Tezos.Mutez.toTezFloat,
      ),
    )
    |> toEqual(Belt.Result.Ok(1.0))
  });

  test("104.112233", _ => {
    expect(
      Belt.Result.map(
        Tezos.Mutez.ofTezString("104.112233"),
        Tezos.Mutez.toTezFloat,
      ),
    )
    |> toEqual(Belt.Result.Ok(104.112233))
  });
});
