open Jest;
open Expect;
open ReactTestingLibrary;

describe("Snapshots", () => {
  test("Exchange applied", () => {
    render(
      <DexterUi_TransactionsRecentRow
        account=Fixtures_Account.account1
        transaction=Fixtures_Transactions.exchange
      />,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("Exchange and send pending", () => {
    render(
      <DexterUi_TransactionsRecentRow
        account=Fixtures_Account.account1
        transaction=Fixtures_Transactions.exchangeAndSend
      />,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("Add liquidity failed", () => {
    render(
      <DexterUi_TransactionsRecentRow
        account=Fixtures_Account.account1
        transaction=Fixtures_Transactions.addLiquidity
      />,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("Remove liquidity applied", () => {
    render(
      <DexterUi_TransactionsRecentRow
        account=Fixtures_Account.account1
        transaction=Fixtures_Transactions.removeLiquidity
      />,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
});
