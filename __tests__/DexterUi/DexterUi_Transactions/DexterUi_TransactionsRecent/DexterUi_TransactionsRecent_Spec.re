open Jest;
open Expect;
open ReactTestingLibrary;

describe("Snapshots", () => {
  test("No transactions fetched", () => {
    render(
      <DexterUi_Context.ProviderInternal
        value={
          ...DexterUi_Context.defaultValue,
          account: Some(Fixtures_Account.account1),
        }>
        <DexterUi_TransactionsRecent />
      </DexterUi_Context.ProviderInternal>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("No transactions", () => {
    render(
      <DexterUi_Context.ProviderInternal
        value={
          ...DexterUi_Context.defaultValue,
          account: Some(Fixtures_Account.account1),
          transactions: Some([]),
        }>
        <DexterUi_TransactionsRecent />
      </DexterUi_Context.ProviderInternal>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("With transactions", () => {
    render(
      <DexterUi_Context.ProviderInternal
        value={
          ...DexterUi_Context.defaultValue,
          account: Some(Fixtures_Account.account1),
          transactions: Some([Fixtures_Transactions.exchange]),
        }>
        <DexterUi_TransactionsRecent />
      </DexterUi_Context.ProviderInternal>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
});
