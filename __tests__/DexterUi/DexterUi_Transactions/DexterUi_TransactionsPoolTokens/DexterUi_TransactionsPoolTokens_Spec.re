open Jest;
open Expect;
open ReactTestingLibrary;

describe("Snapshots", () => {
  test("No pool tokens", () => {
    render(<DexterUi_TransactionsPoolTokens />)
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("With pool token, but no lqtBalance", () => {
    render(
      <DexterUi_Context.ProviderInternal
        value={
          ...DexterUi_Context.defaultValue,
          balances: [Fixtures_Balances.tztBalance],
        }>
        <DexterUi_TransactionsPoolTokens />
      </DexterUi_Context.ProviderInternal>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("With pool token", () => {
    render(
      <DexterUi_Context.ProviderInternal
        value={
          ...DexterUi_Context.defaultValue,
          balances: [Fixtures_Balances.mtzBalance],
        }>
        <DexterUi_TransactionsPoolTokens />
      </DexterUi_Context.ProviderInternal>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
});
