open Jest;
open Expect;
open ReactTestingLibrary;

describe("Snapshots", () => {
  test("Dark mode 332.123523 XTZ without border", () => {
    render(
      <DexterUi_Context.ProviderInternal
        value={...DexterUi_Context.defaultValue, darkMode: true}>
        <DexterUi_WalletInfoBalancesRow
          symbol={"XTZ" |> React.string}
          amount={"332.123523" |> React.string}
        />
      </DexterUi_Context.ProviderInternal>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("Light mode 10000 MTZ with border", () => {
    render(
      <DexterUi_Context.ProviderInternal
        value={...DexterUi_Context.defaultValue, darkMode: true}>
        <DexterUi_WalletInfoBalancesRow
          symbol={"MTZ" |> React.string}
          amount={"10000" |> React.string}
        />
      </DexterUi_Context.ProviderInternal>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
});
