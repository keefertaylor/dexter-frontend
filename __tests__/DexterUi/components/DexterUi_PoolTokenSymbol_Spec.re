open Jest;
open Expect;
open ReactTestingLibrary;

describe("Snapshots", () => {
  test("XTZ/MTZ", () => {
    render(
      <DexterUi_PoolTokenSymbol
        poolToken=(
          Fixtures_Balances.xtzTenTezBalance,
          Fixtures_Balances.mtzBalance,
        )
      />,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("MTZ/TZG", () => {
    render(
      <DexterUi_PoolTokenSymbol
        poolToken=(Fixtures_Balances.mtzBalance, Fixtures_Balances.tzgBalance)
      />,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
});
