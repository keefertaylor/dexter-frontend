open Jest;
open Expect;
open ReactTestingLibrary;

describe("Snapshots", () => {
  test("Render", () => {
    render(<DexterUi_ConnectWallet />)
    |> container
    |> expect
    |> toMatchSnapshot
  })
});
