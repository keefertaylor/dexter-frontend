open Jest;
open Expect;
open ReactTestingLibrary;

let display = DexterUi_ExchangeRate.display;

describe("display", () => {
  test("exchange rate for xtz to token", () => {
    let inputValue: Valid.t(InputType.t) =
      Valid(Mutez(Tezos.Mutez.Mutez(500000L)), "");
    let inputToken = Fixtures_Balances.xtzOneTezBalance;
    let outputToken = Fixtures_Balances.tzgBalance;
    expect(display(inputValue, inputToken, outputToken))
    |> toEqual("1 XTZ = 9920.00000000 TZG");
  });

  test("exchange rate for xtz to token", () => {
    let inputValue: Valid.t(InputType.t) =
      Valid(Mutez(Tezos.Mutez.oneTez), "");
    let inputToken = Fixtures_Balances.xtzOneTezBalance;
    let outputToken = Fixtures_Balances.tzgBalance;
    expect(display(inputValue, inputToken, outputToken))
    |> toEqual("1 XTZ = 9871.00000000 TZG");
  });

  test("exchange rate for xtz to token", () => {
    let inputValue: Valid.t(InputType.t) =
      Valid(Mutez(Fixtures_Balances.tenTez), "");
    let inputToken = Fixtures_Balances.xtzTenTezBalance;
    let outputToken = Fixtures_Balances.tzgBalance;
    expect(display(inputValue, inputToken, outputToken))
    |> toEqual("1 XTZ = 9066.10000000 TZG");
  });

  test("exchange rate for token to xtz", () => {
    let inputValue: Valid.t(InputType.t) =
      Valid(
        InputType.Token(Tezos.Token.mkToken(Bigint.of_int(100000), 0)),
        "",
      );
    let inputToken = Fixtures_Balances.tzgBalance;
    let outputToken = Fixtures_Balances.xtzOneTezBalance;
    expect(display(inputValue, inputToken, outputToken))
    |> toEqual("1 TZG = 0.00009066 XTZ");
  });

  test("exchange rate for token to xtz", () => {
    let inputValue: Valid.t(InputType.t) =
      Valid(Token(Tezos.Token.mkToken(Bigint.of_int(1000000), 0)), "");
    let inputToken = Fixtures_Balances.tzgBalance;
    let outputToken = Fixtures_Balances.xtzOneTezBalance;
    expect(display(inputValue, inputToken, outputToken))
    |> toEqual("1 TZG = 0.00004992 XTZ");
  });

  test("market rate for xtz to token", () => {
    let inputValue: Valid.t(InputType.t) = Invalid("");
    let inputToken = Fixtures_Balances.xtzOneTezBalance;
    let outputToken = Fixtures_Balances.tzgBalance;
    expect(display(inputValue, inputToken, outputToken))
    |> toEqual("1 XTZ = 10000.00000000 TZG");
  });

  test("market rate for token to xtz", () => {
    let inputValue: Valid.t(InputType.t) = Invalid("");
    let inputToken = Fixtures_Balances.tzgBalance;
    let outputToken = Fixtures_Balances.xtzOneTezBalance;

    expect(display(inputValue, inputToken, outputToken))
    |> toEqual("1 TZG = 0.00010000 XTZ");
  });

  test("market rate for xtz to token", () => {
    let inputValue: Valid.t(InputType.t) = Invalid("");
    let inputToken = Fixtures_Balances.xtzOneTezBalance;
    let outputToken = Fixtures_Balances.mtzBalance;
    expect(display(inputValue, inputToken, outputToken))
    |> toEqual("1 XTZ = 0.01523806 MTZ");
  });

  test("exchange rate for token to xtz", () => {
    let inputValue: Valid.t(InputType.t) =
      Valid(
        InputType.Token(Tezos.Token.mkToken(Bigint.of_int(1000), 3)),
        "",
      );
    let inputToken = Fixtures_Balances.tztBalance;
    let outputToken = Fixtures_Balances.xtzOneTezBalance;
    expect(display(inputValue, inputToken, outputToken))
    |> toEqual("1 TZT = 0.10078700 XTZ");
  });
});

describe("Snapshots", () => {
  test("tzgBalance", () => {
    render(
      <DexterUi_ExchangeRate
        inputValue={Valid(Mutez(Tezos.Mutez.Mutez(500000L)), "")}
        inputToken=Fixtures_Balances.xtzOneTezBalance
        outputToken=Fixtures_Balances.tzgBalance
      />,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });

  test("mtzBalance", () => {
    render(
      <DexterUi_ExchangeRate
        inputValue={Valid(Mutez(Tezos.Mutez.Mutez(500000L)), "")}
        inputToken=Fixtures_Balances.xtzOneTezBalance
        outputToken=Fixtures_Balances.mtzBalance
      />,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });

  test("tztBalance", () => {
    render(
      <DexterUi_ExchangeRate
        inputValue={Valid(Mutez(Tezos.Mutez.Mutez(500000L)), "")}
        inputToken=Fixtures_Balances.xtzOneTezBalance
        outputToken=Fixtures_Balances.tztBalance
      />,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
});
