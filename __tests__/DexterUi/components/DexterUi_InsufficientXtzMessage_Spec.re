open Jest;
open Expect;
open ReactTestingLibrary;

describe("Snapshots", () => {
  test("Renders", () => {
    render(<DexterUi_InsufficientXtzMessage />)
    |> container
    |> expect
    |> toMatchSnapshot
  })
});
