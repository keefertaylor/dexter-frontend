open Jest;
open Expect;
open DexterUi_AddLiquidity_Utils;

describe("getTokenMaxValue", () => {
  test("Limit from token balance", () =>
    expect(
      Valid.toString(
        getTokenMaxValue(
          Fixtures_Balances.mtzExchangeBalance,
          Tezos.Mutez.Mutez(1000000000000000L),
        ),
        Tezos.Token.toStringWithCommas,
      ),
    )
    |> toEqual("0.1")
  );
  test("Limit from xtz balance", () =>
    expect(
      Valid.toString(
        getTokenMaxValue(
          Fixtures_Balances.mtzExchangeBalance,
          Tezos.Mutez.Mutez(2000000L),
        ),
        Tezos.Token.toStringWithCommas,
      ),
    )
    |> toEqual("0.025")
  );
});
