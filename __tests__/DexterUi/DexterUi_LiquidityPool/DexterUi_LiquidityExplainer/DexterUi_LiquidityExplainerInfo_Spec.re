open Jest;
open Expect;
open ReactTestingLibrary;

describe("Snapshots", () => {
  test("Renders", () => {
    render(<DexterUi_LiquidityExplainerInfo />)
    |> container
    |> expect
    |> toMatchSnapshot
  })
});

[@react.component]
let make = () =>
  <Flex
    width={`percent(100.)}
    alignItems=`center
    flexDirection=`column
    mb={`px(12)}>
    <DexterUi_LiquidityExplainerInfoRow
      boxHeight={`px(118)} number=1 image="/img/liquidity-explainer-1.png">
      <Text lightModeColor=Colors.blackish1 textStyle=TextStyles.bold>
        {"Add liquidity (XTZ + any FA1.2 token)" |> React.string}
      </Text>
      <Flex mt={`px(2)} />
      <Text>
        {"Add equivalent amounts of XTZ and another FA1.2 token to the Liquidity Pool. These tokens provide liquidity for other users to make exchanges with Dexter."
         |> React.string}
      </Text>
      <Flex mt={`px(6)} />
      <Text>
        {"Receive a quantity of Dexter Pool Tokens representing the underlying pair of tokens you added to the Liquidity Pool."
         |> React.string}
      </Text>
    </DexterUi_LiquidityExplainerInfoRow>
    <Flex height={`px(16)} />
    <DexterUi_LiquidityExplainerInfoRow
      boxHeight={`px(96)}
      number=2
      image="/img/liquidity-explainer-2.png"
      imageHeight=50>
      <Text lightModeColor=Colors.blackish1 textStyle=TextStyles.bold>
        {"Earn liquidity fees" |> React.string}
      </Text>
      <Flex mt={`px(2)} />
      <Text>
        {"Earn liquidity fees in the form of Pool Tokens for the duration you provide liquidity to the exchange. Dexter charges 0.3% for all exchanges; you'll earn fees relative to the amount of liquidity you are providing to the pool."
         |> React.string}
      </Text>
    </DexterUi_LiquidityExplainerInfoRow>
    <Flex height={`px(16)} />
    <DexterUi_LiquidityExplainerInfoRow
      boxHeight={`px(96)} number=3 image="/img/liquidity-explainer-3.png">
      <Text lightModeColor=Colors.blackish1 textStyle=TextStyles.bold>
        {"Remove liquidity anytime" |> React.string}
      </Text>
      <Flex mt={`px(2)} />
      <Text>
        {"Remove the liquidity you've provided at any time by redeeming your Dexter Pool Tokens - including the tokens you've earned from liquidity fees - and receive corresponding amounts of your underlying asset pair in return."
         |> React.string}
      </Text>
    </DexterUi_LiquidityExplainerInfoRow>
  </Flex>;
