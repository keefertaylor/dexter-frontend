open Jest;
open Expect;
open ReactTestingLibrary;

describe("Snapshots", () => {
  test("TZG", () => {
    render(
      <DexterUi_Context.ProviderInternal
        value={
          ...DexterUi_Context.defaultValue,
          account: Some(Fixtures_Account.account1),
        }>
        <DexterUi_PoolDataRowButtons
          exchangeBalance=Fixtures_Balances.tzgExchangeBalance
        />
      </DexterUi_Context.ProviderInternal>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("MTZ", () => {
    render(
      <DexterUi_Context.ProviderInternal
        value={
          ...DexterUi_Context.defaultValue,
          account: Some(Fixtures_Account.account1),
        }>
        <DexterUi_PoolDataRowButtons
          exchangeBalance=Fixtures_Balances.mtzExchangeBalance
        />
      </DexterUi_Context.ProviderInternal>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("MTZ; No XTZ", () => {
    render(
      <DexterUi_Context.ProviderInternal
        value={
          ...DexterUi_Context.defaultValue,
          account:
            Some({...Fixtures_Account.account1, xtz: Tezos.Mutez.zero}),
        }>
        <DexterUi_PoolDataRowButtons
          exchangeBalance=Fixtures_Balances.mtzExchangeBalance
        />
      </DexterUi_Context.ProviderInternal>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
});
