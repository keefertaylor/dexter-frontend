open Jest;
open Expect;
open ReactTestingLibrary;

describe("Snapshots", () => {
  test("Loading", () => {
    render(
      <DexterUi_Context.ProviderInternal
        value={...DexterUi_Context.defaultValue, balances: []}>
        <DexterUi_RightColumn />
      </DexterUi_Context.ProviderInternal>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("Exchange", () => {
    render(
      <DexterUi_Context.ProviderInternal
        value={
          ...DexterUi_Context.defaultValue,
          balances: Fixtures_Balances.balances1,
        }>
        <DexterUi_RightColumn />
      </DexterUi_Context.ProviderInternal>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("Liquidity Pool", () => {
    render(
      <DexterUi_Context.ProviderInternal
        value={
          ...DexterUi_Context.defaultValue,
          balances: Fixtures_Balances.balances1,
          route: Dexter_Route.LiquidityPool(AddLiquidity),
        }>
        <DexterUi_RightColumn />
      </DexterUi_Context.ProviderInternal>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
});
