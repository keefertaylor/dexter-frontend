open Jest;
open Expect;
open ReactTestingLibrary;

describe("Snapshots", () => {
  test("Light mode", () => {
    render(
      <DexterUi_Context.ProviderInternal value=DexterUi_Context.defaultValue>
        <DexterUi_Content />
      </DexterUi_Context.ProviderInternal>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("Dark mode", () => {
    render(
      <DexterUi_Context.ProviderInternal
        value={...DexterUi_Context.defaultValue, darkMode: true}>
        <DexterUi_Content />
      </DexterUi_Context.ProviderInternal>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
});
