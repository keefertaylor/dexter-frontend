let account1: Dexter_Account.t = {
  address: Tezos.Address.zero,
  xtz: Tezos.Mutez.Mutez(100000000L),
  headBlock: {
    chainId: "",
    hash: "",
    header: {
      level: 0,
      predecessor: "",
      timestamp:
        Tezos_Timestamp.Timestamp(
          MomentRe.moment("2020-09-30T00:00:01.000Z"),
        ),
    },
    operations: [],
  },
  wallet: Tezos_Wallet.TezBridge,
};
