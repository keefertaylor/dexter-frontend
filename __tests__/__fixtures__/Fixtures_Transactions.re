let exchange: Dexter_Transaction.t = {
  block: "",
  contractId: "",
  hash: "",
  status: Applied,
  walletAddress: Tezos.Address.zero,
  timestamp:
    Tezos_Timestamp.Timestamp(MomentRe.moment("2020-09-30T00:00:01.000Z")),
  transactionType:
    Dexter_TransactionType.Exchange({
      valueIn: Mutez(Tezos.Mutez.Mutez(100000000L)),
      symbolIn: "XTZ",
      valueOut: Token(Tezos.Token.ofFloat(2.42321, 3)),
      symbolOut: "MTZ",
      destination: Tezos.Address.zero,
    }),
};

let exchangeAndSend: Dexter_Transaction.t = {
  block: "",
  contractId: "",
  hash: "",
  status: Pending,
  walletAddress: Tezos.Address.zero,
  timestamp:
    Tezos_Timestamp.Timestamp(MomentRe.moment("2020-09-30T00:00:01.000Z")),
  transactionType:
    Dexter_TransactionType.Exchange({
      valueIn: Token(Tezos.Token.ofFloat(2020.4, 2)),
      symbolIn: "TZT",
      valueOut: Mutez(Tezos.Mutez.Mutez(1000000L)),
      symbolOut: "XTZ",
      destination: Contract(Contract("")),
    }),
};

let addLiquidity: Dexter_Transaction.t = {
  block: "",
  contractId: "",
  hash: "",
  status: Failed,
  walletAddress: Tezos.Address.zero,
  timestamp:
    Tezos_Timestamp.Timestamp(MomentRe.moment("2020-09-30T00:00:01.000Z")),
  transactionType:
    Dexter_TransactionType.AddLiquidity({
      xtzIn: Tezos.Mutez.Mutez(100000000L),
      tokenIn: Tezos.Token.ofFloat(2020., 0),
      symbol: "TZG",
    }),
};

let removeLiquidity: Dexter_Transaction.t = {
  block: "",
  contractId: "",
  hash: "",
  status: Applied,
  walletAddress: Tezos.Address.zero,
  timestamp:
    Tezos_Timestamp.Timestamp(MomentRe.moment("2020-09-30T00:00:01.000Z")),
  transactionType:
    Dexter_TransactionType.RemoveLiquidity({
      xtzOut: Tezos.Mutez.Mutez(100000000L),
      tokenOut: Tezos.Token.ofFloat(2020., 0),
      lqtBurned: Tezos.Token.ofFloat(20000030., 0),
      symbol: "TZG",
    }),
};
