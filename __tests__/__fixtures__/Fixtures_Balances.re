let happyTezosAddress =
  Tezos.Address.Contract(Contract("tz1WCd2jm4uSt4vntk4vSuUWoZQGhLcDuR9q"));

let tzgExchangeBalance: Dexter_ExchangeBalance.t = {
  name: "",
  symbol: "TZG",
  icon: "",
  tokenContract: Tezos.Contract.Contract(""),
  dexterContract: Tezos.Contract.Contract(""),
  dexterBaker: Some(happyTezosAddress),
  tokenBalance: Tezos.Token.mkToken(Bigint.of_int64(100L), 0),
  lqtBalance: Tezos.Token.mkToken(Bigint.of_int64(233232L), 0),
  exchangeTotalLqt: Tezos.Token.mkToken(Bigint.of_int64(1000000000000L), 0),
  exchangeXtz: Tezos.Mutez.Mutez(100000000L),
  exchangeTokenBalance: Tezos.Token.mkToken(Bigint.of_int64(1000000L), 0),
  exchangeAllowanceForAccount: Tezos.Token.zero,
};

let tzgBalance = Dexter_Balance.ExchangeBalance(tzgExchangeBalance);

let mtzExchangeBalance: Dexter_ExchangeBalance.t = {
  name: "",
  symbol: "MTZ",
  icon: "",
  tokenContract: Tezos.Contract.Contract(""),
  dexterContract: Tezos.Contract.Contract(""),
  dexterBaker: None,
  tokenBalance: Tezos.Token.mkToken(Bigint.of_int64(100L), 3),
  lqtBalance: Tezos.Token.mkToken(Bigint.of_int64(232233L), 3),
  exchangeTotalLqt: Tezos.Token.mkToken(Bigint.of_int64(1000000000000L), 3),
  exchangeXtz: Tezos.Mutez.Mutez(4044215554L),
  exchangeTokenBalance: Tezos.Token.mkToken(Bigint.of_int64(61626L), 3),
  exchangeAllowanceForAccount: Tezos.Token.zero,
};

let mtzBalance = Dexter_Balance.ExchangeBalance(mtzExchangeBalance);

let tzgSmallExchangeBalance: Dexter_ExchangeBalance.t = {
  name: "",
  symbol: "TZG",
  icon: "",
  tokenContract: Tezos.Contract.Contract(""),
  dexterContract: Tezos.Contract.Contract(""),
  dexterBaker: None,
  lqtBalance: Tezos.Token.mkToken(Bigint.of_int64(233232L), 0),
  exchangeTokenBalance: Tezos.Token.mkToken(Bigint.of_int64(1000000L), 0),
  exchangeTotalLqt: Tezos.Token.mkToken(Bigint.of_int64(1000000000000L), 0),
  exchangeXtz: Tezos.Mutez.Mutez(200000L),
  exchangeAllowanceForAccount: Tezos.Token.zero,
  tokenBalance: Tezos.Token.mkToken(Bigint.of_int64(10000L), 0),
};

let tzgSmallBalance = Dexter_Balance.ExchangeBalance(tzgExchangeBalance);

let tztExchangeBalance: Dexter_ExchangeBalance.t = {
  name: "",
  symbol: "TZT",
  icon: "",
  tokenContract: Tezos.Contract.Contract(""),
  dexterContract: Tezos.Contract.Contract(""),
  dexterBaker: None,
  tokenBalance: Tezos.Token.mkToken(Bigint.of_int64(10000L), 2),
  lqtBalance: Tezos.Token.zero,
  exchangeTotalLqt:
    Tezos.Token.mkToken(Bigint.of_int64(100000000000000L), 0),
  exchangeXtz: Tezos.Mutez.Mutez(1010000000L),
  exchangeTokenBalance: Tezos.Token.mkToken(Bigint.of_int64(9990000L), 2),
  exchangeAllowanceForAccount: Tezos.Token.zero,
};

let tztBalance = Dexter_Balance.ExchangeBalance(tztExchangeBalance);

let tenTez = Tezos.Mutez.Mutez(10000000L);

let xtzTenTezBalance = Dexter_Balance.XtzBalance(tenTez);
let xtzZeroBalance = Dexter_Balance.XtzBalance(Tezos.Mutez.zero);
let xtzOneTezBalance = Dexter_Balance.XtzBalance(Tezos.Mutez.oneTez);

let balances1 = [mtzBalance, tztBalance, tzgBalance, xtzTenTezBalance];
