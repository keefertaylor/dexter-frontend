exception DecodeError(string);

module Network = {
  module Type = {
    module Raw = {
      type t = string;
    };

    type t =
      | Mainnet
      | Delphinet
      | Custom;

    let fromRaw = (raw: Raw.t): t =>
      switch (raw) {
      | "mainnet" => Mainnet
      | "delphinet" => Delphinet
      | "custom" => Custom
      | _ => raise @@ DecodeError("Expected Network.Type, got " ++ raw)
      };

    let toRaw = (t: t): Raw.t => {
      switch (t) {
      | Mainnet => "mainnet"
      | Delphinet => "delphinet"
      | Custom => "custom"
      };
    };
  };

  module Raw = {
    type t = {
      [@bs.as "type"]
      type_: string,
      [@bs.as "name"]
      name: Js.Nullable.t(string),
      [@bs.as "rpcUrl"]
      rpcUrl: Js.Nullable.t(string),
    };
  };

  type t = {
    type_: Type.t,
    name: option(string),
    rpcUrl: option(string),
  };

  let fromRaw = (raw: Raw.t): t => {
    type_: Type.fromRaw(raw.type_),
    name: Js.Nullable.toOption(raw.name),
    rpcUrl: Js.Nullable.toOption(raw.rpcUrl),
  };

  let toRaw = (t: t): Raw.t => {
    type_: Type.toRaw(t.type_),
    name: t.name |> Js.Nullable.fromOption,
    rpcUrl: t.rpcUrl |> Js.Nullable.fromOption,
  };
};

module Permission = {
  module Scope = {
    module Raw = {
      type t = string;
    };

    type t =
      | OperationRequest
      | Sign
      | Threshold;

    let fromRaw = (raw: Raw.t): t =>
      switch (raw) {
      | "operation_request" => OperationRequest
      | "sign" => Sign
      | "threshold" => Threshold
      | _ => raise @@ DecodeError("Expected Permissions.Scope, got " ++ raw)
      };
  };

  module Response = {
    module Raw = {
      type t = {
        [@bs.as "beaconId"]
        beaconId: string,
        [@bs.as "address"]
        address: string,
        [@bs.as "network"]
        network: Network.Raw.t,
        [@bs.as "scopes"]
        scopes: array(string),
      };
    };

    type t = {
      beaconId: string,
      address: string,
      network: Network.t,
      scopes: list(Scope.t),
    };

    let fromRaw = (raw: Raw.t): t => {
      beaconId: raw.beaconId,
      address: raw.address,
      network: Network.fromRaw(raw.network),
      scopes: Array.map(Scope.fromRaw, raw.scopes) |> Array.to_list,
    };
  };
};

module AccountInfo = {
  module Raw = {
    /* [@bs.deriving {abstract: light}] */
    type t = {
      [@bs.as "accountIdentifier"]
      accountIdentifier: string,
      [@bs.as "beaconId"]
      beaconId: string,
      [@bs.as "address"]
      address: string,
      [@bs.as "pubkey"]
      pubkey: string,
      [@bs.as "network"]
      network: Network.Raw.t,
      [@bs.as "scopes"]
      scopes: array(Permission.Scope.Raw.t),
    };
  };

  type t = {
    accountIdentifier: string,
    beaconId: string,
    address: string,
    pubkey: string,
    network: Network.t,
    scopes: list(Permission.Scope.t),
  };

  let fromRaw = (raw: Raw.t): t => {
    accountIdentifier: raw.accountIdentifier,
    beaconId: raw.beaconId,
    address: raw.address,
    pubkey: raw.pubkey,
    network: raw.network |> Network.fromRaw,
    scopes: Array.map(Permission.Scope.fromRaw, raw.scopes) |> Array.to_list,
  };

  let hasWrite = t =>
    List.mem(Permission.Scope.OperationRequest, t.scopes)
    && List.mem(Permission.Scope.Sign, t.scopes);
};

module Tezos = {
  module Transaction = {
    module Parameters = {
      module Raw = {
        [@bs.deriving {abstract: light}]
        type t = {
          [@bs.as "entrypoint"]
          entrypoint: string,
          [@bs.as "value"]
          value: Js.Json.t,
        };
      };

      type t = {
        entrypoint: string,
        value: Tezos.Expression.t,
      };

      let toRaw = (t: t): Raw.t =>
        Raw.t(
          ~entrypoint=t.entrypoint,
          ~value=Tezos.Expression.encode(t.value),
        );
    };
  };

  module Operation = {
    module Response = {
      module Raw = {
        type t = {
          [@bs.as "beaconId"]
          beaconId: string,
          [@bs.as "transactionHash"]
          transactionHash: string,
        };
      };

      type t = {
        beaconId: string,
        transactionHash: string,
      };

      let fromRaw = (raw: Raw.t): t => {
        beaconId: raw.beaconId,
        transactionHash: raw.transactionHash,
      };
    };

    module Type = {
      type t =
        | Endorsement
        | SeedNonceRevelation
        | DoubleEndorsementEvidence
        | DoubleBakingEvidence
        | ActivateAccount
        | Proposals
        | Ballot
        | Reveal
        | Transaction
        | Origination
        | Delegation;

      let toString = (t: t): string =>
        switch (t) {
        | Endorsement => "endorsement"
        | SeedNonceRevelation => "seed_nonce_revelation"
        | DoubleEndorsementEvidence => "double_endorsement_evidence"
        | DoubleBakingEvidence => "double_baking_evidence"
        | ActivateAccount => "activate_account"
        | Proposals => "proposals"
        | Ballot => "ballot"
        | Reveal => "reveal"
        | Transaction => "transaction"
        | Origination => "origination"
        | Delegation => "delegation"
        };
    };

    module Raw = {
      [@bs.deriving {abstract: light}]
      type t = {
        [@bs.as "kind"]
        kind: string,
        [@bs.as "amount"]
        amount: Js.Json.t,
        [@bs.as "destination"]
        destination: Js.Json.t,
        [@bs.as "parameters"]
        parameters: Js.Nullable.t(Transaction.Parameters.Raw.t),
      };
    };

    type t = {
      kind: Type.t,
      amount: Tezos.Mutez.t,
      destination: Tezos.Contract.t, /* it can be contract or address */
      parameters: option(Transaction.Parameters.t),
    };

    let toRaw = (t: t): Raw.t =>
      Raw.t(
        ~kind=Type.toString(t.kind),
        ~amount=Tezos.Mutez.encode(t.amount),
        ~destination=Tezos.Contract.encode(t.destination),
        ~parameters={
          switch (t.parameters) {
          | Some(p) => Js.Nullable.return(Transaction.Parameters.toRaw(p))
          | None => Js.Nullable.null
          };
        },
      );
  };
};

type dappClient;

let createDappClientRaw: string => dappClient = [%bs.raw
  {|
        function(dapp) {
          const beacon = require("@airgap/beacon-sdk");
          return new beacon.DAppClient({name: dapp});
        }
       |}
];

let createDappClient = (name: string): dappClient => {
  createDappClientRaw(name);
};

let postTransactionRaw:
  (dappClient, Network.Raw.t, array(Tezos_Operation.Raw.t)) =>
  Js.Promise.t(Tezos.Operation.Response.Raw.t) = [%bs.raw
  {|
        function(client, network, operations) {
          console.log(operations);
          return client
                .requestOperation({
                  network: network,
                  operationDetails: operations
                })
                .then(function (response) {
                  console.log('operations', response)
                  return response;
                })
                .catch(function (operationError) {console.error(operationError); return operationError;});
         }
       |}
];

let requestPermissionsRaw:
  (dappClient, Network.Raw.t) => Js.Promise.t(Permission.Response.Raw.t) = [%bs.raw
  {|
        function(client, network) {
          return client.requestPermissions({network: network});
         }
       |}
];

let requestPermissions =
    (dappClient, network): Js.Promise.t(Permission.Response.t) =>
  requestPermissionsRaw(dappClient, Network.toRaw(network))
  |> Js.Promise.then_(response =>
       Permission.Response.fromRaw(response) |> Js.Promise.resolve
     );

[@bs.send]
external requestOperation:
  (dappClient, Tezos.Operation.Raw.t) => Js.Promise.t(unit) =
  "requestOperation";

let getActiveAccountRaw:
  dappClient => Js.Promise.t(Js.Nullable.t(AccountInfo.Raw.t)) = [%bs.raw
  {|
        function(client) {
          return client.getActiveAccount();
        }
       |}
  ];

let disconnectRaw:
  dappClient => Js.Promise.t(unit) = [%bs.raw
  {|
        function(client) {
          return client.setActiveAccount(undefined);
        }
       |}
  ];

let disconnect = (dappClient): Js.Promise.t(unit) => {
  disconnectRaw(dappClient);
};

let getActiveAccount = (dappClient): Js.Promise.t(option(AccountInfo.t)) => {
  getActiveAccountRaw(dappClient)
  |> Js.Promise.then_(account =>
       (
         switch (Js.Nullable.toOption(account)) {
         | Some(a) => Some(AccountInfo.fromRaw(a))
         | None => None
         }
       )
       |> Js.Promise.resolve
     );
};

let postTransaction =
    (dappClient, operations: list(Tezos_Operation.t))
    : Js.Promise.t(option(Tezos.Operation.Response.t)) =>
  getActiveAccount(dappClient)
  |> Js.Promise.then_(account =>
       switch (account) {
       | Some((account: AccountInfo.t)) =>
         postTransactionRaw(
           dappClient,
           Network.toRaw(account.network),
           Belt.List.map(operations, Tezos_Operation.toRaw) |> Array.of_list,
         )
         |> Js.Promise.then_(response =>
              Js.Promise.resolve(
                Some(Tezos.Operation.Response.fromRaw(response)),
              )
            )
       | None => Js.Promise.resolve(None)
       }
     );

let hasRequiredPermissions = (account: AccountInfo.t) => {
  List.mem(Permission.Scope.OperationRequest, account.scopes)
  && List.mem(Permission.Scope.Sign, account.scopes);
};
