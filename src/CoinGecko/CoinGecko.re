let decode = (json: Js.Json.t): Belt.Result.t(list(Currency.t), string) => {
  switch (
    json
    |> Json.Decode.field("tezos", internal_ => {
         Currency_Dict.list
         |> List.map((currency: Currency.t) =>
              switch (
                internal_
                |> Json.Decode.optional(
                     Json.Decode.field(
                       currency.symbol |> Js.String.toLowerCase,
                       Json.Decode.float,
                     ),
                   )
              ) {
              | Some(rate) => Some({...currency, rate})
              | _ => None
              }
            )
         |> Array.of_list
         |> Utils.filterNone
         |> Array.to_list
       })
  ) {
  | currenciesState => Belt.Result.Ok(currenciesState)
  | exception (Json.Decode.DecodeError(err)) => Belt.Result.Error(err)
  };
};

let fetch = (): Js.Promise.t(Belt.Result.t(list(Currency.t), string)) => {
  let url =
    "https://api.coingecko.com/api/v3/simple/price?ids=tezos&vs_currencies="
    ++ (
      Currency_Dict.list
      |> List.map((currency: Currency.t) => currency.symbol)
      |> Array.of_list
      |> Js.Array.joinWith(",")
    );
  Js.Promise.(
    Fetch.fetchWithInit(
      url,
      Fetch.RequestInit.make(
        ~method_=Get,
        ~headers=
          Fetch.HeadersInit.makeWithArray([|
            ("Accept", "application/json"),
          |]),
        ~credentials=Omit,
        (),
      ),
    )
    |> then_(Fetch.Response.json)
    |> then_(json => resolve(decode(json)))
    |> catch(error => {
         Js.Console.log(error);
         ErrorReporting.Sentry.capturePromiseError(
           "CoinGecko.fetch failed. ",
           error,
         );
         resolve(
           Belt.Result.Error("CoinGecko.fetch failed. Promise error."),
         );
       })
  );
};
