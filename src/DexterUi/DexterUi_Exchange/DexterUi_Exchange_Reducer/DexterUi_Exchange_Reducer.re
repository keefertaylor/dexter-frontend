open DexterUi_Exchange_Reducer_Utils;

type state = {
  inputValue: Valid.t(InputType.t),
  outputValue: Valid.t(InputType.t),
  inputToken: Dexter.Balance.t,
  outputToken: Dexter.Balance.t,
};

type action =
  | UpdateInputValue(Valid.t(InputType.t))
  | UpdateOutputValue(Valid.t(InputType.t))
  | UpdateTokens(Dexter.Balance.t, Dexter.Balance.t)
  | ResetInputs
  | ResetTokens;

let useExchangeReducer = balances => {
  let (t1, t2) = DexterUi_Hooks.useTokensFromLocalStorage(balances);
  let xtzBalance = balances |> Dexter.Balance.getXtz;
  let firstTokenBalance = balances |> Dexter.Balance.getFirstToken;

  let initialInputToken = t1 |> Utils.orDefault(xtzBalance);
  let initialOutputToken = t2 |> Utils.orDefault(firstTokenBalance);

  let initialState = {
    inputValue: initialInputToken |> Dexter.Balance.getInitialValue,
    outputValue: initialOutputToken |> Dexter.Balance.getInitialValue,
    inputToken: initialInputToken,
    outputToken: initialOutputToken,
  };

  React.useReducer(
    (state, action) =>
      switch (action) {
      | UpdateTokens(inputToken, outputToken) => {
          inputToken,
          outputToken,
          inputValue:
            inputToken !== state.inputToken
              ? Dexter_Value.getZeroInputValue(inputToken) : state.inputValue,
          outputValue:
            inputToken !== state.inputToken
              ? Dexter_Value.getZeroInputValue(outputToken)
              : calculateOutput(inputToken, state.inputValue, outputToken),
        }
      | UpdateInputValue(inputValue) => {
          ...state,
          inputValue,
          outputValue:
            calculateOutput(state.inputToken, inputValue, state.outputToken),
        }
      | UpdateOutputValue(outputValue) => {
          ...state,
          outputValue,
          inputValue:
            calculateInput(state.inputToken, state.outputToken, outputValue),
        }
      | ResetInputs => {
          ...state,
          inputValue: initialInputToken |> Dexter.Balance.getInitialValue,
          outputValue: initialOutputToken |> Dexter.Balance.getInitialValue,
        }
      | ResetTokens => {
          ...state,
          inputToken: initialInputToken,
          outputToken: initialOutputToken,
        }
      },
    initialState,
  );
};
