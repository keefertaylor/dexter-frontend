/**
 * pair %xtzToToken (address :to) (pair (nat :minTokensBought) (timestamp :deadline))
 */

let encodeXtzToToken =
    (
      to_: Tezos.Address.t,
      minTokensRequired: Tezos.Token.t,
      deadline: Tezos.Timestamp.t,
    ) =>
  Tezos.(
    Expression.SingleExpression(
      Primitives.PrimitiveData(PrimitiveData.Pair),
      Some([
        Expression.StringExpression(Tezos.Address.toString(to_)),
        Expression.SingleExpression(
          Primitives.PrimitiveData(PrimitiveData.Pair),
          Some([
            Expression.IntExpression(minTokensRequired.value),
            Expression.StringExpression(Tezos.Timestamp.toString(deadline)),
          ]),
          None,
        ),
      ]),
      None,
    )
  );

/**
 * pair %tokenToXtz (pair (address :owner) (address :to)) (pair (nat :tokensSold) (pair (mutez :minXtzBought) (timestamp :deadline)))
 */

let encodeTokenToXtz =
    (
      owner: Tezos.Address.t,
      to_: Tezos.Address.t,
      tokensSold: Tezos.Token.t,
      minXtzBought: Tezos.Mutez.t,
      deadline: Tezos.Timestamp.t,
    ) =>
  Tezos.(
    Expression.SingleExpression(
      Primitives.PrimitiveData(PrimitiveData.Pair),
      Some([
        Expression.SingleExpression(
          Primitives.PrimitiveData(PrimitiveData.Pair),
          Some([
            Expression.StringExpression(Tezos.Address.toString(owner)),
            Expression.StringExpression(Tezos.Address.toString(to_)),
          ]),
          None,
        ),
        Expression.SingleExpression(
          Primitives.PrimitiveData(PrimitiveData.Pair),
          Some([
            Expression.IntExpression(tokensSold.value),
            Expression.SingleExpression(
              Primitives.PrimitiveData(PrimitiveData.Pair),
              Some([
                Expression.IntExpression(Tezos.Mutez.toBigint(minXtzBought)),
                Expression.StringExpression(
                  Tezos.Timestamp.toString(deadline),
                ),
              ]),
              None,
            ),
          ]),
          None,
        ),
      ]),
      None,
    )
  );

let xtzToToken =
    (
      contract: Tezos.Contract.t,
      to_: Tezos.Address.t,
      mutez: Tezos.Mutez.t,
      minTokensRequired: Tezos.Token.t,
      deadline: Tezos.Timestamp.t,
      wallet: Tezos_Wallet.t,
    )
    : Js.Promise.t(Belt.Result.t(Tezos_Wallet.Operation.Response.t, string)) => {
  switch (wallet) {
  | TezBridge =>
    TezBridge.postTransactions([
      {
        kind: Tezos_Operation.Kind.Transaction,
        destination: contract,
        amount: mutez,
        parameters:
          Some({
            entrypoint: "xtzToToken",
            value: encodeXtzToToken(to_, minTokensRequired, deadline),
          }),
      },
    ])
    |> Js.Promise.then_(result =>
         Belt.Result.Ok(Tezos_Wallet.Operation.Response.TezBridge(result))
         |> Js.Promise.resolve
       )
  | Beacon(client) =>
    Beacon.postTransaction(
      client,
      [
        {
          kind: Tezos_Operation.Kind.Transaction,
          destination: contract,
          amount: mutez,
          parameters:
            Some({
              entrypoint: "xtzToToken",
              value: encodeXtzToToken(to_, minTokensRequired, deadline),
            }),
        },
      ],
    )
    |> Js.Promise.then_(result => {
         switch (result) {
         | None =>
           Belt.Result.Error("network not found") |> Js.Promise.resolve
         | Some(result) =>
           Belt.Result.Ok(Tezos_Wallet.Operation.Response.Beacon(result))
           |> Js.Promise.resolve
         }
       })

  };
};

let tokenToXtz =
    (
      ~dexterContract: Tezos.Contract.t,
      ~tokenContract: Tezos.Contract.t,
      ~owner: Tezos.Address.t,
      ~to_: Tezos.Address.t,
      ~tokensSold: Tezos.Token.t,
      ~minXtzBought: Tezos.Mutez.t,
      ~deadline: Tezos.Timestamp.t,
      ~wallet: Tezos_Wallet.t,
      ~dexterAllowanceForOwner: Tezos.Token.t,
      (),
    )
    : Js.Promise.t(Belt.Result.t(Tezos_Wallet.Operation.Response.t, string)) => {
  switch (wallet) {
  | TezBridge =>
    TezBridge.postTransactions(
      List.append(
        Token_Approve.mkApproveOperations(
          ~dexterAllowanceForOwner,
          ~tokenContract,
          ~dexterContract,
          ~approveAmount=tokensSold,
          (),
        ),
        [
          {
            kind: Tezos_Operation.Kind.Transaction,
            destination: dexterContract,
            amount: Tezos.Mutez.zero,
            parameters:
              Some({
                entrypoint: "tokenToXtz",
                value:
                  encodeTokenToXtz(
                    owner,
                    to_,
                    tokensSold,
                    minXtzBought,
                    deadline,
                  ),
              }),
          },
        ],
      ),
    )
    |> Js.Promise.then_(result =>
         Belt.Result.Ok(Tezos_Wallet.Operation.Response.TezBridge(result))
         |> Js.Promise.resolve
       )

  | Beacon(client) =>
    Beacon.postTransaction(
      client,
      List.append(
        Token_Approve.mkApproveOperations(
          ~dexterAllowanceForOwner,
          ~tokenContract,
          ~dexterContract,
          ~approveAmount=tokensSold,
          (),
        ),
        [
          {
            kind: Tezos_Operation.Kind.Transaction,
            destination: dexterContract,
            amount: Tezos.Mutez.zero,
            parameters:
              Some({
                entrypoint: "tokenToXtz",
                value:
                  encodeTokenToXtz(
                    owner,
                    to_,
                    tokensSold,
                    minXtzBought,
                    deadline,
                  ),
              }),
          },
        ],
      ),
    )
    |> Js.Promise.then_(result => {
         switch (result) {
         | None =>
           Belt.Result.Error("network not found") |> Js.Promise.resolve
         | Some(result) =>
           Belt.Result.Ok(Tezos_Wallet.Operation.Response.Beacon(result))
           |> Js.Promise.resolve
         }
       })

  };
};

let onExchange =
    (
      account: Dexter_Account.t,
      pushTransaction: Dexter_Transaction.t => unit,
      resetInputs: unit => unit,
      sendTo: bool,
      sendToAddress: string,
      slippageRate: option(float),
      state: DexterUi_Exchange_Reducer.state,
      transactionTimeout: int,
    ) => {
  let owner = account.address;
  let deadline = Tezos.Timestamp.minutesFromNow(transactionTimeout);
  let destination =
    sendTo
      ? switch (sendToAddress |> Tezos_Address.ofString) {
        | Belt.Result.Ok(s) => s
        | Belt.Result.Error(_e) => owner
        }
      : owner;

  switch (
    state.inputValue,
    state.outputValue,
    state.inputToken,
    state.outputToken,
    slippageRate,
  ) {
  | (
      Valid(Mutez(inputValue), _),
      Valid(Token(outputValue), _),
      _,
      ExchangeBalance(outputToken),
      Some(slippageRate),
    ) =>
    /* exchange xtz to token */
    let dexterContract = outputToken.dexterContract;
    let xtz = inputValue;
    let minTokensRequired =
      Dexter_Exchange.minimumTokenOut(
        outputValue,
        max(slippageRate, Dexter_LocalStorage.MaximumSlippage.get()),
      );

    xtzToToken(
      dexterContract,
      destination,
      xtz,
      minTokensRequired,
      deadline,
      account.wallet,
    )
    |> Js.Promise.then_(result => {
         switch (
           (result: Belt.Result.t(Tezos_Wallet.Operation.Response.t, string))
         ) {
         | Belt.Result.Ok(response) =>
           resetInputs();

           Dexter_Transaction.ofTransactionResponse(
             dexterContract,
             response,
             account.address,
             Dexter_TransactionType.Exchange({
               destination,
               symbolIn: "XTZ",
               symbolOut: outputToken.symbol,
               valueIn: InputType.Mutez(xtz),
               valueOut: InputType.Token(minTokensRequired),
             }),
           )
           |> Js.Promise.then_(
                (result: Belt.Result.t(Dexter_Transaction.t, string)) => {
                switch (result) {
                | Belt.Result.Ok(transaction) => pushTransaction(transaction)
                | Belt.Result.Error(error) => Js.log(error)
                };

                Js.Promise.resolve();
              })
           |> Js.Promise.catch(error => {
                Js.log(error);
                ErrorReporting.Sentry.capturePromiseError(
                  "DexterUi_ExchangeButton.xtzToToken.ofTransactionResponse failed. ",
                  error,
                );
                Js.Promise.resolve();
              })
           |> ignore;

         | Belt.Result.Error(error) => Js.log(error)
         };

         Js.Promise.resolve();
       })
    |> Js.Promise.catch(error => {
         Js.log(error);
         ErrorReporting.Sentry.capturePromiseError(
           "DexterUi_ExchangeButton.xtzToToken failed. ",
           error,
         );
         Js.Promise.resolve();
       })
    |> ignore;

  | (
      Valid(Token(inputValue), _),
      Valid(Mutez(outputValue), _),
      ExchangeBalance(inputToken),
      _,
      Some(slippageRate),
    ) =>
    /* exchange token to xtz */
    let dexterContract = inputToken.dexterContract;
    let tokensSold = inputValue;
    let minXtzBought =
      Dexter_Exchange.minimumXtzOut(
        outputValue,
        max(slippageRate, Dexter_LocalStorage.MaximumSlippage.get()),
      );

    tokenToXtz(
      ~dexterContract,
      ~tokenContract=inputToken.tokenContract,
      ~owner,
      ~to_=destination,
      ~tokensSold,
      ~minXtzBought,
      ~deadline,
      ~wallet=account.wallet,
      ~dexterAllowanceForOwner=inputToken.exchangeAllowanceForAccount,
      (),
    )
    |> Js.Promise.then_(result => {
         switch (
           (result: Belt.Result.t(Tezos_Wallet.Operation.Response.t, string))
         ) {
         | Belt.Result.Ok(response) =>
           resetInputs();

           Dexter_Transaction.ofTransactionResponse(
             dexterContract,
             response,
             account.address,
             Dexter_TransactionType.Exchange({
               destination,
               symbolIn: inputToken.symbol,
               symbolOut: "XTZ",
               valueIn: InputType.Token(tokensSold),
               valueOut: InputType.Mutez(minXtzBought),
             }),
           )
           |> Js.Promise.then_(result => {
                switch (result) {
                | Belt.Result.Ok(transaction) => pushTransaction(transaction)
                | Belt.Result.Error(error) => Js.log(error)
                };
                Js.Promise.resolve();
              })
           |> Js.Promise.catch(error => {
                Js.log(error);
                ErrorReporting.Sentry.capturePromiseError(
                  "DexterUi_ExchangeButton.tokenToXtz.ofTransactionResponse failed. ",
                  error,
                );
                Js.Promise.resolve();
              })
           |> ignore;
         | Belt.Result.Error(error) => Js.log(error)
         };

         Js.Promise.resolve();
       })
    |> Js.Promise.catch(error => {
         Js.log(error);
         ErrorReporting.Sentry.capturePromiseError(
           "DexterUi_ExchangeButton.tokenToXtz failed. ",
           error,
         );
         Js.Promise.resolve();
       })
    |> ignore;

  | _ => ()
  };
};

let getSecondToken =
    (
      token: Dexter_Balance.t,
      t1: Dexter_Balance.t,
      t2: Dexter_Balance.t,
      xtzBalance: Dexter_Balance.t,
    )
    : Dexter_Balance.t =>
  switch (token) {
  | ExchangeBalance(_) =>
    switch (t2) {
    | ExchangeBalance(_) => xtzBalance
    | _ => t2
    }
  | XtzBalance(_) => t1
  };
