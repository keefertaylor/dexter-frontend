let checkInputsInvalid =
    (
      account: option(Dexter_Account.t),
      inputValue: Valid.t(InputType.t),
      inputToken: Dexter_Balance.t,
      outputValue: Valid.t(InputType.t),
      outputToken: Dexter_Balance.t,
    ) => {
  switch (account, inputValue, outputValue) {
  | (Some(account), Valid(inputValue, _), Valid(outputValue, _)) =>
    switch (inputValue, inputToken, outputValue, outputToken) {
    | (
        Mutez(inputValue),
        XtzBalance(_),
        Token(outputValue),
        ExchangeBalance(outputToken),
      ) =>
      Tezos.Mutez.leqZero(inputValue)
      || Tezos.Token.leqZero(outputValue)
      || Tezos.Mutez.(gt(inputValue, userMax(account.xtz)))
      || Tezos.Token.gt(outputValue, outputToken.exchangeTokenBalance)
    /* maxXto token */
    | (
        Token(inputValue),
        ExchangeBalance(inputToken),
        Mutez(outputValue),
        XtzBalance(_),
      ) =>
      Tezos.Token.leqZero(inputValue)
      || Tezos.Mutez.leqZero(outputValue)
      || Tezos.Token.gt(inputValue, inputToken.tokenBalance)
      || Tezos.Mutez.gt(outputValue, inputToken.exchangeXtz)
    /* token to maxX*/
    | _ => true /* you cannot trade when both are XTZ or both are Token */
    }

  | _ => true
  };
};

let checkExceedsSlippageRate =
    (slippageRate: option(float), slippageChoice: Dexter_Slippage.t) =>
  slippageRate
  |> Utils.map(slippage =>
       slippage >= 50.0 || slippage > Dexter_Slippage.toFloat(slippageChoice)
     )
  |> Utils.orDefault(true);
