open DexterUi_ExchangeControls_Utils;

[@react.component]
let make =
    (~state: DexterUi_Exchange_Reducer.state, ~resetInputs: unit => unit) => {
  let {account, transactionStatus}: DexterUi_Context.t =
    DexterUi_Context.useContext();
  let (sendTo, setSendTo) = React.useState(_ => false);
  let (sendToAddress, setSendToAddress) = React.useState(_ => "");
  let (showAdvancedOptions, setShowAdvancedOptions) =
    React.useState(_ => false);
  let (slippageChoice, setSlippageChoice) =
    React.useState(_ =>
      Dexter_LocalStorage.MaximumSlippage.get() |> Dexter_Slippage.ofFloat
    );
  let (slippageRate, setSlippageRate) = React.useState(_ => None);

  let resetInputs =
    React.useCallback0(() => {
      setSendTo(_ => false);
      resetInputs();
    });

  /* monitor slippage choice */
  React.useEffect1(
    () => {
      let newSlippage =
        switch (slippageChoice) {
        | PointOne => Some(0.1)
        | Half => Some(0.5)
        | One => Some(1.0)
        | Custom(Valid(s, _)) => Some(s)
        | Custom(_) => None
        };

      switch (newSlippage) {
      | Some(slippage) => Dexter_LocalStorage.MaximumSlippage.set(slippage)
      | None => ()
      };
      None;
    },
    [|slippageChoice|],
  );

  React.useEffect1(
    () => {
      if (!sendTo) {
        setSendToAddress(_ => "");
      };
      None;
    },
    [|sendTo|],
  );

  /* monitor input values to calculate the slippage rate */
  React.useEffect3(
    () => {
      setSlippageRate(_ =>
        switch (state.inputValue, state.outputValue) {
        | (Valid(Mutez(inputValue), _), Valid(_, _)) =>
          switch (state.outputToken) {
          | ExchangeBalance(outputToken) =>
            Dexter_Exchange.xtzToTokenSlippageDisplay(
              inputValue,
              outputToken.exchangeXtz,
              outputToken.exchangeTokenBalance,
            )
          | _ => None
          }

        | (Valid(Token(inputValue), _), Valid(_, _)) =>
          switch (state.inputToken) {
          | ExchangeBalance(inputToken) =>
            Dexter_Exchange.tokenToXtzSlippageForDisplay(
              inputValue,
              inputToken.exchangeXtz,
              inputToken.exchangeTokenBalance,
            )
          | _ => None
          }
        | _ => None
        }
      );

      None;
    },
    (state.inputValue, state.inputToken, state.outputToken),
  );

  let exceedsSlippageRate =
    checkExceedsSlippageRate(slippageRate, slippageChoice);

  let inputsInvalid =
    checkInputsInvalid(
      account,
      state.inputValue,
      state.inputToken,
      state.outputValue,
      state.outputToken,
    );

  let accountWalletAddress =
    account
    |> Utils.map((account: Dexter_Account.t) =>
         Tezos.Address.toString(account.address)
       )
    |> Utils.orDefault("");

  let invalidAddress =
    sendTo
    && (
      sendToAddress
      |> Tezos.Address.ofString
      |> Belt.Result.isError
      || sendToAddress === accountWalletAddress
    );

  let hasInsufficientXtz = DexterUi_Hooks.useHasInsufficientXtz();

  let isExchangeDisabled =
    transactionStatus === Pending
    || exceedsSlippageRate
    || inputsInvalid
    || invalidAddress
    || hasInsufficientXtz;

  <Flex flexGrow=1. flexDirection=`column alignItems=`center pb={`px(20)}>
    <DexterUi_ExchangeRate
      inputValue={state.inputValue}
      inputToken={state.inputToken}
      outputToken={state.outputToken}
    />
    <DexterUi_ExchangeAdvancedOptions
      setSlippageChoice
      slippageChoice
      showAdvancedOptions
      setShowAdvancedOptions
    />
    <DexterUi_ExchangeSendToAddress
      invalidAddress
      outputToken={state.outputToken}
      sendTo
      setSendTo
      sendToAddress
      setSendToAddress
    />
    {hasInsufficientXtz
       ? <DexterUi_InsufficientXtzMessage />
       : <DexterUi_ExchangeSlippageInfo
           slippageChoice
           slippageRate
           setShowAdvancedOptions
         />}
    <DexterUi_ExchangeDescription
      expectedSlippageRate=slippageRate
      isExchangeDisabled
      slippageChoice
      state
    />
    <DexterUi_ExchangeButton
      isExchangeDisabled
      state
      slippageRate
      sendTo
      sendToAddress
      resetInputs
    />
  </Flex>;
};
