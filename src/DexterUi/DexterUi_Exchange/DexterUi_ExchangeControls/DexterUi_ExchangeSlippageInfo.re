[@react.component]
let make =
    (
      ~slippageChoice: Dexter_Slippage.t,
      ~slippageRate: option(float),
      ~setShowAdvancedOptions: (bool => bool) => unit,
    ) => {
  let slippageRate: float = slippageRate |> Utils.orDefault(0.);

  let isZero = slippageRate === 0.;
  let isAboveLimit =
    slippageRate > (slippageChoice |> Dexter_Slippage.toFloat);
  let isGe50 = slippageRate >= 50.;

  let (text, variant): (React.element, DexterUi_Panel.t) =
    switch (isGe50, isAboveLimit) {
    | (true, _) => (
        "- This exchange exceeds 50% price slippage and cannot be executed."
        |> React.string,
        Error,
      )
    | (_, true) => (
        <>
          {"- This exchange will exceed "
           ++ Dexter_Slippage.toString(slippageChoice)
           ++ " slippage. Please "
           |> React.string}
          <Flex
            onClick={_ => setShowAdvancedOptions(_ => true)} inlineFlex=true>
            <span
              className=Css.(
                style([textDecoration(`underline), ...TextStyles.default])
              )>
              {"update your slippage allowance" |> React.string}
            </span>
          </Flex>
          {" to proceed." |> React.string}
        </>,
        Warning,
      )
    | _ => (React.null, Info)
    };

  let isHidden = slippageRate |> Js.Float.isNaN || isZero;

  <Flex
    className=Css.(
      style([
        opacity(isHidden ? 0. : 1.),
        visibility(isHidden ? `hidden : `visible),
      ])
    )
    mt={`px(6)}
    flexGrow=1.
    justifyContent=`center>
    <DexterUi_Panel variant>
      Common.iInCircle
      Common.nbsp
      <span className=Css.(style([fontWeight(bold)]))>
        {" Estimated slippage: "
         ++ Printf.sprintf("%.2f", slippageRate)
         ++ "% "
         |> React.string}
      </span>
      text
    </DexterUi_Panel>
  </Flex>;
};
