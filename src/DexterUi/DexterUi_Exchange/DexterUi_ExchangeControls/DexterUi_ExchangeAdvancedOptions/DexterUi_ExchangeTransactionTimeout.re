[@react.component]
let make =
    (
      ~localTransactionTimeout: int,
      ~setLocalTransactionTimeout: (int => int) => unit,
    ) => {
  let isInvalid = localTransactionTimeout > 1440;

  <Flex alignItems=`center>
    <DexterUi_SecondaryInput
      onBlur={_ =>
        if (localTransactionTimeout < 1) {
          setLocalTransactionTimeout(_ => 1);
        }
      }
      onChange={ev => {
        let s = ev |> Common.eventToValue;
        let s =
          localTransactionTimeout === 0 ? s |> Js.String.replace("0", "") : s;

        switch (s |> int_of_string_opt) {
        | Some(localTransactionTimeout) =>
          setLocalTransactionTimeout(_ => localTransactionTimeout)
        | None when s === "" => setLocalTransactionTimeout(_ => 0)
        | None => ()
        };
      }}
      inputColor=?{isInvalid ? Some(Colors.red) : None}
      inputBorderColor=?{isInvalid ? Some(Colors.red) : None}
      placeholder="20"
      noDarkMode=true
      inputBackground=Colors.white
      inputTextAlign=`right
      value={localTransactionTimeout |> string_of_int}
      width={`px(40)}
    />
    <Flex width={`px(8)} />
    <Text color=Colors.grey1> {"Minutes" |> React.string} </Text>
  </Flex>;
};
