[@react.component]
let make =
    (
      ~localSlippageChoice: Dexter_Slippage.t,
      ~setLocalSlippageChoice:
         (Dexter_Slippage.t => Dexter_Slippage.t) => unit,
    ) =>
  <Flex alignItems=`center>
    <DexterUi_ExchangeSlippageOption
      option=Dexter_Slippage.PointOne
      slippageChoice=localSlippageChoice
      setSlippageChoice=setLocalSlippageChoice
    />
    <DexterUi_ExchangeSlippageOption
      option=Dexter_Slippage.Half
      slippageChoice=localSlippageChoice
      setSlippageChoice=setLocalSlippageChoice
    />
    <DexterUi_ExchangeSlippageOption
      option=Dexter_Slippage.One
      slippageChoice=localSlippageChoice
      setSlippageChoice=setLocalSlippageChoice
    />
    <DexterUi_ExchangeSlippageOptionsCustom
      slippageChoice=localSlippageChoice
      setSlippageChoice=setLocalSlippageChoice
    />
  </Flex>;
