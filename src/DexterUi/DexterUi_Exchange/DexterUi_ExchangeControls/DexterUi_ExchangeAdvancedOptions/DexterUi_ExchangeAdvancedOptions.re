[@react.component]
let make =
    (
      ~setSlippageChoice: (Dexter_Slippage.t => Dexter_Slippage.t) => unit,
      ~slippageChoice: Dexter_Slippage.t,
      ~showAdvancedOptions: bool,
      ~setShowAdvancedOptions: (bool => bool) => unit,
    ) => {
  let {transactionTimeout, setTransactionTimeout}: DexterUi_Context.t =
    DexterUi_Context.useContext();
  let (localTransactionTimeout, setLocalTransactionTimeout) =
    React.useState(_ => transactionTimeout);
  let (localSlippageChoice, setLocalSlippageChoice) =
    React.useState(_ => slippageChoice);

  React.useEffect1(
    () => {
      if (!showAdvancedOptions) {
        setLocalTransactionTimeout(_ => transactionTimeout);
        setLocalSlippageChoice(_ => slippageChoice);
      };
      None;
    },
    [|showAdvancedOptions|],
  );

  <Flex mt={`px(10)} justifyContent=`center position=`relative>
    <Flex onClick={_ => setShowAdvancedOptions(_ => true)}>
      <Text
        darkModeColor=Colors.primaryDark
        lightModeColor=Colors.blue
        textDecoration=`underline>
        {"Advanced options" |> React.string}
      </Text>
    </Flex>
    {showAdvancedOptions
     |> Utils.renderIf(
          <DexterUi_ExchangeAdvancedOptionsModal>
            <DexterUi_ExchangeAdvancedOptionsTitle
              title={"Limit price slippage" |> React.string}
              tooltipText={
                "Some price slippage may occur on large orders. Limiting price slippage may protect your trade from acute price movement, however your order is more likely to fail due to normal price changes."
                |> React.string
              }
            />
            <Flex height={`px(10)} />
            <DexterUi_ExchangeSlippageOptions
              localSlippageChoice
              setLocalSlippageChoice
            />
            <Flex height={`px(18)} />
            <DexterUi_ExchangeAdvancedOptionsTitle
              title={"Transaction timeout" |> React.string}
              tooltipText={
                "Dexter will revert your transaction if it has not been completed in this timeframe."
                |> React.string
              }
            />
            <Flex height={`px(5)} />
            <DexterUi_ExchangeTransactionTimeout
              localTransactionTimeout
              setLocalTransactionTimeout
            />
            <Flex
              alignItems=`center
              mt={`px(16)}
              pr={`px(10)}
              justifyContent=`flexEnd>
              <DexterUi_Button
                disabled={
                  switch (localSlippageChoice) {
                  | Custom(Invalid(_))
                  | Custom(InvalidInitialState) => true
                  | _ => localTransactionTimeout > 1440
                  }
                }
                small=true
                onClick={_ => {
                  setSlippageChoice(_ => localSlippageChoice);
                  setTransactionTimeout(localTransactionTimeout);
                  setShowAdvancedOptions(_ => false);
                }}
                width={`px(112)}>
                {"Save changes" |> React.string}
              </DexterUi_Button>
              <Flex width={`px(16)} />
              <Flex onClick={_ => setShowAdvancedOptions(_ => false)}>
                <Text
                  darkModeColor=Colors.primaryDark
                  lightModeColor=Colors.blue
                  textDecoration=`underline>
                  {"Cancel" |> React.string}
                </Text>
              </Flex>
            </Flex>
          </DexterUi_ExchangeAdvancedOptionsModal>,
        )}
  </Flex>;
};
