[@react.component]
let make = (~children: React.element) => {
  <>
    <Flex
      position=`fixed
      background={`rgba((0, 0, 0, `num(0.33)))}
      width={`percent(100.)}
      height={`percent(100.)}
      top=`zero
      left=`zero
      zIndex=4
    />
    <Flex
      position=`absolute
      p={`px(20)}
      background=Colors.offWhite
      borderRadius={`px(16)}
      flexDirection=`column
      minWidth={`px(263)}
      top={`px(-36)}
      zIndex=5>
      children
    </Flex>
  </>;
};
