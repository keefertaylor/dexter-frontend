[@react.component]
let make = () => {
  let {balances}: DexterUi_Context.t = DexterUi_Context.useContext();

  let (state, dispatch) =
    DexterUi_Exchange_Reducer.useExchangeReducer(balances);

  DexterUi_Hooks.useResetting(
    () => dispatch(ResetTokens),
    () => dispatch(ResetInputs),
  );

  let updateInputToken =
    React.useCallback1(
      (inputToken: Dexter.Balance.t) => {
        let outputToken =
          DexterUi_Exchange_Utils.getSecondToken(
            inputToken,
            state.inputToken,
            state.outputToken,
            balances |> Dexter_Balance.getXtz,
          );

        dispatch(UpdateTokens(inputToken, outputToken));
        Dexter_Route.redirectToTokensHash(inputToken, outputToken);
      },
      [|state.inputToken, state.outputToken|],
    );

  let updateOutputToken =
    React.useCallback1(
      (outputToken: Dexter.Balance.t) => {
        let inputToken =
          DexterUi_Exchange_Utils.getSecondToken(
            outputToken,
            state.outputToken,
            state.inputToken,
            balances |> Dexter_Balance.getXtz,
          );

        dispatch(UpdateTokens(inputToken, outputToken));
        Dexter_Route.redirectToTokensHash(inputToken, outputToken);
      },
      [|state.inputToken, state.outputToken|],
    );

  let onClickMax = {
    let inputValue =
      switch (state.inputToken, state.outputToken) {
      | (XtzBalance(xtz), ExchangeBalance(_))
          when Tezos.Mutez.(gtZero(userMax(xtz))) =>
        Some(Dexter_Value.getMutezInputValue(Tezos.Mutez.userMax(xtz)))
      | (ExchangeBalance(token), XtzBalance(_))
          when Tezos.Token.(gtZero(token.tokenBalance)) =>
        Some(Dexter_Value.getTokenInputValue(token.tokenBalance))
      | _ => None
      };

    inputValue
    |> Utils.map((inputValue, _) => dispatch(UpdateInputValue(inputValue)));
  };

  let showRequiredXtzReserveNote =
    DexterUi_Hooks.useShowRequiredXtzReserveNote(
      switch (state.inputValue) {
      | Valid(Mutez(xtzValue), _) => Some(xtzValue)
      | _ => None
      },
    );

  <Flex flexDirection=`column px={Css.px(16)} pt={Css.px(4)} flexGrow=1.>
    <Flex>
      <DexterUi_ExchangeColumn
        minHeight={`px(179)}
        title={"Exchange:" |> React.string}
        token={state.inputToken}
        tokenSearchSide=Side.Left
        tokenSearchBalances=balances
        tokenSearchOnChange=updateInputToken
        balanceInfo=[
          "My balance: "
          ++ Dexter_Balance.getAccountBalanceAsString(state.inputToken),
        ]>
        <DexterUi_TokenInput
          note=?{
            showRequiredXtzReserveNote
              ? Some(
                  Tezos.Mutez.(requiredXtzReserve |> toTezStringWithCommas)
                  ++ " XTZ must be reserved to pay network fees.",
                )
              : None
          }
          token={state.inputToken}
          value={state.inputValue}
          updateValue={v => dispatch(UpdateInputValue(v))}
          ?onClickMax
        />
      </DexterUi_ExchangeColumn>
      <DexterUi_ExchangeSpacer
        onClick={_ => {
          dispatch(UpdateTokens(state.outputToken, state.inputToken));
          Dexter_Route.redirectToTokensHash(
            state.outputToken,
            state.inputToken,
          );
        }}
      />
      <DexterUi_ExchangeColumn
        title={"To receive (estimate):" |> React.string}
        token={state.outputToken}
        tokenSearchBalances=balances
        tokenSearchOnChange=updateOutputToken
        balanceInfo=[
          "My balance: "
          ++ Dexter_Balance.getAccountBalanceAsString(state.outputToken),
          "Dexter balance: "
          ++ (
            switch (state.inputToken, state.outputToken) {
            | (XtzBalance(_), ExchangeBalance(_)) =>
              state.outputToken |> Dexter_Balance.getExchangeTotalTokenAsString
            | (ExchangeBalance(_), XtzBalance(_)) =>
              state.inputToken |> Dexter_Balance.getExchangeTotalXtzAsString
            | _ => ""
            }
          ),
        ]>
        <DexterUi_TokenInput
          token={state.outputToken}
          value={state.outputValue}
          aboveLimitNote="There is insufficient liquidity for this exchange."
          updateValue={v => dispatch(UpdateOutputValue(v))}
          limit={
            switch (state.outputToken) {
            | XtzBalance(xtz) =>
              Mutez(
                switch (state.inputToken) {
                | ExchangeBalance(token) => token.exchangeXtz
                | _ => xtz
                },
              )
            | ExchangeBalance(token) => Token(token.exchangeTokenBalance)
            }
          }
        />
      </DexterUi_ExchangeColumn>
    </Flex>
    <DexterUi_ExchangeControls
      state
      resetInputs={() => dispatch(ResetInputs)}
    />
  </Flex>;
};
