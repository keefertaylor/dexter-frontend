[@react.component]
let make = () => {
  let {account}: DexterUi_Context.t = DexterUi_Context.useContext();

  <Flex flexDirection=`column maxWidth={`px(328)} minWidth={`px(328)}>
    <DexterUi_Box height={`px(257)}>
      {switch (account) {
       | Some(account) => <DexterUi_WalletInfo account />
       | _ => <DexterUi_ConnectWallet />
       }}
    </DexterUi_Box>
    <Flex height={`px(18)} />
    <DexterUi_Transactions />
    <Flex mt={`px(12)} pl={`px(16)}>
      <Text> {React.string("v" ++ Common.packageVersion())} </Text>
    </Flex>
  </Flex>;
};
