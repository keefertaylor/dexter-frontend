type currenciesState = {
  available: list(Currency.t),
  current: option(Currency.t),
  error: bool,
};

let initialCurrenciesState: currenciesState = {
  available: [],
  current:
    Currency_Dict.list
    |> Currency.findBySymbolOrDefault(Dexter_LocalStorage.Currency.get()),
  error: false,
};
