type t = {
  connectToBeacon: unit => unit,
  connectToTezBridge: unit => unit,
  currenciesState: DexterUi_Context_Types.currenciesState,
  setCurrenciesState: DexterUi_Context_Types.currenciesState => unit,
  darkMode: bool,
  setDarkMode: bool => unit,
  route: Dexter_Route.t,
  setRoute: (Dexter_Route.t, option(string)) => unit,
  account: option(Dexter_Account.t),
  disconnect: unit => unit,
  balances: list(Dexter_Balance.t),
  transactions: option(list(Dexter_Transaction.t)),
  pushTransaction: Dexter_Transaction.t => unit,
  transactionStatus: TZKT.Status.t,
  setTransactionStatus: TZKT.Status.t => unit,
  transactionTimeout: int,
  setTransactionTimeout: int => unit,
};

let defaultValue = {
  connectToBeacon: () => (),
  connectToTezBridge: () => (),
  currenciesState: DexterUi_Context_Types.initialCurrenciesState,
  setCurrenciesState: _ => (),
  darkMode: false,
  setDarkMode: _ => (),
  route: Dexter_Route.Exchange,
  setRoute: (_, _) => (),
  account: None,
  disconnect: () => (),
  balances: [],
  transactions: None,
  pushTransaction: _ => (),
  transactionStatus: Ready,
  setTransactionStatus: _ => (),
  transactionTimeout: 20,
  setTransactionTimeout: _ => (),
};

let ctx = React.createContext(defaultValue);

module ProviderInternal = {
  let makeProps = (~value, ~children, ()) => {
    "value": value,
    "children": children,
  };

  let make = React.Context.provider(ctx);
};

module Provider = {
  [@react.component]
  let make = (~children: React.element) => {
    let baseUrl = Utils.useBaseUrl();
    let (state, dispatch) = DexterUi_Context_Reducer.useContextReducer();

    let (path, hash) = Utils.useUrl();

    React.useEffect1(
      () => {
        if (path |> Dexter_Route.fromPath !== state.route) {
          dispatch(SetRoute(path |> Dexter_Route.fromPath));
        };
        None;
      },
      [|path |> Dexter_Route.fromPath |> Dexter_Route.toUrl|],
    );

    React.useEffect1(
      () => {
        if (hash !== "") {
          Dexter_LocalStorage.SelectedTokens.set(hash);
        };
        None;
      },
      [|hash|],
    );

    let setAccount =
      React.useCallback(account => dispatch(SetAccount(account)));

    let setBalances =
      React.useCallback(balances => dispatch(SetBalances(balances)));

    let setDarkMode =
      React.useCallback(darkMode => {
        Dexter_LocalStorage.DarkMode.set(darkMode);
        dispatch(SetDarkMode(darkMode));
      });

    let setCurrenciesState =
        (currenciesState: DexterUi_Context_Types.currenciesState) => {
      switch (currenciesState.current) {
      | Some(currency) => Dexter_LocalStorage.Currency.set(currency.symbol)
      | _ => ()
      };

      dispatch(SetCurrenciesExchangeRates(currenciesState));
    };

    let setRoute =
      React.useCallback((route, hash: option(string)) => {
        baseUrl
        ++ (route |> Dexter_Route.toUrl)
        ++ {
          switch (hash) {
          | Some(hash) => {j|#$hash|j}
          | _ => ""
          };
        }
        |> ReasonReactRouter.push
      });

    let setTransactions =
      React.useCallback(transactions =>
        dispatch(SetTransactions(transactions))
      );

    let setTransactionStatus =
      React.useCallback(transactionStatus =>
        if (transactionStatus !== state.transactionStatus) {
          dispatch(SetTransactionStatus(transactionStatus));
        }
      );

    let setTransactionTimeout =
      React.useCallback(transactionTimeout => {
        Dexter_LocalStorage.TransactionTimeout.set(transactionTimeout);
        dispatch(SetTransactionTimeout(transactionTimeout));
      });

    let pushTransaction =
      React.useCallback((transaction: Dexter_Transaction.t) => {
        Dexter_LocalStorage.Transaction.push(transaction);
        setTransactionStatus(Pending);
        setTransactions(
          switch (state.transactions) {
          | Some(transactions) => Some([transaction, ...transactions])
          | _ => Some([transaction])
          },
        );
      });

    let connectToBeacon =
      React.useCallback(() =>
        DexterUi_Context_Utils.connectToBeacon(setAccount, setBalances)
        |> ignore
      );

    let connectToTezBridge =
      React.useCallback(() =>
        DexterUi_Context_Utils.connectToTezBridge(setAccount, setBalances)
        |> ignore
      );

    let disconnect = () => {
      switch (state.account) {
      | Some(account) =>
        switch (account.wallet) {
        | Beacon(dappClient) => Beacon.disconnect(dappClient) |> ignore
        | _ => ()
        }
      | _ => ()
      };

      Dexter_LocalStorage.WalletType.remove();
      setAccount(None);
      setTransactions(None);
      setTransactionStatus(Ready);

      let disconnectedBalances: list(Dexter_Balance.t) =
        state.balances
        |> List.map(balance =>
             switch ((balance: Dexter_Balance.t)) {
             | XtzBalance(_) => Dexter_Balance.XtzBalance(Tezos.Mutez.zero)
             | ExchangeBalance(token) =>
               Dexter_Balance.ExchangeBalance({
                 ...token,
                 tokenBalance: Tezos.Token.zero,
                 lqtBalance: Tezos.Token.zero,
               })
             }
           );
      setBalances(disconnectedBalances);
    };

    DexterUi_Context_Utils.useSetup(
      connectToBeacon,
      connectToTezBridge,
      setBalances,
      setCurrenciesState,
    );

    DexterUi_Context_Utils.useAccountPolling(
      state.account,
      setAccount,
      setBalances,
      state.transactionStatus,
    );

    DexterUi_Context_Utils.useTransactionStatusPolling(
      state.account,
      state.transactions,
      setTransactions,
      setTransactionStatus,
    );

    <ProviderInternal
      value={
        connectToBeacon,
        connectToTezBridge,
        currenciesState: state.currenciesState,
        darkMode: state.darkMode,
        setDarkMode,
        route: state.route,
        setRoute,
        account: state.account,
        disconnect,
        balances: state.balances,
        setCurrenciesState,
        transactions: state.transactions,
        pushTransaction,
        transactionStatus: state.transactionStatus,
        setTransactionStatus,
        transactionTimeout: state.transactionTimeout,
        setTransactionTimeout,
      }>
      children
    </ProviderInternal>;
  };
};

let useContext = (): t => React.useContext(ctx);
