type state = {
  account: option(Dexter_Account.t),
  balances: list(Dexter_Balance.t),
  darkMode: bool,
  currenciesState: DexterUi_Context_Types.currenciesState,
  route: Dexter_Route.t,
  transactions: option(list(Dexter_Transaction.t)),
  transactionStatus: TZKT.Status.t,
  transactionTimeout: int,
};

type action =
  | SetAccount(option(Dexter_Account.t))
  | SetBalances(list(Dexter_Balance.t))
  | SetDarkMode(bool)
  | SetCurrenciesExchangeRates(DexterUi_Context_Types.currenciesState)
  | SetRoute(Dexter_Route.t)
  | SetTransactions(option(list(Dexter_Transaction.t)))
  | SetTransactionStatus(TZKT.Status.t)
  | SetTransactionTimeout(int);

let useContextReducer = () => {
  let (path, _) = Utils.useUrl();
  let initialState = {
    account: None,
    balances: [],
    darkMode: Dexter_LocalStorage.DarkMode.get(),
    currenciesState: DexterUi_Context_Types.initialCurrenciesState,
    route: path |> Dexter_Route.fromPath,
    transactions: None,
    transactionStatus: TZKT.Status.Ready,
    transactionTimeout: Dexter_LocalStorage.TransactionTimeout.get(),
  };

  React.useReducer(
    (state, action) =>
      switch (action) {
      | SetAccount(account) => {...state, account}
      | SetBalances(balances) => {...state, balances}
      | SetDarkMode(darkMode) => {...state, darkMode}
      | SetCurrenciesExchangeRates(currenciesState) => {
          ...state,
          currenciesState,
        }
      | SetRoute(route) => {...state, route}
      | SetTransactions(transactions) => {...state, transactions}
      | SetTransactionStatus(transactionStatus) => {
          ...state,
          transactionStatus,
        }
      | SetTransactionTimeout(transactionTimeout) => {
          ...state,
          transactionTimeout,
        }
      },
    initialState,
  );
};
