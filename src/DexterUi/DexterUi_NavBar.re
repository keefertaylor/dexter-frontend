type link = {
  href: string,
  text: string,
};

let links: array(link) = [|
  {href: "https://stats.dexter.exchange", text: "Dexter Stats"},
  {href: "https://dexter.exchange/docs/dexter-tutorials/", text: "Tutorials"},
  {
    href: "https://docs.google.com/forms/d/e/1FAIpQLSeM5hB6A4swBrpwMceGQZ3ioS_k3_SbqLESaea5svGwobUmeQ/viewform?usp=sf_link",
    text: "Share Feedback",
  },
|];

[@react.component]
let make = () =>
  <Flex
    alignItems=`center
    height={`px(61)}
    width={`percent(100.)}
    justifyContent=`center
    background=Colors.offWhite
    flexShrink=0.>
    <Flex
      maxWidth={`px(1024)}
      width={`percent(100.)}
      px={`px(28)}
      justifyContent=`spaceBetween
      alignItems=`center>
      <Flex alignItems=`center>
        <Flex mr={`px(18)}>
          <Flex height={`px(21)} width={`px(115)}>
            <DexterUi_Image src="logo.svg" />
          </Flex>
          <Flex ml={`px(14)} mt={`px(-2)}>
            <Text textStyle=TextStyles.version color=Colors.blackish1>
              {"V1.0" |> React.string}
            </Text>
          </Flex>
        </Flex>
        <a
          className=Css.(style([textDecoration(`none)]))
          target="_blank"
          href="https://dexter.exchange">
          <DexterUi_Button px={`px(16)} variant=Secondary>
            {"About Dexter" |> React.string}
          </DexterUi_Button>
        </a>
      </Flex>
      <Flex>
        <Flex justifyContent=`flexEnd alignItems=`center mr={`px(20)}>
          {links
           |> Array.mapi((i, {href, text}) =>
                <Flex
                  key={i |> string_of_int} alignItems=`baseline ml={`px(20)}>
                  <a target="_blank" href>
                    <Text textStyle=TextStyles.h4 color=Colors.black>
                      {text |> React.string}
                    </Text>
                  </a>
                </Flex>
              )
           |> React.array}
        </Flex>
        <DexterUi_ModeSwitch />
        <Flex mr={`px(12)} />
        <DexterUi_CurrencySwitch />
      </Flex>
    </Flex>
  </Flex>;
