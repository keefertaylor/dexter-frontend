[@react.component]
let make =
    (
      ~children: React.element,
      ~boxHeight: Flex.heightType,
      ~image: string,
      ~imageHeight: int=65,
      ~number: int,
    ) => {
  let {darkMode}: DexterUi_Context.t = DexterUi_Context.useContext();

  <Flex
    className=Css.(
      style([
        boxShadow(
          Shadow.box(~y=px(1), ~blur=px(4), rgba(0, 0, 0, `num(0.2))),
        ),
      ])
    )
    height=boxHeight
    borderRadius={`px(5)}
    py={`px(20)}
    px={`px(24)}
    background={darkMode ? Colors.tabInactiveDark : Colors.white}
    alignItems=`center
    width={`percent(100.)}
    justifyContent=`spaceBetween>
    <Flex>
      <Text lightModeColor=Colors.blackish1 textStyle=TextStyles.bold>
        {(number |> string_of_int) ++ "." |> React.string}
      </Text>
      <Flex ml={`px(4)} />
      <Flex flexDirection=`column width={`px(329)}> children </Flex>
    </Flex>
    <Flex flexGrow=1. justifyContent=`center>
      <DexterUi_Image
        className=Css.(style([height(`px(imageHeight))]))
        src=image
      />
    </Flex>
  </Flex>;
};
