[@react.component]
let make = () => {
  let {balances, darkMode}: DexterUi_Context.t =
    DexterUi_Context.useContext();
  let exchangeBalances = balances |> Dexter_Balance.getTokens1;

  <Flex flexDirection=`column mb={`px(-20)}>
    <DexterUi_PoolDataHeader />
    <Flex
      flexDirection=`column height={`px(367)} overflowY=`auto pb={`px(4)}>
      {exchangeBalances
       |> List.mapi((i, exchangeBalance) =>
            <Flex key={i |> string_of_int} flexDirection=`column flexShrink=0.>
              <DexterUi_PoolDataRow exchangeBalance />
              {i < (exchangeBalances |> List.length)
               - 1
               |> Utils.renderIf(
                    <Flex
                      background={Colors.line(darkMode)}
                      full=true
                      height={`px(1)}
                      mt={`px(8)}
                    />,
                  )}
            </Flex>
          )
       |> Array.of_list
       |> React.array}
      <Flex height={`px(17)} />
      <a
        className=Css.(style([textDecoration(`none)]))
        href="https://stats.dexter.exchange"
        target="_blank">
        <Flex alignItems=`center justifyContent=`center>
          <DexterUi_Image
            className=Css.(style([height(`px(14)), marginRight(`px(6))]))
            src="pool-data.png"
          />
          <Text
            darkModeColor=Colors.primaryDark
            lightModeColor=Colors.blue
            textDecoration=`underline>
            {"View more pool data as stats.dexter.exchange" |> React.string}
          </Text>
        </Flex>
      </a>
    </Flex>
  </Flex>;
};
