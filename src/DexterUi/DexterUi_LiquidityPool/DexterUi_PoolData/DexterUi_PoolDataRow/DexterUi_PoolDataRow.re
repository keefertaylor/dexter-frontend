[@react.component]
let make = (~exchangeBalance: Dexter_ExchangeBalance.t) =>
  <Flex mt={`px(8)}>
    <Flex width={`percent(23.)} alignItems=`center pr={`px(12)}>
      <DexterUi_PoolTokenIcon
        icons=(Tezos.Mutez.icon, exchangeBalance.icon)
        small=true
      />
      <Flex width={`px(12)} />
      <Text textStyle=TextStyles.bold lightModeColor=Colors.darkGrey2>
        <DexterUi_PoolTokenSymbol
          poolToken=(
            XtzBalance(Tezos_Mutez.zero),
            ExchangeBalance(exchangeBalance),
          )
        />
      </Text>
    </Flex>
    <Flex width={`percent(19.)} alignItems=`center pr={`px(12)}>
      <Text>
        {(exchangeBalance.exchangeXtz |> Tezos.Mutez.toTezStringWithCommas)
         ++ " XTZ"
         |> React.string}
      </Text>
    </Flex>
    <Flex width={`percent(19.)} alignItems=`center pr={`px(12)}>
      <Text>
        {(
           exchangeBalance.exchangeTokenBalance
           |> Tezos.Token.toStringWithCommas
         )
         ++ " "
         ++ exchangeBalance.symbol
         |> React.string}
      </Text>
    </Flex>
    <Flex width={`percent(19.)} alignItems=`center pr={`px(12)}>
      <Text>
        {switch (exchangeBalance.dexterBaker) {
         | Some(dexterBaker) =>
           <DexterUi_Link
             href={
               "https://tzkt.io/" ++ (dexterBaker |> Tezos_Address.toString)
             }>
             {Dexter_Settings.knownBakers
              |> List.find_opt((baker: Dexter_Settings.baker) =>
                   baker.address === (dexterBaker |> Tezos_Address.toString)
                 )
              |> Utils.map((baker: Dexter_Settings.baker) => baker.name)
              |> Utils.orDefault(dexterBaker |> Tezos_Address.toString)
              |> React.string}
           </DexterUi_Link>
         | _ => "No baker" |> React.string
         }}
      </Text>
    </Flex>
    <Flex width={`percent(20.)} alignItems=`center justifyContent=`center>
      <DexterUi_PoolDataRowButtons exchangeBalance />
    </Flex>
  </Flex>;
