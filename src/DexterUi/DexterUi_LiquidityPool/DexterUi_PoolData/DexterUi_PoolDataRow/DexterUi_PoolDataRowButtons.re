[@react.component]
let make = (~exchangeBalance: Dexter_ExchangeBalance.t) => {
  let {account}: DexterUi_Context.t = DexterUi_Context.useContext();

  let hasXtzBalance =
    switch (account) {
    | Some(account) => account.xtz |> Tezos_Mutez.gtZero
    | _ => false
    };

  let hasLqtBalance = exchangeBalance.lqtBalance |> Tezos_Token.gtZero;
  let hasTokenBalance = exchangeBalance.tokenBalance |> Tezos_Token.gtZero;

  <>
    <DexterUi_PoolDataRowButton
      disabled={!hasTokenBalance || !hasXtzBalance}
      hash={Dexter_Route.getTokensHash(
        Dexter_Balance.XtzBalance(Tezos_Mutez.zero),
        ExchangeBalance(exchangeBalance),
      )}
      route={Dexter_Route.LiquidityPool(AddLiquidity)}
    />
    <DexterUi_PoolDataRowButton
      disabled={!hasLqtBalance}
      hash={Dexter_Route.getTokensHash(
        Dexter_Balance.XtzBalance(Tezos_Mutez.zero),
        ExchangeBalance(exchangeBalance),
      )}
      route={Dexter_Route.LiquidityPool(RemoveLiquidity)}
    />
    <DexterUi_PoolDataRowButton
      disabled={!hasXtzBalance && !hasTokenBalance}
      hash={
        hasXtzBalance
          ? Dexter_Route.getTokensHash(
              Dexter_Balance.XtzBalance(Tezos_Mutez.zero),
              ExchangeBalance(exchangeBalance),
            )
          : Dexter_Route.getTokensHash(
              ExchangeBalance(exchangeBalance),
              Dexter_Balance.XtzBalance(Tezos_Mutez.zero),
            )
      }
      route=Dexter_Route.Exchange
    />
  </>;
};
