[@react.component]
let make = (~disabled: bool=false, ~hash: string, ~route: Dexter_Route.t) => {
  let {account, setRoute}: DexterUi_Context.t = DexterUi_Context.useContext();

  let allDisabled = account |> Belt.Option.isNone;

  <Flex
    onClick=?{
      allDisabled || disabled
        ? None : Some(_ => setRoute(route, Some(hash)))
    }
    mr={`px(8)}>
    <DexterUi_TransactionIcon
      disabled={allDisabled || disabled}
      transactionType={
        switch (route) {
        | Dexter_Route.LiquidityPool(AddLiquidity) => AddLiquidity
        | Dexter_Route.LiquidityPool(RemoveLiquidity) => RemoveLiquidity
        | _ => Exchange
        }
      }
    />
  </Flex>;
};
