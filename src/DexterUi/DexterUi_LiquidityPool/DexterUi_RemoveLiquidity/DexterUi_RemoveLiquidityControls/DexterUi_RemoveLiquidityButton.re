open DexterUi_RemoveLiquidity_Utils;

[@react.component]
let make =
    (
      ~disabled: bool,
      ~resetInputs: unit => unit,
      ~state: DexterUi_RemoveLiquidity_Reducer.state,
      ~tokenAmount: Tezos.Token.t,
      ~xtzAmount: Tezos.Mutez.t,
    ) => {
  let {account, pushTransaction, transactionTimeout}: DexterUi_Context.t =
    DexterUi_Context.useContext();

  <Flex flexGrow=1. flexDirection=`column alignItems=`center mt={`px(16)}>
    <DexterUi_Button
      disabled
      px={`px(20)}
      onClick={_ =>
        switch (account) {
        | Some(account) when !disabled =>
          onRemoveLiquidity(
            account,
            pushTransaction,
            resetInputs,
            state,
            tokenAmount,
            transactionTimeout,
            xtzAmount,
          )
        | _ => ()
        }
      }>
      {"Redeem pool tokens" |> React.string}
    </DexterUi_Button>
  </Flex>;
};
