let checkIsInvalid =
    (
      tokenAmount: Tezos.Token.t,
      xtzAmount: Tezos.Mutez.t,
      state: DexterUi_RemoveLiquidity_Reducer.state,
    ) => {
  switch (state.token, state.liquidity) {
  | (Some(token), Valid(liquidity, _))
      when
        Tezos.Token.gtZero(liquidity)
        && Tezos.Token.leq(liquidity, token.lqtBalance) =>
    Tezos.Mutez.leqZero(xtzAmount) || Tezos.Token.leqZero(tokenAmount)
  | _ => true
  };
};

[@react.component]
let make =
    (
      ~resetInputs: unit => unit,
      ~token: Dexter_ExchangeBalance.t,
      ~tokenAmount: Tezos.Token.t,
      ~xtzAmount: Tezos.Mutez.t,
      ~state: DexterUi_RemoveLiquidity_Reducer.state,
    ) => {
  let {transactionStatus}: DexterUi_Context.t = DexterUi_Context.useContext();

  let hasInsufficientXtz = DexterUi_Hooks.useHasInsufficientXtz();

  let isInvalid =
    hasInsufficientXtz || checkIsInvalid(tokenAmount, xtzAmount, state);

  <>
    {hasInsufficientXtz |> Utils.renderIf(<DexterUi_InsufficientXtzMessage />)}
    <DexterUi_RemoveLiquidityDescription
      isInvalid
      token
      tokenAmount
      xtzAmount
      state
    />
    <DexterUi_RemoveLiquidityButton
      disabled={isInvalid || transactionStatus === Pending}
      resetInputs
      state
      tokenAmount
      xtzAmount
    />
  </>;
};
