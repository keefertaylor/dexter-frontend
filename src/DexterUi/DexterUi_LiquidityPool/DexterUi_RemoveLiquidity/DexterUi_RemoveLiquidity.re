[@react.component]
let make = () => {
  let {account, balances}: DexterUi_Context.t = DexterUi_Context.useContext();

  let (state, dispatch) =
    DexterUi_RemoveLiquidity_Reducer.useRemoveLiquidityReducer(
      balances |> Dexter_Balance.getTokens,
    );

  DexterUi_Hooks.useResetting(
    () => dispatch(ResetTokens),
    () => dispatch(ResetInputs),
  );

  let noAccount = account === None;
  let noLiquidity = balances |> Dexter_Balance.isEmptyLqt;

  switch (noAccount || noLiquidity, state.token) {
  | (true, _) => <DexterUi_RemoveLiquidityMessage noAccount noLiquidity />
  | (_, Some(token)) =>
    let (xtzAmount, tokenAmount) =
      switch (state.liquidity) {
      | Valid(liquidity, _) =>
        let (xtzAmount, tokenAmount) =
          Dexter_AddLiquidity.removeLiquidityOutput(
            ~lqtBurned=liquidity,
            ~totalLiquidity=token.exchangeTotalLqt,
            ~tokenPool=token.exchangeTokenBalance,
            ~xtzPool=token.exchangeXtz,
            (),
          );

        (xtzAmount, tokenAmount);
      | _ => (Tezos.Mutez.zero, Tezos.Token.zero)
      };

    <>
      <Flex mb={`px(10)}>
        <DexterUi_ExchangeColumn
          title={"Redeem pool tokens:" |> React.string}
          poolToken=(XtzBalance(xtzAmount), ExchangeBalance(token))
          tokenSearchBalances={balances |> Dexter_Balance.getPositiveLqt}
          tokenSearchOnChange={token =>
            switch (token) {
            | ExchangeBalance(token) =>
              dispatch(UpdatePoolToken(token));
              Dexter_Route.redirectToTokensHash(
                XtzBalance(Tezos.Mutez.zero),
                ExchangeBalance(token),
              );
            | _ => ()
            }
          }
          balanceInfo=[
            "My balance: " ++ Tezos.Token.toStringWithCommas(token.lqtBalance),
            "Pool size: "
            ++ Tezos.Token.toStringWithCommas(token.exchangeTotalLqt),
          ]>
          <DexterUi_TokenInput
            limit={Token(token.lqtBalance)}
            withCurrencyValue=false
            token={ExchangeBalance(token)}
            onClickMax={_ =>
              dispatch(
                UpdateLiquidityValue(
                  Dexter_Value.getTokenValue(token.lqtBalance),
                ),
              )
            }
            updateValue={v => {
              let v: Valid.t(Tezos.Token.t) =
                switch (v) {
                | Valid(Token(token), s) => Valid(token, s)
                | Valid(Mutez(_), s)
                | Invalid(s) => Invalid(s)
                | InvalidInitialState => InvalidInitialState
                };
              dispatch(UpdateLiquidityValue(v));
            }}
            value={
              switch (state.liquidity) {
              | Valid(value, s) => Valid(Token(value), s)
              | Invalid(s) => Invalid(s)
              | InvalidInitialState => InvalidInitialState
              }
            }
          />
        </DexterUi_ExchangeColumn>
        <DexterUi_ExchangeSpacer />
        <Flex
          flexDirection=`column
          width={`calc((`sub, `percent(200. /. 3.), `px(25)))}>
          <Flex>
            <DexterUi_ExchangeColumn
              title={"Receive (estimate):" |> React.string}
              token={ExchangeBalance(token)}>
              <DexterUi_ExchangeInput
                displayOnly=true
                note={
                  <DexterUi_CurrencyValue token value={Token(tokenAmount)} />
                }>
                {Tezos.Token.toStringWithCommas(tokenAmount) |> React.string}
              </DexterUi_ExchangeInput>
            </DexterUi_ExchangeColumn>
            <DexterUi_ExchangePlus />
            <DexterUi_ExchangeColumn token={XtzBalance(xtzAmount)}>
              <DexterUi_ExchangeInput
                displayOnly=true
                note={<DexterUi_CurrencyValue value={Mutez(xtzAmount)} />}>
                {Tezos.Mutez.toTezStringWithCommas(xtzAmount) |> React.string}
              </DexterUi_ExchangeInput>
            </DexterUi_ExchangeColumn>
          </Flex>
          <DexterUi_MarketRate
            xtzPool={token.exchangeXtz}
            tokenPool={token.exchangeTokenBalance}
            symbol={token.symbol}
          />
        </Flex>
      </Flex>
      <DexterUi_RemoveLiquidityControls
        resetInputs={() => dispatch(ResetInputs)}
        token
        state
        tokenAmount
        xtzAmount
      />
    </>;
  | _ => React.null
  };
};
