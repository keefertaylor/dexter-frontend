[@react.component]
let make = () =>
  <Flex mt={`px(6)} flexGrow=1. justifyContent=`center>
    <DexterUi_Panel variant=Warning>
      {"This will be the first liquidity provided for this token pair. The ratio of tokens you add will set the exchange rate for the next user to exchange this pair; this exchange rate may change as trading volume of this pair increases."
       |> React.string}
    </DexterUi_Panel>
  </Flex>;
