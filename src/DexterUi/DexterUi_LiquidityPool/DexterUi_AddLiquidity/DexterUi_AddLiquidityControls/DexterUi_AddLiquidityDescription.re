[@react.component]
let make =
    (
      ~disabled: bool=false,
      ~isInvalid: bool,
      ~state: DexterUi_AddLiquidity_Reducer.state,
    ) => {
  let {account, transactionStatus}: DexterUi_Context.t =
    DexterUi_Context.useContext();

  <Flex flexDirection=`column alignItems=`center mt={`px(14)}>
    <Text
      lightModeColor=Colors.grey1 textStyle=TextStyles.h2 textAlign=`center>
      {switch (
         account |> Belt.Option.isNone,
         disabled,
         isInvalid,
         transactionStatus,
         state.token,
       ) {
       | (true, _, _, _, _) =>
         "Please connect a wallet to use the Dexter Liquidity Pool."
         |> React.string
       | (_, true, _, _, _) =>
         <>
           {"You need two different types of Tezos tokens in your wallet to add liquidity."
            |> React.string}
           <br />
           {"Click the \'Learn more\' link in the Dexter Liquidity Pool explainer above for more info"
            |> React.string}
         </>
       | (_, _, _, Pending, _) => Common.pendingTransactionMessage
       | (_, _, false, _, Some(token)) =>
         <>
           {"You are about to add "
            ++ (
              switch (state.xtzValue) {
              | Valid(m, _) => Tezos.Mutez.toTezStringWithCommas(m)
              | _ => "0"
              }
            )
            ++ " XTZ  and "
            ++ (
              switch (state.tokenValue) {
              | Valid(m, _) => Tezos.Token.toStringWithCommas(m)
              | _ => "0"
              }
            )
            ++ " "
            ++ token.symbol
            ++ " to the liquidity pool to receive "
            ++ Tezos.Token.toStringWithCommas(state.liquidity)
            ++ " "
            |> React.string}
           <DexterUi_PoolTokenSymbol
             poolToken=(
               XtzBalance(Tezos_Mutez.zero),
               ExchangeBalance(token),
             )
           />
           {" pool tokens. You may redeem your pool tokens later by removing liquidity from the pool to receive your share of XTZ and "
            ++ token.symbol
            ++ " tokens."
            |> React.string}
         </>
       | _ => React.null
       }}
    </Text>
  </Flex>;
};
