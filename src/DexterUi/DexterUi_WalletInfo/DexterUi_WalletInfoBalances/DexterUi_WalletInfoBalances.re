[@react.component]
let make = () => {
  let {balances}: DexterUi_Context.t = DexterUi_Context.useContext();
  let positiveBalances = balances |> Dexter_Balance.getPositive;

  <>
    <Flex mt={`px(16)}>
      <Text lightModeColor=Colors.blackish1 textStyle=TextStyles.bold>
        {"Token balances" |> React.string}
      </Text>
    </Flex>
    {switch (balances |> Dexter_Balance.isEmpty) {
     | true =>
       <DexterUi_EmptyMessage>
         {"You do not have any Tezos tokens in your wallet" |> React.string}
       </DexterUi_EmptyMessage>
     | _ =>
       <Flex
         mb={`px(-16)}
         pb={`px(3)}
         flexDirection=`column
         overflowY=`auto
         height={`px(87)}>
         {positiveBalances
          |> List.mapi((i: int, balance: Dexter.Balance.t) =>
               <DexterUi_WalletInfoBalancesRow
                 key={balance |> Dexter_Balance.getSymbol}
                 withBorder={i < (positiveBalances |> List.length) - 1}
                 symbol={balance |> Dexter_Balance.getSymbol |> React.string}
                 amount={
                   balance
                   |> Dexter_Balance.getAccountBalanceAsString
                   |> React.string
                 }
               />
             )
          |> Array.of_list
          |> React.array}
       </Flex>
     }}
  </>;
};
