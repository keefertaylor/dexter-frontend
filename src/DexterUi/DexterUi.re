include DexterUi_Style;
module MediaQuery = Common_MediaQuery;

[@react.component]
let make = () =>
  <>
    <MediaQuery.Compat query="(max-device-width: 768px)">
      <DexterUi_Mobile />
    </MediaQuery.Compat>
    <MediaQuery.Compat query="(min-device-width: 769px)">
      <DexterUi_Context.Provider>
        <DexterUi_Content />
        {switch (Common.Deployment.get()) {
         | Development => <DexterUi_Development />
         | _ => React.null
         }}
      </DexterUi_Context.Provider>
    </MediaQuery.Compat>
  </>;
