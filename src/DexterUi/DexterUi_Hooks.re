let useResetting = (resetTokens: unit => unit, resetInputs: unit => unit) => {
  let {account, balances}: DexterUi_Context.t = DexterUi_Context.useContext();
  /* monitor the balances prop, if it changes, update the tokens */
  React.useEffect1(
    () => {
      resetTokens();
      None;
    },
    [|balances|],
  );

  /* monitor the account prop, if it changes, reset inputs */
  React.useEffect1(
    () => {
      if (account |> Belt.Option.isNone) {
        resetInputs();
      };
      None;
    },
    [|account|],
  );
};

let useHasInsufficientXtz = () => {
  let {account}: DexterUi_Context.t = DexterUi_Context.useContext();

  account
  |> Utils.map((account: Dexter_Account.t) =>
       Tezos.Mutez.(leqZero(userMax(account.xtz)))
     )
  |> Utils.orDefault(false);
};

let useShowRequiredXtzReserveNote = (xtzValue: option(Tezos.Mutez.t)) => {
  let {account}: DexterUi_Context.t = DexterUi_Context.useContext();

  account
  |> Utils.map((account: Dexter_Account.t) =>
       switch (xtzValue) {
       | Some(xtzValue) =>
         Tezos.Mutez.(
           geq(xtzValue, userMax(account.xtz))
           && leq(xtzValue, account.xtz)
           && gtZero(xtzValue)
         )
       | _ => false
       }
     )
  |> Utils.orDefault(false);
};

let useTokensFromLocalStorage =
    (balances: list(Dexter_Balance.t))
    : (option(Dexter_Balance.t), option(Dexter_Balance.t)) => {
  let symbols =
    Dexter_LocalStorage.SelectedTokens.get()
    |> Utils.orDefault("")
    |> String.split_on_char('_');

  (
    List.nth_opt(symbols, 0)
    |> Utils.flatMap(symbol =>
         balances |> Dexter_Balance.findBalanceBySymbol(symbol)
       ),
    List.nth_opt(symbols, 1)
    |> Utils.flatMap(symbol =>
         balances |> Dexter_Balance.findBalanceBySymbol(symbol)
       ),
  );
};
