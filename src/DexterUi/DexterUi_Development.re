[@react.component]
let make = () => {
  let {darkMode}: DexterUi_Context.t = DexterUi_Context.useContext();
  let (isVisible, setIsVisible) = React.useState(_ => false);
  let (address, setAddress) = React.useState(_ => "");
  let (message, setMessage) = React.useState(_ => None);
  let (requesting, setRequesting) = React.useState(_ => false);

  let buttonDisabled =
    Belt.Result.isError(Tezos.Address.ofString(address)) || requesting;

  let onClick = _ =>
    switch (Tezos.Address.ofString(address)) {
    | Ok(address) =>
      setRequesting(_ => true);
      setMessage(_ =>
        Some("Processing XTZ request. This will take up to two minutes.")
      );
      Dexter_Development.getXtz(address)
      |> Js.Promise.then_(result => {
           switch (result) {
           | Belt.Result.Ok(msg) =>
             setMessage(_ => Some(msg));
             setRequesting(_ => false);
             Js.Promise.resolve();

           | Belt.Result.Error(msg) =>
             setMessage(_ => Some(msg));
             setRequesting(_ => false);
             Js.Promise.resolve();
           }
         })
      |> ignore;
    | Error(_) => ()
    };

  <Flex
    className=Css.(
      style([
        transform(
          translateY(isVisible ? `zero : `calc((`sub, pct(100.), px(30)))),
        ),
      ])
    )
    position=`fixed
    bottom={`px(0)}
    width={`percent(100.)}
    alignItems=`flexEnd
    flexDirection=`column>
    <Flex
      height={`px(30)}
      alignItems=`center
      px={`px(8)}
      onClick={_ => setIsVisible(_ => !isVisible)}>
      <Text textStyle=TextStyles.h3> {"Development" |> React.string} </Text>
    </Flex>
    <Flex
      background={Colors.boxBackground(darkMode)}
      py={`px(8)}
      px={`px(20)}
      alignItems=`center
      width={`percent(100.)}
      justifyContent=`flexEnd>
      <Flex alignItems=`center position=`relative>
        <Text textStyle=TextStyles.h1>
          {"camlCase XTZ Faucet: " |> React.string}
        </Text>
        <Flex mx={`px(16)}>
          <DexterUi_AddressInput
            value=address
            setValue={value => setAddress(_ => value)}
          />
        </Flex>
        <DexterUi_Button
          px={`px(24)} disabled=buttonDisabled onClick small=true>
          {"Get 500 XTZ (once per address)" |> React.string}
        </DexterUi_Button>
        {switch (message) {
         | Some(msg) =>
           <Flex
             className=Css.(style([transform(translateX(pct(-50.)))]))
             position=`fixed
             bottom={`px(56)}
             left={`percent(50.)}>
             <Text> {React.string(msg)} </Text>
           </Flex>
         | None => React.null
         }}
      </Flex>
    </Flex>
  </Flex>;
};
