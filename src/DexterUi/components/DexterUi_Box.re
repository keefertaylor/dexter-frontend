[@react.component]
let make =
    (
      ~children: React.element,
      ~height: option(Flex.heightType)=?,
      ~overflowY: Css.Types.Overflow.t=`auto,
      ~p: Css.Types.Length.t=`px(16),
      ~tabs: React.element=React.null,
    ) => {
  let {darkMode}: DexterUi_Context.t = DexterUi_Context.useContext();

  <Flex
    background={Colors.boxBackground(darkMode)}
    ?height
    flexDirection=`column
    borderRadius={`px(16)}
    position=`relative>
    <Flex
      full=true
      flexDirection=`column
      overflow=`hidden
      flexShrink=0.
      className=Css.(
        style([
          borderTopLeftRadius(`px(16)),
          borderTopRightRadius(`px(16)),
        ])
      )>
      tabs
    </Flex>
    <Flex p overflowY> children </Flex>
  </Flex>;
};
