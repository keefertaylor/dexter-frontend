[@react.component]
let make =
    (
      ~token: option(Dexter_ExchangeBalance.t)=?,
      ~value: InputType.t=Mutez(Tezos.Mutez.zero),
      ~withBrackets: bool=false,
    ) => {
  let {currenciesState}: DexterUi_Context.t = DexterUi_Context.useContext();

  !currenciesState.error
  |> Utils.renderIf(
       currenciesState.current
       |> Utils.renderOpt(currency =>
            <span
              className={
                withBrackets
                  ? Css.(
                      style([
                        Css.before([Css.contentRule(`text("\\00a0 "))]),
                      ])
                    )
                  : ""
              }>
              {withBrackets |> Utils.renderIf("(" |> React.string)}
              {value
               |> InputType.toTezFloat(token)
               |> Currency.toStringWithCommas(currency)
               |> React.string}
              {withBrackets |> Utils.renderIf(")" |> React.string)}
            </span>
          ),
     );
};
