[@react.component]
let make = (~onClose: unit => unit) => {
  let {currenciesState}: DexterUi_Context.t = DexterUi_Context.useContext();
  let (value, setValue) = React.useState(_ => "");

  <DexterUi_Dialog onClose px={`px(24)} width={`px(366)}>
    <Text textStyle=TextStyles.h1 color=Colors.black>
      {"Select local currency" |> React.string}
    </Text>
    {currenciesState.error
       ? <DexterUi_CurrencySwitchError />
       : <>
           <Flex mb={`px(8)} />
           <input
             autoFocus=true
             placeholder="Search..."
             className=Css.(
               style([
                 background(Colors.white),
                 border(`zero, `none, Colors.white),
                 fontSize(px(10)),
                 padding2(~v=`zero, ~h=`px(15)),
                 boxSizing(`borderBox),
                 borderRadius(`px(16)),
                 height(`px(32)),
                 Css.placeholder([color(Colors.grey)]),
                 Css.width(pct(100.)),
                 flexShrink(0.),
               ])
             )
             value
             onChange={ev => setValue(ev |> Common.eventToValue)}
           />
           <Flex
             flexDirection=`column
             full=true
             maxHeight={`px(368)}
             mt={`px(8)}
             overflowY=`auto
             pb={`px(14)}>
             <DexterUi_CurrencySwitchListGroup popular=true value />
             <DexterUi_CurrencySwitchListGroup value />
           </Flex>
         </>}
  </DexterUi_Dialog>;
};
