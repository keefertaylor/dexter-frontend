[@react.component]
let make = () => {
  <>
    <Flex mb={`px(4)} />
    <Text color=Colors.grey1 textStyle=TextStyles.h2>
      {"Pricing data is currently unavailable." |> React.string}
    </Text>
    <Text color=Colors.grey1 textStyle=TextStyles.h2>
      {"Please check back again soon." |> React.string}
    </Text>
    <Flex mb={`px(24)} />
  </>;
};
