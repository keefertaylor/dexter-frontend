[@react.component]
let make = () => {
  let {currenciesState}: DexterUi_Context.t = DexterUi_Context.useContext();
  let (showDialog, setShowDialog) = React.useState(_ => false);

  <>
    {showDialog
     |> Utils.renderIf(
          <DexterUi_CurrencySwitchList
            onClose={_ => setShowDialog(_ => false)}
          />,
        )}
    <Flex
      background=Colors.lightGrey1
      borderRadius={`px(14)}
      alignItems=`center
      justifyContent=`spaceBetween
      p={`px(4)}
      onClick=?{
        currenciesState.available |> List.length > 0 || currenciesState.error
          ? Some(_ => setShowDialog(_ => true)) : None
      }
      position=`relative>
      <Flex
        alignItems=`center
        background=Colors.offWhite
        borderRadius={`px(14)}
        height={`percent(100.)}
        justifyContent=`center
        py={`px(2)}
        width={`px(36)}>
        <Text
          textStyle=TextStyles.h4
          fontWeight=`semiBold
          textDecoration={currenciesState.error ? `lineThrough : `none}
          color={currenciesState.error ? Colors.red : Colors.text(false)}>
          {currenciesState.current
           |> Utils.map((currency: Currency.t) => currency.symbol)
           |> Utils.orDefault(Dexter_LocalStorage.Currency.get())
           |> React.string}
        </Text>
      </Flex>
    </Flex>
  </>;
};
