[@react.component]
let make = (~children: React.element, ~href: string) =>
  <Text darkModeColor=Colors.primaryDark lightModeColor=Colors.blue ellipsis=true>
    <a href target="_blank"> children </a>
  </Text>;
