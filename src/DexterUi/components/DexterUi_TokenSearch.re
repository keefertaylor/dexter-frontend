/**
 * Exchange left: show how much token user has
 * Exchange right: show how much token exchange has
 * Add Liquidity left: show how much token user has
 * Add Liquidity right: show how much token user has
 * Remove Liquidity left: show how much liquidity user has
 */

[@react.component]
let make =
    (
      ~balances: list(Dexter_Balance.t),
      ~hideTokenSearch: _ => unit,
      ~side: Side.t,
      ~onTokenChange: Dexter_Balance.t => unit,
    ) => {
  let {darkMode, route}: DexterUi_Context.t = DexterUi_Context.useContext();

  let (filter, setFilter) = React.useState(_ => "");
  let (balancesFiltered, setBalancesFiltered) = React.useState(_ => balances);

  let setFilterHandler = (newFilter: string) => {
    setFilter(_ => newFilter);

    let filter = newFilter |> String.trim |> String.lowercase_ascii;

    setBalancesFiltered(_ =>
      newFilter == ""
        ? balances
        : balances
          |> List.filter((token: Dexter_Balance.t) =>
               Common.isSubstring(
                 token |> Dexter_Balance.getSymbol |> String.lowercase_ascii,
                 filter,
               )
               || Common.isSubstring(
                    token |> Dexter_Balance.getName |> String.lowercase_ascii,
                    filter,
                  )
             )
    );
  };

  /* if the balance prop changes, upddate everything */
  React.useEffect1(
    () => {
      setFilterHandler(filter);
      None;
    },
    [|balances|],
  );

  <Flex position=`relative>
    <Flex
      className=Css.(style([important(cursor(`default))]))
      position=`fixed
      top=`zero
      bottom=`zero
      left=`zero
      right=`zero
      zIndex=2
      width={`percent(100.)}
      onClick=hideTokenSearch
    />
    <Flex
      className=Css.(
        style([
          boxShadow(
            Shadow.box(~y=px(2), ~blur=px(10), rgba(0, 0, 0, `num(0.25))),
          ),
        ])
      )
      top={`px(-40)}
      background={Colors.boxBackground(darkMode)}
      borderRadius={`px(8)}
      flexDirection=`column
      maxWidth=`initial
      position=`absolute
      overflow=`hidden
      zIndex=3>
      <Flex pl={`px(14)} pr={`px(16)} py={`px(8)} alignItems=`center>
        <Text fontSize={`px(18)}>
          <i className="icon-magnifying-glass" />
        </Text>
        <Flex width={`px(14)} />
        <input
          autoFocus=true
          className=Css.(
            style([
              borderWidth(`px(0)),
              outlineStyle(`none),
              backgroundColor(`transparent),
              color(darkMode ? Colors.white : Colors.black),
              placeholder([color(Colors.text(darkMode))]),
              width(px(150)),
              ...TextStyles.h1,
            ])
          )
          placeholder="Search token"
          value=filter
          onChange={ev =>
            setFilterHandler(ReactEvent.Form.target(ev)##value)
          }
        />
      </Flex>
      {balancesFiltered
       |> List.mapi((i, balance: Dexter_Balance.t) =>
            <Flex
              className=Css.(
                style([
                  borderTop(px(1), `solid, Colors.line(darkMode)),
                  hover([
                    background(
                      darkMode ? Colors.tabInactiveDark : Colors.whiteGray,
                    ),
                  ]),
                ])
              )
              alignItems=`center
              justifyContent=`spaceBetween
              pl={`px(12)}
              pr={`px(16)}
              py={`px(8)}
              key={string_of_int(i)}
              onClick={_ => balance |> onTokenChange}>
              <Flex alignItems=`center>
                {switch (route) {
                 | LiquidityPool(RemoveLiquidity) =>
                   <DexterUi_PoolTokenIcon
                     icons=(
                       Tezos.Mutez.icon,
                       balance |> Dexter.Balance.getIcon,
                     )
                     small=true
                   />
                 | _ =>
                   <DexterUi_TokenIcon
                     icon={balance |> Dexter.Balance.getIcon}
                     small=true
                   />
                 }}
                <Flex
                  flexDirection=`column
                  alignItems=`flexStart
                  justifyContent=`center
                  ml={`px(12)}>
                  <Text
                    whiteSpace=`nowrap
                    textStyle=TextStyles.h1
                    darkModeColor=Colors.white
                    lightModeColor=Colors.black>
                    {switch (route) {
                     | LiquidityPool(RemoveLiquidity) =>
                       <DexterUi_PoolTokenSymbol
                         poolToken=(XtzBalance(Tezos_Mutez.zero), balance)
                       />
                     | _ => balance |> Dexter_Balance.getSymbol |> React.string
                     }}
                  </Text>
                  <Text whiteSpace=`nowrap>
                    {balance |> Dexter_Balance.getName |> React.string}
                  </Text>
                </Flex>
              </Flex>
              {switch (balance) {
               | ExchangeBalance(_) =>
                 <Flex flexDirection=`column alignItems=`flexEnd ml={`px(32)}>
                   <Text whiteSpace=`nowrap textStyle=TextStyles.h1>
                     {(
                        switch (route, side) {
                        | (Exchange, Left) =>
                          balance |> Dexter_Balance.getAccountBalanceAsString
                        | (Exchange, Right) =>
                          balance
                          |> Dexter_Balance.getExchangeTotalTokenAsString
                        | (LiquidityPool(AddLiquidity), _) =>
                          balance |> Dexter_Balance.getAccountBalanceAsString
                        | (LiquidityPool(RemoveLiquidity), _) =>
                          balance |> Dexter_Balance.getLiquidityAsString
                        | _ => ""
                        }
                      )
                      |> React.string}
                   </Text>
                   <Text whiteSpace=`nowrap>
                     {(
                        switch (route, side) {
                        | (Exchange, Left) => "You have"
                        | (Exchange, Right) => "Exchange has"
                        | (LiquidityPool(_), _) => "You have"
                        | _ => ""
                        }
                      )
                      |> React.string}
                   </Text>
                 </Flex>
               | _ => React.null
               }}
            </Flex>
          )
       |> Array.of_list
       |> React.array}
    </Flex>
  </Flex>;
};
