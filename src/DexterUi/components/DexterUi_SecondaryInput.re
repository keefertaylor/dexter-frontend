[@react.component]
let make =
    (
      ~disabled: bool=false,
      ~icon: option(React.element)=?,
      ~inputBackground: Flex.backgroundType=`transparent,
      ~inputBorderColor: Css.Types.Color.t=Colors.lightGrey,
      ~inputColor: option(Css.Types.Color.t)=?,
      ~inputTextAlign: Css.Types.TextAlign.t=`left,
      ~inputPlaceholderColor: option(Css.Types.Color.t)=?,
      ~noDarkMode: bool=false,
      ~onBlur: option(ReactEvent.Focus.t => unit)=?,
      ~onFocus: option(ReactEvent.Focus.t => unit)=?,
      ~onChange: ReactEvent.Form.t => unit,
      ~placeholder: string,
      ~pl: Css.Types.Length.t=`px(8),
      ~pr: Css.Types.Length.t=`px(8),
      ~translucent: bool=false,
      ~value: string,
      ~width: Flex.widthType,
    ) => {
  let {darkMode}: DexterUi_Context.t = DexterUi_Context.useContext();
  let translucent = translucent || disabled;
  let darkMode = !noDarkMode && darkMode;

  let inputColor =
    inputColor |> Utils.orDefault(darkMode ? Colors.offWhite : Colors.grey1);
  let inputPlaceholderColor =
    inputPlaceholderColor |> Utils.orDefault(Colors.text(darkMode));

  <Flex
    className=Css.(
      style([
        opacity(translucent ? 0.33 : 1.),
        border(pxFloat(darkMode ? 1. : 0.5), `solid, inputBorderColor),
      ])
    )
    background=inputBackground
    position=`relative
    borderRadius={`px(4)}
    alignItems=`center
    pl
    pr
    width
    height={`px(27)}>
    <input
      placeholder
      className=Css.(
        style([
          background(`none),
          border(`zero, `none, Colors.white),
          color(inputColor),
          fontSize(px(11)),
          margin(`zero),
          padding(`zero),
          textAlign(inputTextAlign),
          Css.placeholder([color(inputPlaceholderColor)]),
          Css.width(pct(100.)),
        ])
      )
      disabled
      value
      ?onBlur
      ?onFocus
      onChange
    />
    {icon
     |> Utils.renderOpt(icon =>
          !disabled
          |> Utils.renderIf(
               <Flex
                 position=`absolute
                 top=`zero
                 bottom=`zero
                 right={`px(6)}
                 alignItems=`center>
                 icon
               </Flex>,
             )
        )}
  </Flex>;
};
