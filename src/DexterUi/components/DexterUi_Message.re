open Utils;

[@react.component]
let make =
    (
      ~darkModeEnabled: bool=true,
      ~subtitle: option(React.element)=?,
      ~title: React.element,
    ) =>
  <Flex flexDirection=`column width={`percent(100.)} alignItems=`center>
    <Text
      textStyle=TextStyles.h1
      darkModeColor={darkModeEnabled ? Colors.white : Colors.black}
      lightModeColor=Colors.black>
      title
    </Text>
    {subtitle
     |> renderOpt(subtitle => {
          <Flex mt={`px(2)}>
            <Text
              textStyle=TextStyles.h2
              darkModeColor={darkModeEnabled ? Colors.offWhite : Colors.grey1}
              lightModeColor=Colors.grey1>
              subtitle
            </Text>
          </Flex>
        })}
  </Flex>;
