let xtzToTokenRate = (inputValue: Valid.t(InputType.t), xtz, token) =>
  switch (inputValue) {
  | Valid(Mutez(inputValue), _) when Tezos.Mutez.gtZero(inputValue) =>
    Dexter_Exchange.xtzToTokenExchangeRateForDisplay(inputValue, xtz, token)
  | _ => Dexter_Exchange.xtzToTokenMarketRateForDisplay(xtz, token)
  };

let tokenToXtzRate = (inputValue: Valid.t(InputType.t), xtz, token) =>
  switch (inputValue) {
  | Valid(Token(inputValue), _) when Tezos.Token.gtZero(inputValue) =>
    Dexter_Exchange.tokenToXtzExchangeRateForDisplay(inputValue, xtz, token)
  | _ => Dexter_Exchange.tokenToXtzMarketRateForDisplay(xtz, token)
  };

/** outputToken should never be XTZ */
let display =
    (
      inputValue: Valid.t(InputType.t),
      inputToken: Dexter_Balance.t,
      outputToken: Dexter_Balance.t,
    ) =>
  switch (inputToken, outputToken) {
  | (XtzBalance(_), ExchangeBalance(outputToken))
      when Tezos.Mutez.gtZero(outputToken.exchangeXtz) =>
    let rate =
      xtzToTokenRate(
        inputValue,
        outputToken.exchangeXtz,
        outputToken.exchangeTokenBalance,
      );

    let sign = rate < 0.00000001 ? " < " : " = ";
    let rate =
      rate < 0.00000001 ? "0.00000001" : Printf.sprintf("%.8f", rate);

    "1 XTZ" ++ sign ++ rate ++ " " ++ outputToken.symbol;
  | (ExchangeBalance(inputToken), XtzBalance(_))
      when Tezos.Token.gtZero(inputToken.exchangeTokenBalance) =>
    let rate =
      tokenToXtzRate(
        inputValue,
        inputToken.exchangeXtz,
        inputToken.exchangeTokenBalance,
      );

    let sign = rate < 0.00000001 ? " < " : " = ";
    let rate =
      rate < 0.00000001 ? "0.00000001" : Printf.sprintf("%.8f", rate);

    "1 " ++ inputToken.symbol ++ sign ++ rate ++ " XTZ";
  | (ExchangeBalance(token), XtzBalance(_))
  | (XtzBalance(_), ExchangeBalance(token)) =>
    "The " ++ token.symbol ++ " exchange has no liquidity"
  | _ => "No exchange rate"
  };

[@react.component]
let make =
    (
      ~inputValue: Valid.t(InputType.t),
      ~inputToken: Dexter_Balance.t,
      ~outputToken: Dexter_Balance.t,
    ) =>
  <DexterUi_Rate>
    {display(inputValue, inputToken, outputToken) |> React.string}
  </DexterUi_Rate>;
