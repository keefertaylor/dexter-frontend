[@react.component]
let make = (~blue: bool=false) =>
  <Flex
    width={`px(28)}
    minHeight={`percent(100.)}
    position=`relative
    justifyContent=`center>
    <Flex position=`absolute top={`px(128)}>
      <DexterUi_Icon
        darkModeSuffix="-d"
        size=18
        name={blue ? "and-blue" : "and"}
      />
    </Flex>
  </Flex>;
