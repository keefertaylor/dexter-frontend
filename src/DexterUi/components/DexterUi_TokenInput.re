/**
 * An input box for XTZ or tokens that belong to the logged in account.
 * The current value may be invalid.
 */

[@react.component]
let make =
    (
      ~disabled: bool=false,
      ~limit: option(InputType.t)=?,
      ~note: option(string)=?,
      ~aboveLimitNote: string="Insufficient balance.",
      ~withCurrencyValue: bool=true,
      ~onClickMax: option(unit => unit)=?,
      ~token: Dexter_Balance.t, /* all blockchain details about the account */
      ~updateValue: Valid.t(InputType.t) => unit,
      ~value: Valid.t(InputType.t),
    ) => {
  let currencyValue =
    switch (value) {
    | Valid(value, _) =>
      <DexterUi_CurrencyValue
        token=?{
          switch (token) {
          | ExchangeBalance(token) => Some(token)
          | _ => None
          }
        }
        value
      />
    | _ => <DexterUi_CurrencyValue />
    };

  let limit: InputType.t =
    limit
    |> Utils.orDefault(
         switch (token) {
         | XtzBalance(xtz) => Mutez(Tezos.Mutez.userMax(xtz))
         | ExchangeBalance(token) => Token(token.tokenBalance)
         }: InputType.t,
       );

  let isValid =
    switch (value, limit) {
    | (Valid(Mutez(mutez), _), Mutez(limit)) =>
      Tezos.Mutez.leqZero(mutez) || Tezos.Mutez.leq(mutez, limit)
    | (Valid(Token(token), _), Token(limit)) =>
      Tezos.Token.leqZero(token) || Tezos.Token.leq(token, limit)
    | (InvalidInitialState, _) => true
    | _ => false
    };

  <>
    <TokenInput
      currencyValue=?{withCurrencyValue ? Some(currencyValue) : None}
      disabled
      isValid
      onChange={v =>
        switch (v) {
        | "" => updateValue(InvalidInitialState)
        | value =>
          switch (limit) {
          | Mutez(_) =>
            let value = Tezos_Util.removeRedundantDecimals(value, 6);
            switch (Tezos.Mutez.ofTezString(value)) {
            | Belt.Result.Ok(i) => updateValue(Valid(Mutez(i), value))
            | Belt.Result.Error(_) => updateValue(Invalid(value))
            };
          | Token(limit) =>
            let value =
              Tezos_Util.removeRedundantDecimals(value, limit.decimals);
            switch (Tezos.Token.ofString(value, limit.decimals)) {
            | Belt.Result.Ok(i) => updateValue(Valid(Token(i), value))
            | Belt.Result.Error(_) => updateValue(Invalid(value))
            };
          }
        }
      }
      ?onClickMax
      value={
        switch (value) {
        | Valid(_, valueString) => valueString
        | Invalid(err) => err
        | InvalidInitialState => ""
        }
      }
    />
    {switch (note, aboveLimitNote, isValid, value) {
     | (Some(note), _, _, _)
     | (_, note, false, Valid(_)) =>
       <Flex position=`relative height={`px(12)}>
         <Flex mt={`px(5)} ml={`px(4)} position=`absolute>
           <Text color=?{isValid ? None : Some(Colors.red)}>
             {note |> React.string}
           </Text>
         </Flex>
       </Flex>
     | _ => React.null
     }}
  </>;
};
