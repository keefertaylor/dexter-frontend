type t =
  | Info
  | Warning
  | Error;

[@react.component]
let make = (~children: React.element, ~variant: t=Info) => {
  let {darkMode}: DexterUi_Context.t = DexterUi_Context.useContext();

  let (infoTextColor, infoBorderColor, infoBackgroundColor) =
    switch (variant) {
    | Error => (
        darkMode ? Colors.offWhite : Colors.red,
        darkMode ? Colors.red : Colors.red,
        darkMode ? Colors.darkRed : Colors.lightRed,
      )
    | Warning => (
        darkMode ? Colors.offWhite : Colors.blue,
        darkMode ? Colors.primaryDark : Colors.blue,
        darkMode ? Colors.tabInactiveDark : Colors.whiteGray,
      )
    | _ => (
        darkMode ? Colors.offWhite : Colors.blackish1,
        darkMode ? Colors.offBlack : Colors.offWhite,
        darkMode ? Colors.offBlack : Colors.offWhite,
      )
    };

  <Flex
    className=Css.(style([border(px(1), `solid, infoBorderColor)]))
    background=infoBackgroundColor
    px={`px(16)}
    py={`px(8)}
    borderRadius={`px(8)}
    alignItems=`baseline>
    <Text color=infoTextColor textAlign=`center> children </Text>
  </Flex>;
};
