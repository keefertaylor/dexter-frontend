[@react.component]
let make = () => {
  let {connectToBeacon, connectToTezBridge}: DexterUi_Context.t =
    DexterUi_Context.useContext();
  <>
    <Flex flexDirection=`column width={`percent(100.)}>
      <Flex
        alignItems=`center
        flexDirection=`column
        width={`percent(100.)}
        pt={`px(12)}>
        <DexterUi_Message
          title={"No wallet connected" |> React.string}
          subtitle={
            "Please connect your wallet using the available options."
            |> React.string
          }
        />
        <Flex height={`px(12)} />
        <DexterUi_Button px={`px(32)} onClick={_event => connectToBeacon()}>
          {"Connect Wallet" |> React.string}
        </DexterUi_Button>
        <Flex height={`px(12)} />
        <Flex
          justifyContent=`center
          mb={`px(20)}
          onClick={_ => connectToTezBridge()}>
          <Text
            darkModeColor=Colors.primaryDark
            lightModeColor=Colors.blue
            textDecoration=`underline>
            {"Connect via TezBridge" |> React.string}
          </Text>
        </Flex>
        <DexterUi_WalletInfoBalancesPlaceholder />
      </Flex>
    </Flex>
  </>;
};
