type t =
  | Mutez(Tezos.Mutez.t)
  | Token(Tezos.Token.t);

let toStringForDisplay = t => {
  switch (t) {
  | Mutez(a) => Tezos.Mutez.toTezStringWithCommas(a)
  | Token(b) => Tezos.Token.toStringWithCommas(b)
  };
};

let toTezFloat = (token: option(Dexter_ExchangeBalance.t), value: t) =>
  (
    switch (value) {
    | Token(value) =>
      token
      |> Utils.flatMap((token: Dexter_ExchangeBalance.t) =>
           Dexter_Exchange.tokenToXtz(
             value,
             token.exchangeXtz,
             token.exchangeTokenBalance,
           )
         )
      |> Utils.orDefault(Tezos.Mutez.zero)
    | Mutez(value) => value
    }
  )
  |> Tezos.Mutez.toTezFloat;

let encode = (t: t): Js.Json.t => {
  switch (t) {
  | Mutez(mutez) =>
    Json.Encode.object_([("mutez", Tezos.Mutez.encode(mutez))])
  | Token(token) =>
    Json.Encode.object_([("token", Tezos.Token.encode(token))])
  };
};

let decode = (json: Js.Json.t): Belt.Result.t(t, string) => {
  switch (Js_json.decodeObject(json)) {
  | Some(obj) =>
    switch (Js_dict.keys(obj)) {
    | [|key|] =>
      switch (key) {
      | "mutez" =>
        switch (Tezos.Mutez.decode(Js_dict.unsafeGet(obj, key))) {
        | Belt.Result.Ok(m) => Belt.Result.Ok(Mutez(m))
        | Belt.Result.Error(error) => Belt.Result.Error(error)
        }
      | "token" =>
        switch (Tezos.Token.decode(Js_dict.unsafeGet(obj, key))) {
        | Belt.Result.Ok(t) => Belt.Result.Ok(Token(t))
        | Belt.Result.Error(error) => Belt.Result.Error(error)
        }
      | _ => Belt.Result.Error("decode: Invalid key")
      }

    | _ => Belt.Result.Error("decode: Expected only one key in JSON Object")
    }
  | _ => Belt.Result.Error("decode: Expected JSON Object")
  };
};
