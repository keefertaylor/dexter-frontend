type t('a) =
  | Valid('a, string) /* maintain the input display value, may be slightly different from the final value */
  | Invalid(string)
  | InvalidInitialState;

let toString = (v, a_to_string: 'a => string) =>
  switch (v) {
  | Valid(a, _) => a_to_string(a)
  | Invalid(str) => str
  | InvalidInitialState => ""
  };

let isValid = v =>
  switch (v) {
  | Valid(_a, _) => true
  | Invalid(_str) => false
  | InvalidInitialState => false
  };

let isInvalid = v =>
  switch (v) {
  | Valid(_a, _) => false
  | Invalid(_str) => true
  | InvalidInitialState => false
  };

let map = (f: 'a => 'b, t: t('a)): t('b) => {
  switch (t) {
  | Valid(a, msg) => Valid(f(a), msg)
  | Invalid(str) => Invalid(str)
  | InvalidInitialState => InvalidInitialState
  };
};

let isEqual = (v1: t('a), v2: t('a)): bool => {
  switch (v1) {
  | Valid(a1, msg1) =>
    switch (v2) {
    | Valid(a2, msg2) => a1 === a2 && msg1 === msg2
    | _ => false
    }
  | Invalid(str1) =>
    switch (v2) {
    | Invalid(str2) => str1 === str2
    | _ => false
    }
  | InvalidInitialState =>
    switch (v2) {
    | InvalidInitialState => true
    | _ => false
    }
  };
};
