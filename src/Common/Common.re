let sequence =
    (results: list(Belt.Result.t('a, 'b))): Belt.Result.t(list('a), 'b) =>
  List.fold_left(
    (acc, result) =>
      switch (acc) {
      | Belt.Result.Ok(accs) =>
        switch (result) {
        | Belt.Result.Ok(ok) => Belt.Result.Ok(List.append(accs, [ok]))
        | Belt.Result.Error(err) => Belt.Result.Error(err)
        }

      | Belt.Result.Error(err) => Belt.Result.Error(err)
      },
    Belt.Result.Ok([]),
    results,
  );

/** With an list of options, eliminate all of the Nones, only keep the Somes */
let catSomes = (type a, mas: list(option(a))): list(a) =>
  List.fold_left(
    (acc, x) =>
      switch (x) {
      | None => acc
      | Some(a) => List.append(acc, [a])
      },
    [],
    mas,
  );

let getError = (r: Belt.Result.t('a, string)): string => {
  switch (r) {
  | Belt.Result.Error(err) => err
  | _ => ""
  };
};

let getErrors = (rs: list(Belt.Result.t('a, string))): string => {
  let oMsg =
    List.fold_left(
      (errors, result) => {
        switch (result) {
        | Belt.Result.Error(error) =>
          switch (errors) {
          | Some(errors) => Some(errors ++ ", " ++ error)
          | None => Some(error)
          }
        | _ => errors
        }
      },
      None,
      rs,
    );

  switch (oMsg) {
  | Some(msg) => msg
  | None => "No errors"
  };
};

let nbsp = [%raw {|'\u00a0'|}];
let iInCircle = [%raw {|'\u24D8'|}];

/* add the appropriate amount of commas to a number string */
let addCommas: string => string = [%bs.raw
  {|
    function (s) {
      var sp = s.split(".");
      var l = sp[0].replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
      if (sp.length > 1) {
        return l.concat(".", sp[1]);
      } else {
        return l;
      }
    }
     |}
];

let removeCommas: string => string = [%bs.raw
  {|
    function (s) {
      return s.replace(/,/g, "");
    }
   |}
];

let isSubstring = (string: string, substring: string) => {
  let string = Bytes.of_string(string);
  let substring = Bytes.of_string(substring);
  let ssl = Bytes.length(substring);
  let sl = Bytes.length(string);
  if (ssl == 0 || ssl > sl) {
    false;
  } else {
    let max = sl - ssl;
    let clone = Bytes.create(ssl);
    let rec check = pos =>
      pos <= max
      && {
        Bytes.blit(string, pos, clone, 0, ssl);
        clone == substring
        || check(
             Bytes.index_from(string, succ(pos), Bytes.get(substring, 0)),
           );
      };

    try(check(Bytes.index(string, Bytes.get(substring, 0)))) {
    | Not_found => false
    };
  };
};

[@bs.module] external uuid: unit => string = "uuid-random";

let promiseErrorToString: Js.Promise.error => string = [%bs.raw
  {|
   function (input) {
     return String(input);
   }
|}
];

let paste: unit => Js.Promise.t(string) = [%bs.raw
  {|
   function () {
     return navigator.clipboard.readText();
   }
|}
];

let reload: unit => unit = [%bs.raw
  {|
   function () {
     return location.reload();
   }
|}
];

let eventToValue = event => ReactEvent.Form.target(event)##value;

let packageVersion: unit => string = [%bs.raw
  {|
   function () {
     return VERSION;
   }
|}
];

module Deployment = {
  type t =
    | Production
    | Staging
    | Development;

  let ofRaw: unit => string = [%bs.raw
    {|
     function () {
       if (PROD) {
         return "production";
       } else {
         if (STAG) {
           return "staging";
         } else {
           return "development";
         };
       };
     }
    |}
  ];

  let get = () =>
    switch (ofRaw()) {
    | "production" => Production
    | "staging" => Staging
    | _ => Development
    };
};

let pendingTransactionMessage =
  "Please wait until the previous transaction is confirmed on the Tezos blockchain."
  |> React.string;

exception DecodeError(string);

let unwrapResult = r =>
  switch (r) {
  | Belt.Result.Ok(v) => v
  | Belt.Result.Error(message) => raise @@ DecodeError(message)
  };

// unsafe unwrap, useful for testing
let unwrapOption = r =>
  switch (r) {
  | Some(v) => v
  | None => raise @@ DecodeError("None")
  };
