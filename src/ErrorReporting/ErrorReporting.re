module Sentry = {
  type t;

  let init: unit => t = [%bs.raw
    {|
     function() {
       const Sentry = require('@sentry/browser');
       Sentry.init({
         dsn: "https://003bce79129f4cc797921029c9ac0a47@o359603.ingest.sentry.io/3491142",
         beforeSend(event, hint) {
           // Check if it is an exception, and if so, show the report dialog
           if (event.exception) {
             console.log(exception);
             Sentry.showReportDialog({
               eventId: event.event_id,
               title: "It looks like Dexter is having issues.",
               subtitle: "Our team has been notified.",
               subtitle2: "If you’d like to help, tell us what happened below."
             });
           }
           return event;
         }

       });
     }
    |}
  ];

  let captureException: string => unit = [%bs.raw
    {|
     function(t, errorMessage) {
       const Sentry = require('@sentry/browser');
       Sentry.captureException(errorMessage);
     }
    |}
  ];

  let capturePromiseError: (string, Js.Promise.error) => unit = [%bs.raw
    {|
     function(breadcrumb, errorMessage) {
       const Sentry = require('@sentry/browser');
       Sentry.captureException(breadcrumb + errorMessage);
     }
    |}
  ];
};
