open Utils;

type gradientType = [
  | `currentColor
  | `hex(string)
  | `hsl(
      Css_AtomicTypes.Angle.t,
      Css_AtomicTypes.Percentage.t,
      Css_AtomicTypes.Percentage.t,
    )
  | `hsla(
      Css_AtomicTypes.Angle.t,
      Css_AtomicTypes.Percentage.t,
      Css_AtomicTypes.Percentage.t,
      [ | `num(float) | `percent(float)],
    )
  | `rgb(int, int, int)
  | `rgba(int, int, int, [ | `num(float) | `percent(float)])
  | `transparent
  | `var(string)
  | `varDefault(string, string)
];

type backgroundType = [
  Css.Types.Color.t
  | Css.Types.Url.t
  | Css.Types.Gradient.t(gradientType)
  | `none
];
type flexType = [ | `auto | `initial | `none | `num(float)];
type flexDirectionType = [ Css.Types.FlexDirection.t | Css.Types.Cascading.t];
type flexWrapType = [ Css.Types.FlexWrap.t | Css.Types.Cascading.t];
type flexBasisType = [
  Css.Types.FlexBasis.t
  | Css.Types.Percentage.t
  | Css.Types.Length.t
];
type justifyContentType = [
  Css.Types.PositionalAlignment.t
  | Css.Types.NormalAlignment.t
  | Css.Types.DistributedAlignment.t
  | Css.Types.Cascading.t
];
type alignItemsType = [
  Css.Types.AlignItems.t
  | Css.Types.PositionalAlignment.t
  | Css.Types.BaselineAlignment.t
  | Css.Types.Cascading.t
];
type marginType = [ Css.Types.Length.t | Css.Types.Margin.t];
type widthType = [
  Css.Types.Width.t
  | Css.Types.Percentage.t
  | Css.Types.Length.t
  | Css.Types.Cascading.t
];
type heightType = [
  Css.Types.Height.t
  | Css.Types.Percentage.t
  | Css.Types.Length.t
  | Css.Types.Cascading.t
];
type maxWidthType = [
  Css.Types.MaxWidth.t
  | Css.Types.Percentage.t
  | Css.Types.Length.t
  | Css.Types.Cascading.t
];
type maxHeightType = [
  Css.Types.MaxHeight.t
  | Css.Types.Percentage.t
  | Css.Types.Length.t
  | Css.Types.Cascading.t
];
type positionLenghtsType = [ Css.Types.Length.t | Css.Types.Cascading.t];

type positionType = [ Css.Types.Position.t | Css.Types.Cascading.t];

let applyStyle = (a, b, c, func) =>
  switch (a, b, c) {
  | (Some(a), _, _) => Some(func(a))
  | (_, Some(b), _) => Some(func(b))
  | (_, _, Some(c)) => Some(func(c))
  | _ => None
  };

[@react.component]
let make =
    (
      ~alignItems: option(alignItemsType)=?,
      ~background: option(backgroundType)=?,
      ~borderRadius: option(Css.Types.Length.t)=?,
      ~children: option(React.element)=?,
      ~className: string="",
      ~disabled: bool=false,
      ~flex: flexType=`initial,
      ~flexBasis: option(flexBasisType)=?,
      ~flexDirection: flexDirectionType=`row,
      ~flexGrow: option(float)=?,
      ~flexShrink: option(float)=?,
      ~flexWrap: flexWrapType=`nowrap,
      ~full: bool=false,
      ~height: option(heightType)=?,
      ~inlineFlex: bool=false,
      ~justifyContent: option(justifyContentType)=?,
      ~m: marginType=`zero,
      ~maxHeight: option(maxHeightType)=?,
      ~maxWidth: option(maxWidthType)=?,
      ~mb: option(marginType)=?,
      ~minHeight: option(heightType)=?,
      ~minWidth: option(widthType)=?,
      ~ml: option(marginType)=?,
      ~mr: option(marginType)=?,
      ~mt: option(marginType)=?,
      ~mx: option(marginType)=?,
      ~my: option(marginType)=?,
      ~onClick: option(ReactEvent.Mouse.t => unit)=?,
      ~overflow: option(Css.Types.Overflow.t)=?,
      ~overflowX: option(Css.Types.Overflow.t)=?,
      ~overflowY: option(Css.Types.Overflow.t)=?,
      ~position: option(positionType)=?,
      ~p: option(Css.Types.Length.t)=?,
      ~pb: option(Css.Types.Length.t)=?,
      ~pl: option(Css.Types.Length.t)=?,
      ~pr: option(Css.Types.Length.t)=?,
      ~pt: option(Css.Types.Length.t)=?,
      ~px: option(Css.Types.Length.t)=?,
      ~py: option(Css.Types.Length.t)=?,
      ~top: option(positionLenghtsType)=?,
      ~left: option(positionLenghtsType)=?,
      ~bottom: option(positionLenghtsType)=?,
      ~right: option(positionLenghtsType)=?,
      ~width: option(widthType)=?,
      ~zIndex: option(int)=?,
    ) => {
  let style =
    [|
      applyStyle(mb, my, Some(m), Css.marginBottom),
      applyStyle(ml, mx, Some(m), Css.marginLeft),
      applyStyle(mr, mx, Some(m), Css.marginRight),
      applyStyle(mt, my, Some(m), Css.marginTop),
      applyStyle(pb, py, p, Css.paddingBottom),
      applyStyle(pl, px, p, Css.paddingLeft),
      applyStyle(pr, px, p, Css.paddingRight),
      applyStyle(pt, py, p, Css.paddingTop),
      top |> map(top => Css.top(top)),
      left |> map(left => Css.left(left)),
      bottom |> map(bottom => Css.bottom(bottom)),
      right |> map(right => Css.right(right)),
      alignItems |> map(alignItems => Css.alignItems(alignItems)),
      background |> map(background => Css.background(background)),
      borderRadius |> map(borderRadius => Css.borderRadius(borderRadius)),
      flexBasis |> map(flexBasis => Css.flexBasis(flexBasis)),
      flexGrow |> map(flexGrow => Css.flexGrow(flexGrow)),
      flexShrink |> map(flexShrink => Css.flexShrink(flexShrink)),
      height |> map(height => Css.height(height)),
      justifyContent
      |> map(justifyContent => Css.justifyContent(justifyContent)),
      maxHeight |> map(maxHeight => Css.maxHeight(maxHeight)),
      maxWidth |> map(maxWidth => Css.maxWidth(maxWidth)),
      minHeight |> map(minHeight => Css.minHeight(minHeight)),
      minWidth |> map(minWidth => Css.minWidth(minWidth)),
      overflow |> map(overflow => Css.overflow(overflow)),
      overflowX |> map(overflowX => Css.overflowX(overflowX)),
      overflowY |> map(overflowY => Css.overflowY(overflowY)),
      position |> map(position => Css.position(position)),
      onClick |> map(_ => Css.cursor(disabled ? `default : `pointer)),
      width |> map(width => Css.width(width)),
      zIndex |> map(zIndex => Css.zIndex(zIndex)),
    |]
    |> filterNone
    |> Array.to_list;

  let elementClassName =
    Css.merge([
      Css.style([
        Css.boxSizing(`borderBox),
        Css.display(inlineFlex ? `inlineFlex : `flex),
        Css.flex(flex),
        Css.flexDirection(flexDirection),
        Css.flexWrap(flexWrap),
        Css.width(full ? Css.pct(100.0) : `auto),
        Css.maxWidth(Css.pct(100.0)),
        Css.focus([Css.outline(Css.px(0), `solid, Colors.white)]),
        Css.background(`transparent),
        Css.border(Css.px(0), `solid, Colors.white),
        Css.opacity(disabled ? 0.33 : 1.),
        Css.selector(
          "> img",
          [Css.maxWidth(`percent(100.)), Css.maxHeight(`percent(100.))],
        ),
        ...style,
      ]),
      className,
    ]);
  switch (onClick) {
  | Some(onClick) when !disabled =>
    <button type_="button" className=elementClassName disabled onClick>
      {children |> renderOpt(children => children)}
    </button>
  | _ when disabled =>
    <button type_="button" className=elementClassName disabled>
      {children |> renderOpt(children => children)}
    </button>
  | _ =>
    <div className=elementClassName>
      {children |> renderOpt(children => children)}
    </div>
  };
};
