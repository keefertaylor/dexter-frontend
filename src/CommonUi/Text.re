open Utils;

type fontWeightType = [ Css.Types.FontWeight.t | Css.Types.Cascading.t];
type fontSizeType = [ Css.Types.Length.t | Css.Types.Cascading.t];
type textAlignType = [ Css.Types.TextAlign.t | Css.Types.Cascading.t];
type textFontStyleType = [ Css.Types.FontStyle.t | Css.Types.Cascading.t];
type textLineHeightType = [
  Css.Types.LineHeight.t
  | Css.Types.Length.t
  | Css.Types.Cascading.t
];
type textTransformType = [ Css.Types.TextTransform.t | Css.Types.Cascading.t];
type whiteSpaceType = [ Css.Types.WhiteSpace.t | Css.Types.Cascading.t];
type textDecorationType = [
  | `none
  | `underline
  | `overline
  | `lineThrough
  | Css.Types.Var.t
  | Css.Types.Cascading.t
];

[@react.component]
let make =
    (
      ~children: React.element,
      ~className: string="",
      ~color: option(Css.Types.Color.t)=?,
      ~cursor: option(Css.Types.Cursor.t)=?,
      ~darkModeColor: Css.Types.Color.t=Colors.text(true),
      ~ellipsis=false,
      ~fontFamily: string="Source Sans Pro",
      ~fontSize: option(fontSizeType)=?,
      ~fontStyle: option(textFontStyleType)=?,
      ~fontWeight: option(fontWeightType)=?,
      ~full: bool=false,
      ~hoverColor: option(Css.Types.Color.t)=?,
      ~hoverFontWeight: option(fontWeightType)=?,
      ~inlineBlock: bool=true,
      ~lightModeColor: Css.Types.Color.t=Colors.text(false),
      ~lineHeight: option(textLineHeightType)=?,
      ~spaceAfter: bool=false,
      ~spaceBefore: bool=false,
      ~textAlign: option(textAlignType)=?,
      ~textDecoration: option(textDecorationType)=?,
      ~textStyle: TextStyles.textRules=TextStyles.default,
      ~textTransform: option(textTransformType)=?,
      ~whiteSpace: option(whiteSpaceType)=?,
    ) => {
  let {darkMode}: DexterUi_Context.t = DexterUi_Context.useContext();

  let hoverStyle =
    switch (hoverColor, hoverFontWeight) {
    | (Some(hoverColor), Some(hoverFontWeight)) =>
      Some([
        Css.selector("> *", [Css.color(hoverColor)]),
        Css.color(hoverColor),
        Css.fontWeight(hoverFontWeight),
      ])
    | (Some(hoverColor), _) =>
      Some([
        Css.selector("> *", [Css.color(hoverColor)]),
        Css.color(hoverColor),
      ])
    | (_, Some(hoverFontWeight)) => Some([Css.fontWeight(hoverFontWeight)])
    | _ => None
    };

  let style =
    [|
      fontStyle |> map(fontStyle => Css.fontStyle(fontStyle)),
      fontWeight |> map(fontWeight => Css.fontWeight(fontWeight)),
      textAlign |> map(textAlign => Css.textAlign(textAlign)),
      lineHeight |> map(lineHeight => Css.lineHeight(lineHeight)),
      fontSize |> map(fontSize => Css.fontSize(fontSize)),
      textDecoration
      |> map(textDecoration => Css.textDecoration(textDecoration)),
      textTransform |> map(textTransform => Css.textTransform(textTransform)),
      hoverStyle |> map(hoverStyle => Css.hover(hoverStyle)),
      whiteSpace |> map(whiteSpace => Css.whiteSpace(whiteSpace)),
      cursor |> map(cursor => Css.cursor(cursor)),
    |]
    |> filterNone
    |> Array.to_list;

  let color = color |> orDefault(darkMode ? darkModeColor : lightModeColor);

  let elementClassName =
    Css.merge([
      "text",
      Css.style(textStyle),
      Css.style([
        Css.display(inlineBlock ? `inlineBlock : `inline),
        Css.color(color),
        Css.selector("> *", [Css.color(color)]),
        Css.fontFamilies([`custom(fontFamily), `sansSerif]),
        Css.width(full ? Css.pct(100.) : `auto),
        Css.textOverflow(ellipsis ? `ellipsis : `initial),
        Css.maxWidth(Css.pct(100.)),
        Css.overflow(ellipsis ? `hidden : `visible),
        Css.before(spaceBefore ? [Css.contentRule(`text("\\00a0 "))] : []),
        Css.after(spaceAfter ? [Css.contentRule(`text("\\00a0 "))] : []),
        ...style,
      ]),
      className,
    ]);

  <span className=elementClassName> children </span>;
};
