/**
 * Indexter is a service that retrieves the latest values from FA1.2 view entry
 * points.
 */

/*
 | PATH                                         | Description                                          | type        | error    |
 |----------------------------------------------|------------------------------------------------------|:-----------:|:--------:|
 | /fa12/{contract}/balance/{owner}             | Gets the balance of an {owner} in an FA1.2 {contract}| int (MUTEZ) | string   |
 | /fa12/{contract}/supply                      | Gets the supply of an FA1.2 {contract}               | int (MUTEZ) | string   |
 | /fa12/{contract}/allowance/{owner}/{spender} | Gets the allowance of a {spender} for an {owner} in an FA1.2 {contract}  | int (MUTEZ) | string   |
 */

type route =
  | Balance(Tezos.Contract.t, Tezos.Address.t)
  | BalanceNow(Tezos.Contract.t, Tezos.Address.t)
  | TotalSupply(Tezos.Contract.t)
  | Allowance(Tezos.Contract.t, Tezos.Address.t, Tezos.Address.t);

let toRoute = (baseUrl: string, route: route) =>
  baseUrl
  ++ (
    switch (route) {
    | Balance(contract, owner) =>
      "/fa12/"
      ++ Tezos.Contract.toString(contract)
      ++ "/balance/"
      ++ Tezos.Address.toString(owner)
    | BalanceNow(contract, owner) =>
      "/fa12/"
      ++ Tezos.Contract.toString(contract)
      ++ "/balance/"
      ++ Tezos.Address.toString(owner)
      ++ "/now"
    | TotalSupply(contract) =>
      "/fa12/" ++ Tezos.Contract.toString(contract) ++ "/supply"
    | Allowance(contract, owner, spender) =>
      "/fa12/"
      ++ Tezos.Contract.toString(contract)
      ++ "/allowance/"
      ++ Tezos.Address.toString(owner)
      ++ "/"
      ++ Tezos.Address.toString(spender)
    }
  );

let getBalance =
    (
      ~decimals: int,
      ~baseUrl: string,
      ~contract: Tezos.Contract.t,
      ~owner: Tezos.Address.t,
      ~bypassCache: bool=false,
      (),
    )
    : Js.Promise.t(Belt.Result.t(Tezos_Token.t, string)) => {
  let url =
    toRoute(
      baseUrl,
      bypassCache ? BalanceNow(contract, owner) : Balance(contract, owner),
    );

  Js.Promise.(
    Fetch.fetchWithInit(
      url,
      Fetch.RequestInit.make(
        ~method_=Get,
        ~headers=
          Fetch.HeadersInit.makeWithArray([|
            ("Accept", "application/json"),
          |]),
        ~credentials=Omit,
        (),
      ),
    )
    |> then_(Fetch.Response.json)
    |> then_(json => resolve(Tezos_Token.decodeFromString(decimals, json)))
    |> catch(error => {
         Js.Console.log(error);
         ErrorReporting.Sentry.capturePromiseError(
           "Indexter.getBalance failed: ",
           error,
         );
         resolve(
           Belt.Result.Error("Indexter.getBalance failed. Promise error."),
         );
       })
  );
};

let getTotalSupply =
    (~decimals: int, ~baseUrl: string, ~contract: Tezos.Contract.t, ())
    : Js.Promise.t(Belt.Result.t(Tezos_Token.t, string)) => {
  let url = toRoute(baseUrl, TotalSupply(contract));
  Js.Promise.(
    Fetch.fetchWithInit(
      url,
      Fetch.RequestInit.make(
        ~method_=Get,
        ~headers=
          Fetch.HeadersInit.makeWithArray([|
            ("Accept", "application/json"),
          |]),
        ~credentials=Omit,
        (),
      ),
    )
    |> then_(Fetch.Response.json)
    |> then_(json => resolve(Tezos_Token.decodeFromString(decimals, json)))
    |> catch(error => {
         Js.Console.log(error);
         ErrorReporting.Sentry.capturePromiseError(
           "Indexter.getTotalSupply failed: ",
           error,
         );
         resolve(
           Belt.Result.Error(
             "Indexter.getTotalSupply failed. Promise error.",
           ),
         );
       })
  );
};

let getAllowance =
    (
      ~decimals: int,
      ~baseUrl: string,
      ~contract: Tezos.Contract.t,
      ~owner: Tezos.Address.t,
      ~spender: Tezos.Address.t,
      (),
    )
    : Js.Promise.t(Belt.Result.t(Tezos_Token.t, string)) => {
  let url = toRoute(baseUrl, Allowance(contract, owner, spender));
  Js.Promise.(
    Fetch.fetchWithInit(
      url,
      Fetch.RequestInit.make(
        ~method_=Get,
        ~headers=
          Fetch.HeadersInit.makeWithArray([|
            ("Accept", "application/json"),
          |]),
        ~credentials=Omit,
        (),
      ),
    )
    |> then_(Fetch.Response.json)
    |> then_(json => resolve(Tezos_Token.decodeFromString(decimals, json)))
    |> catch(error => {
         Js.Console.log(error);
         ErrorReporting.Sentry.capturePromiseError(
           "Indexter.getAllowance failed: ",
           error,
         );
         resolve(
           Belt.Result.Error("Indexter.getAllowance failed. Promise error."),
         );
       })
  );
};
