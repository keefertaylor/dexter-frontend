let explode = s => List.init(String.length(s), String.get(s));

let isDigit = char => {
  let c = Char.code(char);
  c >= 48 && c <= 57;
};

// 45 is -

let isStringOfDigits = s =>
  if (String.length(s) === 0) {
    false;
  } else if (String.length(s) === 1) {
    List.fold_right((c, x) => isDigit(c) && x, explode(s), true);
  } else {
    // could start with a minus symbol
    let chars = explode(s);
    let first = List.hd(chars);
    let rest = List.tl(chars);
    (isDigit(first) || first === '-')
    && List.fold_right((c, x) => isDigit(c) && x, rest, true);
  };

let int64_of_string = json =>
  if (Js.typeof(json) == "string") {
    let source: string = Obj.magic(json: Js.Json.t);

    if (isStringOfDigits(source)) {
      Int64.of_string(source);
    } else {
      raise @@ Json.Decode.DecodeError("Expected int64, got " ++ source);
    };
  } else {
    raise @@
    Json.Decode.DecodeError(
      "Expected int64, got " ++ Js.Json.stringify(json),
    );
  };

let bigint = json =>
  if (Js.typeof(json) == "string") {
    let source: string = Obj.magic(json: Js.Json.t);

    if (isStringOfDigits(source)) {
      Bigint.of_string(source);
    } else {
      raise @@ Json.Decode.DecodeError("Expected Bigint.t, got " ++ source);
    };
  } else {
    raise @@
    Json.Decode.DecodeError(
      "Expected Bigint.t, got " ++ Js.Json.stringify(json),
    );
  };

let unwrapResult = Tezos_Util.unwrapResult;

type transaction = {
  source: string,
  fee: Tezos_Mutez.t,
  counter: Bigint.t,
  gasLimit: Bigint.t,
  storageLimit: Bigint.t,
  destination: string,
  parameters: string,
};

type transactionResult = {
  source: string,
  fee: Tezos_Mutez.t,
  counter: Bigint.t,
  gasLimit: Bigint.t,
  storageLimit: Bigint.t,
  destination: string,
  parameters: string,
  metadata: string,
};

/* { "balance_updates": $operation_metadata.alpha.balance_updates, */
/*   "operation_result": */
/*     $operation.alpha.operation_result.transaction, */
/*   "internal_operation_results"?: */
/*     [ $operation.alpha.internal_operation_result ... ] } } */

// $operation_metadata.alpha.balance_updates
module BalanceUpdate = {
  module Contract = {
    type t = {
      contract: string,
      change: Int64.t,
    };

    let decode = (json: Js.Json.t): Belt.Result.t(t, string) => {
      Json.Decode.(
        switch (
          field("contract", string, json),
          field("change", int64_of_string, json),
        ) {
        | (contract, change) => Belt.Result.Ok({contract, change})
        | exception (DecodeError(error)) => Belt.Result.Error(error)
        }
      );
    };
  };

  module Rewards = {
    type t = {
      delegate: string,
      cycle: int,
      change: Int64.t,
    };

    let decode = (json: Js.Json.t): Belt.Result.t(t, string) => {
      Json.Decode.(
        switch (
          field("delegate", string, json),
          field("cycle", int, json),
          field("change", int64_of_string, json),
        ) {
        | (delegate, cycle, change) =>
          Belt.Result.Ok({delegate, cycle, change})
        | exception (DecodeError(error)) => Belt.Result.Error(error)
        }
      );
    };
  };

  module Fees = {
    type t = {
      delegate: string,
      cycle: int,
      change: Int64.t,
    };

    let decode = (json: Js.Json.t): Belt.Result.t(t, string) => {
      Json.Decode.(
        switch (
          field("delegate", string, json),
          field("cycle", int, json),
          field("change", int64_of_string, json),
        ) {
        | (delegate, cycle, change) =>
          Belt.Result.Ok({delegate, cycle, change})
        | exception (DecodeError(error)) => Belt.Result.Error(error)
        }
      );
    };
  };

  module Deposits = {
    type t = {
      delegate: string,
      cycle: int,
      change: Int64.t,
    };

    let decode = (json: Js.Json.t): Belt.Result.t(t, string) => {
      Json.Decode.(
        switch (
          field("delegate", string, json),
          field("cycle", int, json),
          field("change", int64_of_string, json),
        ) {
        | (delegate, cycle, change) =>
          Belt.Result.Ok({delegate, cycle, change})
        | exception (DecodeError(error)) => Belt.Result.Error(error)
        }
      );
    };
  };

  type t =
    | Contract(Contract.t)
    | Rewards(Rewards.t)
    | Fees(Fees.t)
    | Deposits(Deposits.t);

  let decode = (json: Js.Json.t): Belt.Result.t(t, string) => {
    Json.Decode.(
      switch (field("kind", string, json)) {
      | "contract" =>
        Belt.Result.map(Contract.decode(json), a => Contract(a))

      | "freezer" =>
        switch (field("category", string, json)) {
        | "rewards" => Belt.Result.map(Rewards.decode(json), a => Rewards(a))
        | "fees" => Belt.Result.map(Fees.decode(json), a => Fees(a))
        | "deposits" =>
          Belt.Result.map(Deposits.decode(json), a => Deposits(a))
        | category => Belt.Result.Error("Unexpected category: " ++ category)
        | exception (DecodeError(error)) => Belt.Result.Error(error)
        }
      | kind => Belt.Result.Error("Unexpected kind: " ++ kind)
      | exception (DecodeError(error)) => Belt.Result.Error(error)
      }
    );
  };
};

// $operation.alpha.operation_result.transaction
module Operation = {
  module Result = {
    module Transaction = {
      module Applied = {
        type t = {
          storage: option(Tezos_Expression.t),
          bigMapDiff: option(Tezos_Expression.t),
          balanceUpdates: option(list(BalanceUpdate.t)),
          originatedContracts: option(list(string)),
          consumedGas: option(Bigint.t),
          storageSize: option(Bigint.t),
          paidStorageSizeDiff: option(Bigint.t),
          allocatedDestinationContract: option(bool),
        };

        // optional(field("annots", list(string)), json)
        let decode = (json: Js.Json.t): Belt.Result.t(t, string) => {
          Json.Decode.(
            switch (
              optional(
                field("storage", a =>
                  unwrapResult(Tezos_Expression.decode(a))
                ),
                json,
              ),
              optional(
                field("big_map_diff", a =>
                  unwrapResult(Tezos_Expression.decode(a))
                ),
                json,
              ),
              optional(
                field("balance_updates", a =>
                  list(b => unwrapResult(BalanceUpdate.decode(b)), a)
                ),
                json,
              ),
              optional(field("originated_contracts", list(string)), json),
              optional(field("consumed_gas", bigint), json),
              optional(field("storage_size", bigint), json),
              optional(field("paid_storage_size_diff", bigint), json),
              optional(field("allocated_destination_contract", bool), json),
            ) {
            | (
                storage,
                bigMapDiff,
                balanceUpdates,
                originatedContracts,
                consumedGas,
                storageSize,
                paidStorageSizeDiff,
                allocatedDestinationContract,
              ) =>
              Belt.Result.Ok({
                storage,
                bigMapDiff,
                balanceUpdates,
                originatedContracts,
                consumedGas,
                storageSize,
                paidStorageSizeDiff,
                allocatedDestinationContract,
              })
            | exception (DecodeError(error)) => Belt.Result.Error(error)
            }
          );
        };
      };

      type t =
        | Applied(Applied.t)
        | Failed
        | Skipped
        | Backtracked;

      let decode = (json: Js.Json.t): Belt.Result.t(t, string) => {
        Json.Decode.(
          switch (field("status", string, json)) {
          | "applied" =>
            Belt.Result.map(Applied.decode(json), a => Applied(a))
          | "failed" => Belt.Result.Ok(Failed)
          | "skipped" => Belt.Result.Ok(Skipped)
          | "backtracked" => Belt.Result.Ok(Backtracked)
          | status => Belt.Result.Error("Unexpected status: " ++ status)
          | exception (DecodeError(error)) => Belt.Result.Error(error)
          }
        );
      };
    };
  };
};

module Entrypoint = {
  type t =
    | Default
    | Root
    | Do
    | SetDelegate
    | RemoveDelegate
    | Named(string);

  let decode = (json: Js.Json.t) => {
    switch (Json.Decode.string(json)) {
    | "default" => Belt.Result.Ok(Default)
    | "root" => Belt.Result.Ok(Root)
    | "do" => Belt.Result.Ok(Do)
    | "set_delegate" => Belt.Result.Ok(SetDelegate)
    | "remove_delegate" => Belt.Result.Ok(RemoveDelegate)
    | str => Belt.Result.Ok(Named(str))
    | exception (Json.Decode.DecodeError(error)) => Belt.Result.Error(error)
    };
  };
};

module Parameters = {
  type t = {
    entrypoint: Entrypoint.t,
    value: Tezos_Expression.t,
  };

  let decode = (json: Js.Json.t) => {
    switch (
      Json.Decode.(
        field("entrypoint", a => unwrapResult(Entrypoint.decode(a)), json),
        field("value", a => unwrapResult(Tezos_Expression.decode(a)), json),
      )
    ) {
    | (entrypoint, value) => Belt.Result.Ok({entrypoint, value})
    | exception (Json.Decode.DecodeError(error)) => Belt.Result.Error(error)
    };
  };
};

// $operation.alpha.internal_operation_result:
module Internal = {
  module Operation = {
    module Result = {
      module Transaction = {
        type t = {
          source: string,
          amount: Tezos_Mutez.t,
          destination: string,
          parameters: option(Parameters.t),
        };

        let decode = (json: Js.Json.t) => {
          switch (
            Json.Decode.(
              field("source", string, json),
              field(
                "amount",
                a => unwrapResult(Tezos_Mutez.decode(a)),
                json,
              ),
              field("destination", string, json),
              optional(
                field("parameters", a => unwrapResult(Parameters.decode(a))),
                json,
              ),
            )
          ) {
          | (source, amount, destination, parameters) =>
            Belt.Result.Ok({source, amount, destination, parameters})

          | exception (Json.Decode.DecodeError(error)) =>
            Belt.Result.Error(error)
          };
        };
      };

      type t =
        | Reveal
        | Transaction(Transaction.t)
        | Origination
        | Delegation;

      let decode = (json: Js.Json.t) => {
        switch (Json.Decode.(field("kind", string, json))) {
        | "reveal" => Belt.Result.Ok(Reveal)
        | "transaction" =>
          Belt.Result.map(Transaction.decode(json), a => Transaction(a))
        | "origination" => Belt.Result.Ok(Origination)
        | "delegation" => Belt.Result.Ok(Delegation)
        | kind => Belt.Result.Error("Unexpected kind: " ++ kind)
        | exception (Json.Decode.DecodeError(error)) =>
          Belt.Result.Error(error)
        };
      };
    };
  };
};

module Contents = {
  module Result = {
    module Transaction = {
      module Metadata = {
        type t = {
          balanceUpdates: list(BalanceUpdate.t),
          operationResult: Operation.Result.Transaction.t,
          internalOperationResults:
            option(list(Internal.Operation.Result.t)),
        };

        let decode = (json: Js.Json.t) => {
          switch (
            Json.Decode.(
              field(
                "balance_updates",
                a => list(b => unwrapResult(BalanceUpdate.decode(b)), a),
                json,
              ),
              field(
                "operation_result",
                a => unwrapResult(Operation.Result.Transaction.decode(a)),
                json,
              ),
              optional(
                field("internal_operation_results", a =>
                  list(
                    b => unwrapResult(Internal.Operation.Result.decode(b)),
                    a,
                  )
                ),
                json,
              ),
            )
          ) {
          | (balanceUpdates, operationResult, internalOperationResults) =>
            Belt.Result.Ok({
              balanceUpdates,
              operationResult,
              internalOperationResults,
            })
          | exception (Json.Decode.DecodeError(error)) =>
            Belt.Result.Error(error)
          };
        };
      };

      type t = {
        source: string,
        fee: Tezos_Mutez.t,
        counter: Bigint.t,
        gasLimit: Bigint.t,
        storageLimit: Bigint.t,
        amount: Tezos_Mutez.t,
        destination: string, /* contract id */
        parameters: option(Parameters.t),
        metadata: Metadata.t,
      };

      let decode = (json: Js.Json.t) => {
        switch (
          Json.Decode.(
            field("source", string, json),
            field("fee", a => unwrapResult(Tezos_Mutez.decode(a)), json),
            field("counter", bigint, json),
            field("gas_limit", bigint, json),
            field("storage_limit", bigint, json),
            field("amount", a => unwrapResult(Tezos_Mutez.decode(a)), json),
            field("destination", string, json),
            optional(
              field("parameters", a => unwrapResult(Parameters.decode(a))),
              json,
            ),
            field("metadata", a => unwrapResult(Metadata.decode(a)), json),
          )
        ) {
        | (
            source,
            fee,
            counter,
            gasLimit,
            storageLimit,
            amount,
            destination,
            parameters,
            metadata,
          ) =>
          Belt.Result.Ok({
            source,
            fee,
            counter,
            gasLimit,
            storageLimit,
            amount,
            destination,
            parameters,
            metadata,
          })
        | exception (Json.Decode.DecodeError(error)) =>
          Belt.Result.Error(error)
        };
      };
    };

    // $operation.alpha.operation_contents_and_result
    type t =
      | Endorsement
      | SeedNonceRevelation
      | DoubleEndorsementEvidence
      | DoubleBakingEvidence
      | ActivateAccount
      | Proposals
      | Ballot
      | Reveal
      | Transaction(Transaction.t)
      | Origination
      | Delegation;

    let decode = (json: Js.Json.t): Belt.Result.t(t, string) => {
      Json.Decode.(
        switch (field("kind", string, json)) {
        | "endorsement" => Belt.Result.Ok(Endorsement)

        | "seed_nonce_revelation" => Belt.Result.Ok(SeedNonceRevelation)

        | "double_endorsement_evidence" =>
          Belt.Result.Ok(DoubleEndorsementEvidence)

        | "double_baking_evidence" => Belt.Result.Ok(DoubleBakingEvidence)
        | "activate_account" => Belt.Result.Ok(ActivateAccount)
        | "proposals" => Belt.Result.Ok(Proposals)
        | "ballot" => Belt.Result.Ok(Ballot)

        | "reveal" => Belt.Result.Ok(Reveal)
        | "transaction" =>
          Belt.Result.map(Transaction.decode(json), a => Transaction(a))
        | "origination" => Belt.Result.Ok(Origination)
        | "delegation" => Belt.Result.Ok(Delegation)
        | kind => Belt.Result.Error("Unexpected kind: " ++ kind)
        | exception (DecodeError(error)) => Belt.Result.Error(error)
        }
      );
    };
  };

  module Transaction = {
    type t = {
      source: string,
      fee: Tezos_Mutez.t,
      counter: Bigint.t,
      gasLimit: Bigint.t,
      storageLimit: Bigint.t,
      amount: Tezos_Mutez.t,
      destination: string, /* contract id */
      parameters: option(Parameters.t),
    };

    let decode = (json: Js.Json.t) => {
      switch (
        Json.Decode.(
          field("source", string, json),
          field("fee", a => unwrapResult(Tezos_Mutez.decode(a)), json),
          field("counter", bigint, json),
          field("gas_limit", bigint, json),
          field("storage_limit", bigint, json),
          field("amount", a => unwrapResult(Tezos_Mutez.decode(a)), json),
          field("destination", string, json),
          optional(
            field("parameters", a => unwrapResult(Parameters.decode(a))),
            json,
          ),
        )
      ) {
      | (
          source,
          fee,
          counter,
          gasLimit,
          storageLimit,
          amount,
          destination,
          parameters,
        ) =>
        Belt.Result.Ok({
          source,
          fee,
          counter,
          gasLimit,
          storageLimit,
          amount,
          destination,
          parameters,
        })
      | exception (Json.Decode.DecodeError(error)) =>
        Belt.Result.Error(error)
      };
    };
  };

  // $operation.alpha.contents
  type t =
    | Endorsement
    | SeedNonceRevelation
    | DoubleEndorsementEvidence
    | DoubleBakingEvidence
    | ActivateAccount
    | Proposals
    | Ballot
    | Reveal
    | Transaction(Transaction.t)
    | Origination
    | Delegation;

  let decode = (json: Js.Json.t): Belt.Result.t(t, string) => {
    Json.Decode.(
      switch (field("kind", string, json)) {
      | "endorsement" => Belt.Result.Ok(Endorsement)
      | "seed_nonce_revelation" => Belt.Result.Ok(SeedNonceRevelation)
      | "double_endorsement_evidence" =>
        Belt.Result.Ok(DoubleEndorsementEvidence)
      | "double_baking_evidence" => Belt.Result.Ok(DoubleBakingEvidence)
      | "activate_account" => Belt.Result.Ok(ActivateAccount)
      | "proposals" => Belt.Result.Ok(Proposals)
      | "ballot" => Belt.Result.Ok(Ballot)
      | "reveal" => Belt.Result.Ok(Reveal)
      | "transaction" =>
        Belt.Result.map(Transaction.decode(json), a => Transaction(a))
      | "origination" => Belt.Result.Ok(Origination)
      | "delegation" => Belt.Result.Ok(Delegation)
      | kind => Belt.Result.Error("Unexpected kind: " ++ kind)
      | exception (DecodeError(error)) => Belt.Result.Error(error)
      }
    );
  };
};

/* $operation_metadata.alpha.balance_updates: */
/*   [ { "kind": "contract", */
/*       "contract": $contract_id, */
/*       "change": $int64 } */
/*     || { "kind": "freezer", */
/*          "category": "rewards", */
/*          "delegate": $Signature.Public_key_hash, */
/*          "cycle": integer ∈ [-2^31-2, 2^31+2], */
/*          "change": $int64 } */
/*     || { "kind": "freezer", */
/*          "category": "fees", */
/*          "delegate": $Signature.Public_key_hash, */
/*          "cycle": integer ∈ [-2^31-2, 2^31+2], */
/*          "change": $int64 } */
/*     || { "kind": "freezer", */
/*          "category": "deposits", */
/*          "delegate": $Signature.Public_key_hash, */
/*          "cycle": integer ∈ [-2^31-2, 2^31+2], */
/*          "change": $int64 } ... ] */
