/**
 * RFC3339
 * timestamp
 * Epoch in a natural
 */

type t =
  | Timestamp(MomentRe.Moment.t);

let encode = (t: t): Js.Json.t =>
  switch (t) {
  | Timestamp(moment) =>
    Json.Encode.string(
      Belt.Option.getWithDefault(MomentRe.Moment.toJSON(moment), ""),
    )
  };

let decode = (json: Js.Json.t): Belt.Result.t(t, string) =>
  switch (Json.Decode.string(json)) {
  | v => Belt.Result.Ok(Timestamp(MomentRe.momentDefaultFormat(v)))
  | exception (Json.Decode.DecodeError(error)) => Belt.Result.Error(error)
  };

let toString = (t: t): string =>
  switch (t) {
  | Timestamp(moment) =>
    Belt.Option.getWithDefault(MomentRe.Moment.toJSON(moment), "")
  };

let ofString = (str: string): t =>
  Timestamp(MomentRe.momentDefaultFormat(str));

let formatUTC = (t: t): string =>
  switch (t) {
  | Timestamp(moment) =>
    MomentRe.Moment.format(
      "YYYY-MM-DD (HH:mm:ss UTC)",
      MomentRe.Moment.defaultUtc(moment),
    )
  };

let mk = (moment: MomentRe.Moment.t) => Timestamp(moment);

let now = () => mk(MomentRe.momentNow());

let hourFromNow = () => {
  let now = MomentRe.momentNow();
  mk(MomentRe.Moment.add(~duration=MomentRe.duration(2., `hours), now));
};

let minutesFromNow = (minutes: int) => {
  let now = MomentRe.momentNow();
  mk(
    MomentRe.Moment.add(
      ~duration=MomentRe.duration(minutes |> float_of_int, `minutes),
      now,
    ),
  );
};
