type t =
  | BigMapId(int);

let ofInt = i => BigMapId(i);

let encode = t =>
  switch (t) {
  | BigMapId(i) => Json.Encode.int(i)
  };

type s = t;

module Comparable =
  Belt.Id.MakeComparable({
    type t = s;
    let cmp = (c0, c1) =>
      switch (c0, c1) {
      | (BigMapId(int0), BigMapId(int1)) => Pervasives.compare(int0, int1)
      };
  });
