/**
 * A generic Tezos operation interface used by different wallets
 * Generally the data passed is the same because it all goes to
 * the node, but the shape of the data may differ between wallet.
 * In that case we need to convert to this type.
 */

exception DecodeError(string);

module Kind = {
  type t =
    | Endorsement
    | SeedNonceRevelation
    | DoubleEndorsementEvidence
    | DoubleBakingEvidence
    | ActivateAccount
    | Proposals
    | Ballot
    | Reveal
    | Transaction
    | Origination
    | Delegation;

  let toString = (t: t): string =>
    switch (t) {
    | Endorsement => "endorsement"
    | SeedNonceRevelation => "seed_nonce_revelation"
    | DoubleEndorsementEvidence => "double_endorsement_evidence"
    | DoubleBakingEvidence => "double_baking_evidence"
    | ActivateAccount => "activate_account"
    | Proposals => "proposals"
    | Ballot => "ballot"
    | Reveal => "reveal"
    | Transaction => "transaction"
    | Origination => "origination"
    | Delegation => "delegation"
    };

  let fromString = (s: string): t =>
    switch (s) {
    | "endorsement" => Endorsement
    | "seed_nonce_revelation" => SeedNonceRevelation
    | "double_endorsement_evidence" => DoubleEndorsementEvidence
    | "double_baking_evidence" => DoubleBakingEvidence
    | "activate_account" => ActivateAccount
    | "proposals" => Proposals
    | "ballot" => Ballot
    | "reveal" => Reveal
    | "transaction" => Transaction
    | "origination" => Origination
    | "delegation" => Delegation
    | _ => raise @@ DecodeError("Expected Network.Type, got " ++ s)
    };
};

module Parameters = {
  module Raw = {
    type t = {
      [@bs.as "entrypoint"]
      entrypoint: string,
      [@bs.as "value"]
      value: Js.Json.t,
    };
  };

  type t = {
    entrypoint: string,
    value: Tezos.Expression.t,
  };

  let toRaw = (t: t): Raw.t => {
    entrypoint: t.entrypoint,
    value: Tezos.Expression.encode(t.value),
  };
};

module Raw = {
  type t = {
    [@bs.as "kind"]
    kind: string,
    [@bs.as "amount"]
    amount: string,
    [@bs.as "destination"]
    destination: string,
    [@bs.as "parameters"]
    parameters: Js.Nullable.t(Parameters.Raw.t),
  };
};

type t = {
  kind: Kind.t,
  amount: Tezos_Mutez.t,
  destination: Tezos_Contract.t,
  parameters: option(Parameters.t),
};

let toRaw = (t: t): Raw.t =>
  {
    kind: Kind.toString(t.kind),
    amount: Tezos.Mutez.toString(t.amount),
    destination: Tezos.Contract.toString(t.destination),
    parameters: {
      switch (t.parameters) {
      | Some(p) => Js.Nullable.return(Parameters.toRaw(p))
      | None => Js.Nullable.null
      };
    },
  };
