// Use dry run to create an error and retrieve data from a contract

type transactionError = {
  kind: string,
  id: string,
  location: int,
  with_: Tezos_Expression.t,
};

let decodeTransactionError =
    (json: Js.Json.t): Belt.Result.t(transactionError, string) =>
  switch (
    Json.Decode.(
      field("kind", string, json),
      field("id", string, json),
      field("location", int, json),
      field(
        "with",
        a => Tezos_Util.unwrapResult(Tezos_Expression.decode(a)),
        json,
      ),
    )
  ) {
  | (kind, id, location, with_) =>
    Belt.Result.Ok({kind, id, location, with_})
  | exception (Json.Decode.DecodeError(err)) => Belt.Result.Error(err)
  };

type failedTransactionResult = {errors: list(transactionError)};

let decodeFailedTransactionResult =
    (json: Js.Json.t): Belt.Result.t(failedTransactionResult, string) => {
  switch (
    Json.Decode.(
      field("status", string, json),
      field(
        "errors",
        Tezos_Util.decodeSafeList(decodeTransactionError),
        json,
      ),
    )
  ) {
  | (status, errors) =>
    if (status == "failed") {
      Belt.Result.Ok({errors: errors}: failedTransactionResult);
    } else {
      Belt.Result.Error(status);
    }
  | exception (Json.Decode.DecodeError(err)) => Belt.Result.Error(err)
  };
};

type internalOperationResult = {result: failedTransactionResult};

let decodeInternalOperationResult =
    (json: Js.Json.t): Belt.Result.t(internalOperationResult, string) => {
  switch (
    Json.Decode.(
      field("kind", string, json),
      field(
        "result",
        a => Tezos_Util.unwrapResult(decodeFailedTransactionResult(a)),
        json,
      ),
    )
  ) {
  | (kind, result) =>
    if (kind == "transaction") {
      Belt.Result.Ok({result: result});
    } else {
      Belt.Result.Error(kind);
    }
  | exception (Json.Decode.DecodeError(err)) => Belt.Result.Error(err)
  };
};

type metadata = {internalOperationResults: list(internalOperationResult)};

let decodeMetadata = (json: Js.Json.t): Belt.Result.t(metadata, string) => {
  switch (
    Json.Decode.(
      field(
        "internal_operation_results",
        Tezos_Util.decodeSafeList(decodeInternalOperationResult),
        json,
      )
    )
  ) {
  | internalOperationResults =>
    Belt.Result.Ok({internalOperationResults: internalOperationResults})
  | exception (Json.Decode.DecodeError(err)) => Belt.Result.Error(err)
  };
};

type dryRunResponse = {contents: list(metadata)};

let decodeDryRunResponse =
    (json: Js.Json.t): Belt.Result.t(dryRunResponse, string) => {
  switch (
    Json.Decode.(
      field(
        "contents",
        list(a =>
          field(
            "metadata",
            b => Tezos_Util.unwrapResult(decodeMetadata(b)),
            a,
          )
        ),
        json,
      )
    )
  ) {
  | contents => Belt.Result.Ok({contents: contents})
  | exception (Json.Decode.DecodeError(err)) => Belt.Result.Error(err)
  };
};

type parameters = {
  entrypoint: string,
  value: Tezos_Expression.t,
};

let encodeParameters = p =>
  Json.Encode.(
    object_([
      ("entrypoint", string(p.entrypoint)),
      ("value", Tezos_Expression.encode(p.value)),
    ])
  );

type content = {
  kind: string,
  source: string,
  fee: Tezos_Mutez.t,
  counter: Bigint.t,
  gasLimit: Bigint.t,
  storageLimit: Bigint.t,
  amount: Tezos_Mutez.t,
  destination: string,
  parameters,
};

let encodeContent = c =>
  Json.Encode.(
    object_([
      ("kind", string(c.kind)),
      ("source", string(c.source)),      
      ("fee", Tezos_Mutez.encode(c.fee)),
      ("counter", Tezos_Util.bigintEncode(c.counter)),
      ("gas_limit", Tezos_Util.bigintEncode(c.gasLimit)),
      ("storage_limit", Tezos_Util.bigintEncode(c.storageLimit)),
      ("amount", Tezos_Mutez.encode(c.amount)),
      ("destination", string(c.destination)),
      ("parameters", encodeParameters(c.parameters)),
    ])
  );

type operation = {
  branch: string,
  signature: string,
  contents: list(content),
};

let encodeOperation = o =>
  Json.Encode.(
    object_([
      ("branch", string(o.branch)),
      ("signature", string(o.signature)),
      ("contents", list(encodeContent, o.contents)),
    ])
  );

type dryRunTransaction = {
  operation,
  chainId: string,
};

let encodeDryRunTransaction = d =>
  Json.Encode.(
    object_([
      ("operation", encodeOperation(d.operation)),
      ("chain_id", string(d.chainId)),
    ])
  );
