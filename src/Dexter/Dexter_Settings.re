let exchanges: list(Dexter_Exchange.t) =
  switch (Common.Deployment.get()) {
  | Production => [
      {
        name: "tzBTC",
        symbol: "tzBTC",
        decimals: 8,
        icon: "tzBTC-logo.png",
        tokenContract:
          Tezos.Contract.ofString("KT1PWx2mnDueood7fEmfbBDKx1D9BAnnXitn")
          |> Common.unwrapResult,
        dexterContract:
          Tezos.Contract.ofString("KT1DrJV8vhkdLEj76h1H9Q4irZDqAkMPo1Qf")
          |> Common.unwrapResult,
        dexterBigMapId: Tezos.BigMapId.ofInt(123),
      },
      {
        name: "USD Tez",
        symbol: "USDtz",
        decimals: 6,
        icon: "USDtz-logo.png",
        tokenContract:
          Tezos.Contract.ofString("KT1LN4LPSqTMS7Sd2CJw4bbDGRkMv2t68Fy9")
          |> Common.unwrapResult,
        dexterContract:
          Tezos.Contract.ofString("KT1Puc9St8wdNoGtLiD2WXaHbWU7styaxYhD")
          |> Common.unwrapResult,
        dexterBigMapId: Tezos.BigMapId.ofInt(124),
      },
    ]

  | Staging => [
      {
        name: "tzBTC",
        symbol: "tzBTC",
        decimals: 8,
        icon: "tzBTC-logo.png",
        tokenContract:
          Tezos.Contract.ofString("KT1PWx2mnDueood7fEmfbBDKx1D9BAnnXitn")
          |> Common.unwrapResult,
        dexterContract:
          Tezos.Contract.ofString("KT1DrJV8vhkdLEj76h1H9Q4irZDqAkMPo1Qf")
          |> Common.unwrapResult,
        dexterBigMapId: Tezos.BigMapId.ofInt(123),
      },
      {
        name: "USD Tez",
        symbol: "USDtz",
        decimals: 6,
        icon: "USDtz-logo.png",
        tokenContract:
          Tezos.Contract.ofString("KT1LN4LPSqTMS7Sd2CJw4bbDGRkMv2t68Fy9")
          |> Common.unwrapResult,
        dexterContract:
          Tezos.Contract.ofString("KT1Puc9St8wdNoGtLiD2WXaHbWU7styaxYhD")
          |> Common.unwrapResult,
        dexterBigMapId: Tezos.BigMapId.ofInt(124),
      },
      {
        name: "MoonTZ",
        symbol: "MTZ",
        decimals: 3,
        icon: "moonTZ.png",
        tokenContract:
          Tezos.Contract.ofString("KT1V2i7thU8h4ZFKA5FDux49ak2eiuY6AYBb")
          |> Common.unwrapResult,
        dexterContract:
          Tezos.Contract.ofString("KT1CT7S2b9hXNRxRrEcany9sak1qe4aaFAZJ")
          |> Common.unwrapResult,
        dexterBigMapId: Tezos.BigMapId.ofInt(117),
      },
    ]

  | Development => [
      {
        name: "tzBTC",
        symbol: "tzBTC",
        decimals: 8,
        icon: "tzBTC-logo.png",
        tokenContract:
          Tezos.Contract.ofString("KT1RQALMs6RhC6y5e2Hbd6YzWchMGRVTYerG")
          |> Common.unwrapResult,
        dexterContract:
          Tezos.Contract.ofString("KT1GWbSrwGScst12Pc4q7N3SQvgj6TkBzXRf")
          |> Common.unwrapResult,
        dexterBigMapId: Tezos.BigMapId.ofInt(10283),
      },
      {
        name: "USD Tez",
        symbol: "USDtz",
        decimals: 6,
        icon: "USDtz-logo.png",
        tokenContract:
          Tezos.Contract.ofString("KT1UdHaVbHEq7BUrzq2gWsC2ipsnmttV7GN7")
          |> Common.unwrapResult,
        dexterContract:
          Tezos.Contract.ofString("KT1B7U9EfmxKNtTmwzPZjZuRwRy8JCfPW5VS")
          |> Common.unwrapResult,
        dexterBigMapId: Tezos.BigMapId.ofInt(10252),
      },
    ]
  };

ErrorReporting.Sentry.init();

/** dependencies **/

let beaconNode =
  switch (Common.Deployment.get()) {
  | Production => Beacon.Network.Type.Mainnet
  | Staging => Beacon.Network.Type.Mainnet
  | Development => Beacon.Network.Type.Delphinet
  };

let indexterUrl =
  switch (Common.Deployment.get()) {
  | Production => "https://indexter.dexter.exchange"
  | Staging => "https://indexter.dexter.exchange"
  | Development => "https://dev.indexter.dexter.exchange"
  };

let nodeUrl =
  switch (Common.Deployment.get()) {
  | Production => "https://mainnet.camlcase.io"
  | Staging => "https://mainnet.camlcase.io"
  | Development => "https://delphinet.smartpy.io"
  };

let blockExplorerUrl =
  switch (Common.Deployment.get()) {
  | Production => "https://tezblock.io/block/"
  | Staging => "https://tezblock.io/block/"
  | Development => "https://delphinet.tezblock.io/block/"
  };

let transactionExplorerUrl =
  switch (Common.Deployment.get()) {
  | Production => "https://tezblock.io/transaction/"
  | Staging => "https://tezblock.io/transaction/"
  | Development => "https://delphinet.tezblock.io/transaction/"
  };

let tzktUrl =
  switch (Common.Deployment.get()) {
  | Production => "https://api.tzkt.io/v1/accounts/"
  | Staging => "https://api.tzkt.io/v1/accounts/"
  | Development => "https://api.delphi.tzkt.io/v1/accounts/"
  };

type baker = {
  address: string,
  name: string,
};

let knownBakers: list(baker) = [
  {address: "tz1WCd2jm4uSt4vntk4vSuUWoZQGhLcDuR9q", name: "Happy Tezos"},
];
