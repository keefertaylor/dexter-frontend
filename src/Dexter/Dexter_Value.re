let getMutezValue =
    (~invalid: bool=false, mutez: Tezos.Mutez.t): Valid.t(Tezos.Mutez.t) =>
  invalid
    ? Invalid(Tezos.Mutez.toTezString(mutez))
    : Valid(mutez, Tezos.Mutez.toTezString(mutez));

let getTokenValue =
    (~invalid: bool=false, token: Tezos.Token.t): Valid.t(Tezos.Token.t) =>
  invalid
    ? Invalid(Tezos.Token.toString(token))
    : Valid(token, Tezos.Token.toString(token));

let getMutezInputValue =
    (~invalid: bool=false, mutez: Tezos.Mutez.t): Valid.t(InputType.t) =>
  invalid
    ? Invalid(Tezos.Mutez.toTezString(mutez))
    : Valid(Mutez(mutez), Tezos.Mutez.toTezString(mutez));

let getTokenInputValue =
    (~invalid: bool=false, token: Tezos.Token.t): Valid.t(InputType.t) =>
  invalid
    ? Invalid(Tezos.Token.toString(token))
    : Valid(Token(token), Tezos.Token.toString(token));

let mutezZeroValue = getMutezValue(Tezos.Mutez.zero);
let tokenZeroValue = getTokenValue(Tezos.Token.zero);
let mutezZeroInputValue = getMutezInputValue(Tezos.Mutez.zero);
let tokenZeroInputValue = getTokenInputValue(Tezos.Token.zero);

let getZeroInputValue = (balance: Dexter.Balance.t): Valid.t(InputType.t) =>
  switch (balance) {
  | XtzBalance(_) => mutezZeroInputValue
  | ExchangeBalance(_) => tokenZeroInputValue
  };

let inputValueToStringWithCommas = (input: Valid.t(InputType.t)) => {
  switch (input) {
  | Valid(Mutez(mutez), _) => Tezos.Mutez.toTezStringWithCommas(mutez)
  | Valid(Token(token), _) => Tezos.Token.toStringWithCommas(token)
  | _ => ""
  };
};

let inputValueMinimum =
    (input: Valid.t(InputType.t), expectedSlippage: option(float)) => {
  let slippage =
    expectedSlippage
    |> Utils.map(slippage =>
         max(slippage, Dexter_LocalStorage.MaximumSlippage.get())
       )
    |> Utils.orDefault(Dexter_LocalStorage.MaximumSlippage.get());

  switch (input) {
  | Valid(Mutez(mutez), _) =>
    Dexter_Exchange.minimumXtzOut(mutez, slippage)
    |> Tezos.Mutez.toTezStringWithCommas
  | Valid(Token(token), _) =>
    Dexter_Exchange.minimumTokenOut(token, slippage)
    |> Tezos.Token.toStringWithCommas
  | _ => ""
  };
};

let inputValueFee = (input: Valid.t(InputType.t)) => {
  switch (input) {
  | Valid(Mutez(mutez), _) =>
    Dexter_Exchange.xtzFee(mutez) |> Tezos.Mutez.toTezStringWithCommas
  | Valid(Token(token), _) =>
    Dexter_Exchange.tokenFee(token) |> Tezos.Token.toStringWithCommas
  | _ => ""
  };
};
