/**
 * Calculations for the main Dexter entry points: exchange, add liquidity and
 * remove liquidity.
 */

/**
 * This should only be called on non-zero numbers.
 */
let toToken = (m: Tezos.Mutez.t) => {
  switch (m) {
  | Mutez(t) => Tezos.Token.mkToken(Bigint.of_int64(t), 0)
  };
};

/**
 * This should only be called on non-zero numbers.
 */
let toMutez = (m: Tezos.Token.t) => {
  Tezos.Mutez.Mutez(Bigint.to_int64(m.value));
};

/**
 * AddLiquidity: Calculate the exact amount of token to deposit for a given
 * XTZ input.
 **/
let tokensDeposited =
    (xtzIn: Tezos.Mutez.t, xtzPool: Tezos.Mutez.t, tokenPool: Tezos.Token.t)
    : option(Tezos.Token.t) =>
  Tezos.Mutez.gtZero(xtzPool)
  && Tezos.Token.gtZero(tokenPool)
  && Tezos.Mutez.gtZero(xtzIn)
    ? {
      let tokensDeposited =
        Tezos.Token.(
          cdiv(mul(toToken(xtzIn), tokenPool), toToken(xtzPool))
        );

      Tezos.Token.gtZero(tokensDeposited) ? Some(tokensDeposited) : None;
    }
    : None;

/**
 * AddLiquidity: Calculate the exact amount of token to deposit for a given
 * token input.
 **/
let xtzDeposited =
    (
      tokenSent: Tezos.Token.t,
      xtzPool: Tezos.Mutez.t,
      tokenPool: Tezos.Token.t,
    )
    : option(Tezos.Mutez.t) =>
  Tezos.Mutez.gtZero(xtzPool)
  && Tezos.Token.gtZero(tokenPool)
  && Tezos.Token.gtZero(tokenSent)
    ? {
      let xtzDeposited =
        Tezos.Token.(div(mul(tokenSent, toToken(xtzPool)), tokenPool));

      Tezos.Token.gtZero(xtzDeposited) ? Some(toMutez(xtzDeposited)) : None;
    }
    : None;

/**
 * liquidity is minted based on the amount of xtz included in the add liquidity
 * entry point.
 */
let lqtMinted =
    (
      xtzIn: Tezos.Mutez.t,
      xtzPool: Tezos.Mutez.t,
      totalLiquidity: Tezos.Token.t,
    )
    : Tezos.Token.t =>
  Tezos.Mutez.gtZero(xtzPool)
    ? Tezos.Token.(
        div(mul(toToken(xtzIn), totalLiquidity), toToken(xtzPool))
      )
    : Tezos.Token.zero;

let removeLiquidityOutput =
    (
      ~lqtBurned: Tezos.Token.t,
      ~totalLiquidity: Tezos.Token.t,
      ~tokenPool: Tezos.Token.t,
      ~xtzPool: Tezos.Mutez.t,
      (),
    )
    : (Tezos.Mutez.t, Tezos.Token.t) =>
  Tezos.Token.gtZero(totalLiquidity)
    ? {
      let token =
        Tezos.Token.(div(mul(tokenPool, lqtBurned), totalLiquidity));

      let xtz =
        toMutez(
          Tezos.Token.(
            div(mul(toToken(xtzPool), lqtBurned), totalLiquidity)
          ),
        );

      (xtz, token);
    }
    : (Tezos.Mutez.zero, Tezos.Token.mkToken(Bigint.zero, 0));

let liquidityPercentage =
    (liquidityPool: Tezos.Token.t, totalLiquidity: Tezos.Token.t): float =>
  Tezos.Token.geqZero(totalLiquidity)
    ? {
      Tezos.Token.toFloatWithDecimal(liquidityPool)
      /. Tezos.Token.toFloatWithDecimal(totalLiquidity)
      *. 100.0;
    }
    : 0.0;

let liquidityWhenZeroLqt =
    (xtzValue: Valid.t(Tezos.Mutez.t), xtzPool: Tezos.Mutez.t): Tezos.Token.t => {
  switch (xtzValue) {
  | Valid(xtz, _) => Tezos.Mutez.(toToken(add(xtz, xtzPool)))
  | _ => Tezos.Token.zero
  };
};
