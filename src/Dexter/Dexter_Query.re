let fetch =
    (exchange: Dexter_Exchange.t)
    : Js.Promise.t(Belt.Result.t(Dexter_ExchangeBalance.t, string)) => {
  let storePromise =
    exchange.dexterContract
    |> Tezos.Contract.toString
    |> Tezos.RPC.getStorage(Dexter_Settings.nodeUrl, "main", "head")
    |> Js.Promise.then_(result =>
         switch (result) {
         | Belt.Result.Ok(expression) =>
           Dexter_Store.ofExpression(exchange.decimals, expression)
           |> Js.Promise.resolve
         | Belt.Result.Error(err) =>
           Js.log2("Dexter_Query.fetch.storePromise failed", err);
           ErrorReporting.Sentry.captureException(
             "Dexter_Query.fetch.storePromise failed: " ++ err,
           );
           Belt.Result.Error("storePromise failed: " ++ err)
           |> Js.Promise.resolve;
         }
       );

  let bakerPromise =
    exchange.dexterContract
    |> Tezos.Contract.toString
    |> Tezos.RPC.getDelegate(Dexter_Settings.nodeUrl, "main", "head");

  Js.Promise.all2((storePromise, bakerPromise))
  |> Js.Promise.then_(results =>
       switch (results) {
       | (Belt.Result.Ok((store: Dexter_Store.t)), Belt.Result.Ok(baker)) =>
         Belt.Result.Ok(
           {
             dexterBaker: baker,
             dexterContract: exchange.dexterContract,
             exchangeTokenBalance: store.tokenPool,
             exchangeTotalLqt: store.lqt,
             exchangeXtz: store.xtzPool,
             icon: exchange.icon,
             name: exchange.name,
             symbol: exchange.symbol,
             tokenContract: exchange.tokenContract,
             tokenBalance: Tezos_Token.zero,
             lqtBalance: Tezos_Token.zero,
             exchangeAllowanceForAccount: Tezos.Token.zero,
           }: Dexter_ExchangeBalance.t,
         )
         |> Js.Promise.resolve

       | _ =>
         let (r0, r1) = results;
         Js.log2(
           "Dexter_Query.fetch failed",
           String.concat(", ", [Common.getError(r0), Common.getError(r1)]),
         );
         Belt.Result.Error(
           "Dexter_Query.fetch failed: "
           ++ String.concat(
                ", ",
                [Common.getError(r0), Common.getError(r1)],
              ),
         )
         |> Js.Promise.resolve;
       }
     );
};

let getBalances = () =>
  Dexter_Settings.exchanges
  |> List.map((exchange: Dexter_Exchange.t) => fetch(exchange))
  |> Array.of_list
  |> Js.Promise.all;
