/*
 * query a user's transaction history using the TZKT API.
 */

let getNamedEntrypoint =
    (operations: list(TZKT.Operation.t), entrypoint: string)
    : option(TZKT.Operation.t) => {
  List.filter(
    (x: TZKT.Operation.t) => {
      switch (x.parameters) {
      | None => false
      | Some(parameters) =>
        parameters.entrypoint
        == Tezos_Operation_Alpha.Entrypoint.Named(entrypoint)
      }
    },
    operations,
  )
  |> Belt.List.head;
};

// get the token value from the michelson body of a parameter
let getTokenValueFromParameters =
    (decimals: int, parameters: Tezos_Operation_Alpha.Parameters.t)
    : option(Tezos_Token.t) => {
  switch (parameters.value) {
  | Tezos.(
      Expression.SingleExpression(
        Primitives.PrimitiveData(PrimitiveData.Pair),
        Some([
          _,
          Expression.SingleExpression(
            Primitives.PrimitiveData(PrimitiveData.Pair),
            Some([_, Expression.IntExpression(tokenValue)]),
            _,
          ),
        ]),
        _,
      )
    ) =>
    switch (Tezos.Token.ofBigint(tokenValue, decimals)) {
    | Belt.Result.Ok(token) => Some(token)
    | _ => None
    }

  | _ => None
  };
};

/**
 * When a transaction is initiated, we now the amount of XTZ and a minimum or
 * maximum amount of token. The transaction must be applied before we can know
 * the final token amount.
 */

let getTokenValueFromOperation =
    (decimals: int, result: Tezos_Operation_Alpha.Internal.Operation.Result.t)
    : option(Tezos_Token.t) => {
  switch (result) {
  | Transaction(t) =>
    switch (t.parameters) {
    | Some(parameters) => getTokenValueFromParameters(decimals, parameters)
    | None => None
    }

  | _ => None
  };
};

let getTokenValue =
    (
      decimals: int,
      metadata: Tezos_Operation_Alpha.Contents.Result.Transaction.Metadata.t,
    )
    : option(Tezos_Token.t) => {
  switch (metadata.internalOperationResults) {
  | Some(internalOperationResults) =>
    let loTokenValue =
      Belt.List.map(
        internalOperationResults,
        (
          internalOperationResult: Tezos_Operation_Alpha.Internal.Operation.Result.t,
        ) =>
        getTokenValueFromOperation(decimals, internalOperationResult)
      );
    Belt.List.head(Common.catSomes(loTokenValue));

  | None => None
  };
};

let getXtzValueFromOperation =
    (
      dexterContract: string,
      destination: Tezos_Address.t,
      result: Tezos_Operation_Alpha.Internal.Operation.Result.t,
    )
    : option(Tezos_Mutez.t) => {
  switch (result) {
  | Transaction(t) =>
    if (t.destination == Tezos_Address.toString(destination)
        && t.source == dexterContract) {
      Some(t.amount);
    } else {
      None;
    }
  | _ => None
  };
};

let getXtzValue =
    (
      dexterContract: string,
      destination: Tezos_Address.t,
      metadata: Tezos_Operation_Alpha.Contents.Result.Transaction.Metadata.t,
    )
    : option(Tezos_Mutez.t) => {
  switch (metadata.internalOperationResults) {
  | Some(internalOperationResults) =>
    let loXtzValue =
      Belt.List.map(
        internalOperationResults,
        (
          internalOperationResult: Tezos_Operation_Alpha.Internal.Operation.Result.t,
        ) =>
        getXtzValueFromOperation(
          dexterContract,
          destination,
          internalOperationResult,
        )
      );
    Belt.List.head(Common.catSomes(loXtzValue));

  | None => None
  };
};
