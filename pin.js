const pinataSDK = require('@pinata/sdk');
const { env } = require('yargs');
const https = require('https')
const USER = process.env.IPFS_CLUSTER_USER;
const PASS = process.env.IPFS_CLUSTER_PASS;
const pinata = pinataSDK(process.env.PINATA_API_KEY, process.env.PINATA_SECRET_KEY);

pinata.testAuthentication().then((result) => {
    console.log("AUTHENTICATED SUCCESSFULLY")
    console.log(result);
}).catch((err) => {
    console.log("FAILED TO AUTHENTICATE")
    console.log(err);
    process.exit(1)
});

const sourcePath = './build';
const options = {
    pinataMetadata: {
        name: 'dexter-frontend',
    },
    pinataOptions: {
        cidVersion: 0
    }
};

pinata.pinFromFS(sourcePath, options).then((result) => {
    console.log("SUCCESSFUL UPLOAD")
    process.env['SLACK_MESSAGE']=result
    console.log(result);

    const options = {
        hostname: "ipfs.cluster.camlcase.io",
        port: 443,
        path: "/pins/" + result.IpfsHash,
        method: 'POST',
        headers: {
            'Authorization': 'Basic ' + new Buffer(USER + ':' + PASS).toString('base64')
        } 
    }

    const req = https.request(options, res => {
        console.log(`statusCode: ${res.statusCode}`)
      
        res.on('data', d => {
          process.stdout.write(d)
        })
    })
      
    req.on('error', error => {
        console.log("FAILED TO PIN")
        process.env['SLACK_MESSAGE']=error
        console.error(error)
    })
      
    req.end()
}).catch((err) => {
    console.log("FAILED TO UPLOAD")
    process.env['SLACK_MESSAGE']=err
    console.log(err);
});


